Rails.application.routes.draw do

  get 'activities/index'
  match '*path', via: [:options], to: lambda {|_| [204, { 'Content-Type' => 'text/plain' }]}
  resources :track do
    member do
      get :get_coordinates
    end
  end
  resources :blogs, except: [:destroy]

  namespace :admin do
    root 'dashboard#index'
    devise_scope :user do
      get '/sign_in' => "/users/sessions#new" , as: :sign_in
      get '/sign_up' => "/users/registrations#new", as: :sign_up

     
    end
    # devise_scope :admin do
    #   get '/sign_up' => "/admin/fleet_individual/new", as: :sign_up
    # end
    get "/dashboard/cargo-owners" => "dashboard#cargo_owners" , as: :cargo_owners
    get "/dashboard/fleet-owners" => "dashboard#fleet_owners" , as: :fleet_owners
    get "/shipment_dashboard/posted-shipments" => "shipment_dashboard#shipment_posted" , as: :shipment_posted
    get "/shipment_dashboard/fleet-owners" => "shipment_dashboard#fleet_owners" , as: :shipment_upcoming
    get "/shipment_dashboard/fleet-owners" => "shipment_dashboard#fleet_owners" , as: :shipment_ongoing

    post "/dashboard/mark-verified/:company_id" => "dashboard#mark_verified", as: :mark_verified
    post "/dashboard/mark-featured/:company_id" => "dashboard#mark_featured", as: :mark_featured
    post "/dashboard/mark-blacklisted/:company_id" => "dashboard#mark_blacklisted", as: :mark_blacklisted
    post "/dashboard/tax_invoice/:company_id" => "dashboard#tax_invoice", as: :tax_invoice
    post "/dashboard/mark-safe-for-cash-on-delivery/:company_id" => "dashboard#mark_safe_for_cash_on_delivery", as: :mark_safe_for_cash_on_delivery
    get "/dashboard/add_vehicle" => "dashboard#add_vehicle", as: :add_dashboard_vehicle   
    post "/dashboard/add_fleet_dashboard_vehicle" => "dashboard#add_fleet_dashboard_vehicle", as: :add_fleet_dashboard_vehicle
    get "/dashboard/show-user/:user_id" => "dashboard#show_user", as: :show_user
    get "/dashboard/assigned_shipment/:company_id" => "dashboard#assigned_shipment", as: :assigned_shipment
    get "/dashboard/edit-user/:user_id" => "dashboard#edit_user", as: :edit_user
    patch "/dashboard/update-user/:user_id" => "dashboard#update_user", as: :update_user
    get "/dashboard/cargo-contracts/:company_id" => "dashboard#cargo_contracts", as: :cargo_contracts
    post "/payments/mark-paid-by-cargo/:shipment_id" => "payments#mark_paid_by_cargo", as: :mark_paid_by_cargo
    post "/payments/mark-lorryz-comission-received/:shipment_id" => "payments#mark_lorryz_comission_received", as: :mark_lorryz_comission_received
    post "/payments/mark-paid-to-fleet/:shipment_id" => "payments#mark_paid_to_fleet", as: :mark_paid_to_fleet
    post "/payments/cancel_penalty_amount_received/:shipment_id" => "payments#cancel_penalty_amount_received", as: :cancel_penalty_amount_received
    post "/fleet_individual/add_individual_information" => "fleet_individual#add_individual_information", as: :add_individual_information
    post "/fleet_individual/add_vehicle" => "fleet_individual#add_vehicle", as: :add_vehicle   
    post "/fleet_individual/add_fleet_individual_vehicle" => "fleet_individual#add_fleet_individual_vehicle", as: :add_fleet_individual_vehicle
  
     post "/cargo_individual/add_cargo_individual_information" => "cargo_individual#add_cargo_individual_information", as: :add_cargo_individual_information
     post "/cargo_individual/add_cargo_company_information" => "cargo_company#add_cargo_company_information", as: :add_cargo_company_information

     post "/shipments/add_admin_shipment_details" => "shipments#add_admin_shipment_details", as: :add_admin_shipment_details
  
     get "/dashboard/edit-history/:user_id" => "dashboard#edit_history", as: :edit_history

     resources :companies, :only => [  ] do
      collection do
        post :add_company_information
        post :add_individual_information
        patch :add_individual_information
        get :edit
        get :get_individual_information
        put :presign_upload
      end
    end
    resources :primary_vehicle

    resources :pages
    resources :gps_vehicle , except: [:destroy]
    resources :soa    do
      collection do
        get :country_accounts
        get :customer_accounts

        end
      end

    resources :fleet_individual , except: [:destroy] do  collection do
      put :presign_upload
    end end
    resources :cargo_individual , except: [:destroy]
    resources :cargo_company , except: [:destroy]
    resources :invoices , only: [:show]
    resources :countries , except: [:destroy]
    resources :preferred_location , except: [:destroy]
    resources :vehicle_types, except: [:destroy]

    resources :blogs, except: [:destroy]
    resources :locations
    resources :tcn , only: [:show]
    resources :fleet_invoices , only: [:show]

    resources :change_destination_requests
    resources :vendor, except: [:destroy]
    resources :shipments do
      collection do
      get :cargo_invoice
      put :presign_upload

      end
      member do
        delete :cancel
        get :assign_vehicle
        get :assign_cost
        get :email
        get :edit_history

        patch :status_email





      end
      collection do
        post :get_bulk_coordinates

      end
    end
    get '/reports', to: 'reports#index'
    get '/onlinedrivers', to: 'onlinedrivers#index'
    resources :users  do
      collection do
        post :get_all_drivers
        post :get_all_drivers_filtered
        get :change_password


      end
    end


    resources :payments
  end

  namespace :moderator do
    root 'dashboard#index'
    get "/dashboard/cargo-owners" => "dashboard#cargo_owners" , as: :cargo_owners
    get "/dashboard/show-user/:user_id" => "dashboard#show_user", as: :show_user
    get "/dashboard/edit-user/:user_id" => "dashboard#edit_user", as: :edit_user
    patch "/dashboard/update-user/:user_id" => "dashboard#update_user", as: :update_user
    get "/dashboard/cargo-contracts/:company_id" => "dashboard#cargo_contracts", as: :cargo_contracts
    post "/dashboard/mark-verified/:company_id" => "dashboard#mark_verified", as: :mark_verified
    post "/dashboard/mark-featured/:company_id" => "dashboard#mark_featured", as: :mark_featured
    get "/dashboard/add_vehicle" => "dashboard#add_vehicle", as: :add_dashboard_vehicle   
    post "/dashboard/add_fleet_dashboard_vehicle" => "dashboard#add_fleet_dashboard_vehicle", as: :add_fleet_dashboard_vehicle
    get "/shipment_dashboard/posted-shipments" => "shipment_dashboard#shipment_posted" , as: :shipment_posted
    get "/shipment_dashboard/fleet-owners" => "shipment_dashboard#fleet_owners" , as: :shipment_upcoming
    get "/shipment_dashboard/fleet-owners" => "shipment_dashboard#fleet_owners" , as: :shipment_ongoing

    post "/dashboard/mark-blacklisted/:company_id" => "dashboard#mark_blacklisted", as: :mark_blacklisted
    post "/dashboard/mark-safe-for-cash-on-delivery/:company_id" => "dashboard#mark_safe_for_cash_on_delivery", as: :mark_safe_for_cash_on_delivery
    get "/dashboard/fleet-owners" => "dashboard#fleet_owners" , as: :fleet_owners
    post "/dashboard/tax_invoice/:company_id" => "dashboard#tax_invoice", as: :tax_invoice
    post "/payments/mark-paid-by-cargo/:shipment_id" => "payments#mark_paid_by_cargo", as: :mark_paid_by_cargo
    post "/payments/mark-lorryz-comission-received/:shipment_id" => "payments#mark_lorryz_comission_received", as: :mark_lorryz_comission_received
    post "/payments/mark-paid-to-fleet/:shipment_id" => "payments#mark_paid_to_fleet", as: :mark_paid_to_fleet
    post "/payments/cancel_penalty_amount_received/:shipment_id" => "payments#cancel_penalty_amount_received", as: :cancel_penalty_amount_received
    post "/fleet_individual/add_individual_information" => "fleet_individual#add_individual_information", as: :add_individual_information
    post "/fleet_individual/add_vehicle" => "fleet_individual#add_vehicle", as: :add_vehicle   
    post "/fleet_individual/add_fleet_individual_vehicle" => "fleet_individual#add_fleet_individual_vehicle", as: :add_fleet_individual_vehicle
    post "/cargo_individual/add_cargo_individual_information" => "cargo_individual#add_cargo_individual_information", as: :add_cargo_individual_information
    post "/cargo_individual/add_cargo_company_information" => "cargo_company#add_cargo_company_information", as: :add_cargo_company_information
    get "/dashboard/assigned_shipment/:company_id" => "dashboard#assigned_shipment", as: :assigned_shipment
    get "/dashboard/edit-history/:user_id" => "dashboard#edit_history", as: :edit_history

    resources :companies, :only => [  ] do
      collection do
        post :add_company_information
        post :add_individual_information
        patch :add_individual_information
        get :edit
        get :get_individual_information
        put :presign_upload
      end
    end
    resources :vendor, except: [:destroy]
      resources :fleet_individual , except: [:destroy] do  collection do
      put :presign_upload
      end end
    resources :cargo_individual , except: [:destroy]
    resources :cargo_company , except: [:destroy]
    resources :invoices , only: [:show]
    resources :tcn , only: [:show]
    resources :fleet_invoices , only: [:show]
    resources :dashboard , only: [:index]
    resources :preferred_location , except: [:destroy]

    resources :gps_vehicle , except: [:destroy]
    resources :soa    do
      collection do
        get :country_accounts
        get :customer_accounts

        end
      end

    resources :payments
    resources :shipments do
      collection do
        get :cargo_invoice
        put :presign_upload

        end

      member do
        delete :cancel
        get :assign_vehicle
        get :assign_cost
         get :email
         patch :status_email
         get :edit_history
      end
      
      collection do
        post :get_bulk_coordinates
      end
    end
    get '/onlinedrivers', to: 'onlinedrivers#index'
    resources :users  do
      collection do
        post :get_all_drivers
        post :get_all_drivers_filtered

      end
    end


   end

  namespace :cargo do
    root 'dashboard#index'
    resources :payments
    resources :shipments do
      collection do
        get :posted
        get :ongoing
        post :calculate_fare
        get :upcoming
        get :completed
        post :get_bulk_coordinates
        post :get_bulk_filtered_coordinates

        get :pickup_location
        get :dropoff_location
        get :vehicle_type
        get :image_select
        get :eta_update
        get :update_timer
        get :no_bids
        get :no_selected
        get :update_bids
        get :update_upcoming
     

      end
      member do
        get :get_coordinates
        get :cancel
        get :update_destination
        patch :update_destination
        
      end

    end
    resources :bids do
      member do
        post :accept
        post :reject
      end
    end
    resources :dashboard , only: [:index] do 
      collection do
      get :filtered_dashboard
      end
    end
  end
  namespace :fleet do
    root 'shipments#posted'
    resources :payments
    resources :shipments do
      collection do
        get :posted
        get :active
        get :ongoing
        get :won
        get :completed
      end
      member do
        get :quit_bid
        get :assign_vehicle
        get :cancel
        get :get_coordinates
        post :not_interested
      end
      resources :bids do
        collection do
          post :bid_details
          get :bidNow
          # get :notInterested
        end
      end
    end
    
    resources :drivers do
      collection do
        post :register_as_driver
      end
    end
    resources :vehicles do
      collection do
        get :vehicle_types
     
    end
      member do
        post :change_driver
        get :change_driver
      end
    end

    resources :dashboard , only: [:index]
  end
  resources :countries do
    collection do
      get "location/:location_id" => "countries#location"
      get :get_vehicles_by_country
      # get :vehicles
    end
  end
  devise_for :users, controllers: {:invitations => 'users/invitations',passwords: 'users/passwords', sessions: 'users/sessions', registrations: 'users/registrations',confirmations: 'users/confirmations'   }

  devise_scope :user do
    # match "user/account" => "user#account", as: :user_account, via: [:get, :post]
    match '/users/mobile-verification' => 'users/registrations#mobile_verification' , via: [:get, :post]
    get '/users/send-verification-code/:user_id' => 'users/registrations#send_verification_code', as: :send_verification_code
    post '/users/gps_coordinates' => 'users/registrations#gps_coordinates', as: :gps_coordinates
    match '/sign_up' => "users/registrations#new", as: :sign_up,via: [:get, :post]
  end

  resources :companies, :only => [  ] do
    collection do
      post :add_company_information
      patch :add_company_information
      post :add_individual_information
      patch :add_individual_information
      get :edit
      get :get_individual_information
      get :get_company_information
      put :presign_upload
    end
  end
  match "/ratings" => "rating#rate", via: [:get, :post] , as: :ratings_shipment
  resources :invoices , only: [:show]

  namespace :api do
  	namespace :v1 do
      devise_for :users, controllers: { sessions: 'api/v1/users/sessions', registrations: 'api/v1/users/registrations'  }
      resources :ratings do
      end
      resources :invoices do
        member do
          post :send_in_email
        end
      end
      namespace :cargo do
        resources :companies do
          resources :payments
          resources :shipments do
            collection do
              get :posted
              get :ongoing
              get :upcoming
              post :calculate_fare
              get :completed
            end
            resources :bids do
              member do
                post :accept
                post :reject
              end
            end
          end
        end
      end
      namespace :fleet do
        resources :drivers do
          collection do
            post :register_as_driver
            post :upcoming_shipments
            post :ongoing_shipments
            post :completed_shipments
            post :update_vehicle_status
            post :report_problem
            post :payment_received
          end
        end
        resources :vehicles do
          collection do
            get :vehicle_types
            post :change_driver
            get :fetch_nearby
          end
          member do
            put :update_lat_lng
          end
        end
        resources :companies do
          resources :payments
          resources :shipments do
            collection do
              get :posted
              get :active
              get :won
              get :ongoing
              get :ongoing_vehicles
              get :completed

            end
            member do
              post :reject_bid
              post :quit_bid
              put :update_destination
              get :vehicles
              post :not_interested
            end
          resources :bids do
            collection do
              get :bidNow
              get :notInterested
            end
          end
          end
        end
      end
	  	resources :companies, :only => [] do
        collection do
          post :add_company_information
          post :add_individual_information
          get :get_individual_information
          get :get_company_information
        end
      end
      resources :countries do
        member do
          get :locations
          get :vehicles
          get :primary_vehicles
          get :secondary_vehicles

        end
      end
      resources :reports do
        collection do
          post :logs_report
        end
      end
      resources :email do
        collection do
          post :send_shipment_email
        end
      end
      resources :signed_urls do
        collection do
          put :presign_upload
        end
      end
      resources :users, :only => [:update, :destroy] do
        collection do
          post :send_phone_verify_code
          put :verify_phone_number
          post :resent_code
          post :register_as_fleet_owner
          post :register_as_cargo_owner
          post :register_as_individual
          post :register_as_company
          post :current_user_detail
          post :db_device_id
          post :reset_password
          patch :change_online_status
          patch :change_device_id
        end
        member do
          get :notifications
        end
      end
      get 'shipments/:shipment_id/get_bids_count', to: 'fleet/bids#get_bids_count'
    end
  end
  resources :notifications , only: [] do
    collection do
      post :mark_viewed
    end
  end
  resources :users
  resources :subscriptions
  resources :dashboard  , only: [:index] do
    collection do
      get :home
      post :home_shipment
      patch :initial_shipment
      get :initial_shipments
      get :otp_shipments



      post :share_iphone
      post :share_android
      post :contact
      get  :privacy_policy
      get  :terms_and_conditions_for_fleet
      get :terms_and_conditions_for_cargo
      get :email
    end
 
 
 
  end

 resources :pages
  # get "/del" => "company#del"
  root 'dashboard#home'
end


