# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

set :output, "log/cron.log"

every 4.hours do
  rake "shipment_reminder_notification"
  rake "assign_vehicle_reminder_notification"
end

every :day, at: '12:00am' do
  rake "update_vehicle_availability"
end


every :day, at: '5:58pm' do
    puts "User online cron hit!!!"
    puts "UTC"
    rake "update_online_status"
end

#every :day, at: '11:10pm' do
#    puts "User online cron hit!!!"
#    puts "PST"
#    rake "update_online_status"
#end
#
#every :day, at: '11:40pm' do
#    puts "User online cron hit!!!"
#    puts "IST"
#    rake "update_online_status"
#end

#every :day, at: '03:25am' do
#    puts "User online cron hit!!!"
#    puts "+5"
#    rake "update_online_status"
#end

every :day, at: '12:00am' do

   rake "expired_documents_cargo_company"
   rake "expired_documents_fleet_company"
   rake "expired_documents_cargo_individual"
   rake "expired_documents_fleet_individual"   
end

every 3.minutes do
 rake "update_coords"
end


every 15.minutes do
  rake "update_status"
 end