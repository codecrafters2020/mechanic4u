# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_21_133121) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "activities", force: :cascade do |t|
    t.string "trackable_type"
    t.bigint "trackable_id"
    t.string "owner_type"
    t.bigint "owner_id"
    t.string "key"
    t.text "parameters"
    t.string "recipient_type"
    t.bigint "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type"
    t.index ["owner_type", "owner_id"], name: "index_activities_on_owner_type_and_owner_id"
    t.index ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type"
    t.index ["recipient_type", "recipient_id"], name: "index_activities_on_recipient_type_and_recipient_id"
    t.index ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type"
    t.index ["trackable_type", "trackable_id"], name: "index_activities_on_trackable_type_and_trackable_id"
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.jsonb "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "bids", force: :cascade do |t|
    t.integer "company_id"
    t.float "amount"
    t.integer "status"
    t.integer "detention"
    t.integer "shipment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "lat"
    t.float "lng"
  end

  create_table "blogs", force: :cascade do |t|
    t.text "title"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "image_url"
    t.text "initial_summary"
    t.string "slug"
  end

  create_table "change_destination_requests", force: :cascade do |t|
    t.integer "shipment_id"
    t.float "lat"
    t.float "lng"
    t.string "city"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "drop_building_name", default: ""
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "companies", force: :cascade do |t|
    t.string "company_type"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_contractual", default: false
    t.boolean "verified", default: false
    t.boolean "featured", default: false
    t.boolean "blacklisted", default: false
    t.boolean "safe_for_cash_on_delivery", default: true
    t.float "credit_period"
    t.float "credit_amount", default: 0.0
    t.boolean "tax_invoice", default: false
    t.string "tax_registration_no"
  end

  create_table "company_informations", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "landline_no"
    t.string "website_address"
    t.string "license_number"
    t.string "license_expiry_date"
    t.string "secondary_contact_name"
    t.string "secondary_mobile_no"
    t.boolean "news_update"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
    t.boolean "is_complete", default: false
    t.json "avatar"
    t.string "documents", default: [], array: true
    t.string "industry", default: ""
    t.string "trade_registration_no", default: ""
    t.string "trade_registration_expiry", default: ""
    t.string "contact1_first_name", default: ""
    t.string "contact1_last_name", default: ""
    t.string "contact1_mobile", default: ""
    t.string "contact1_email", default: ""
    t.string "contact1_designation", default: ""
    t.string "contact1_office_no", default: ""
    t.string "contact2_first_name", default: ""
    t.string "contact2_last_name", default: ""
    t.string "contact2_mobile", default: ""
    t.string "contact2_email", default: ""
    t.string "contact2_designation", default: ""
    t.string "contact2_office_no", default: ""
    t.string "contact3_first_name", default: ""
    t.string "contact3_last_name", default: ""
    t.string "contact3_mobile", default: ""
    t.string "contact3_email", default: ""
    t.string "contact3_designation", default: ""
    t.string "contact3_office_no", default: ""
    t.index ["company_id"], name: "index_company_informations_on_company_id"
  end

  create_table "company_ratings", force: :cascade do |t|
    t.integer "giver_id"
    t.integer "receiver_id"
    t.integer "shipment_id"
    t.float "rating"
    t.text "remarks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "dialing_code"
    t.string "process"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "commision"
    t.float "lorryz_share_tax"
    t.string "currency"
    t.float "penalty_free_hours"
    t.boolean "status"
    t.float "fleet_owner_income_tax"
    t.integer "fleet_max_days", default: 0
    t.float "fleet_max_amount", default: 0.0
    t.float "fleet_penalty_free_hours"
    t.string "short_name"
    t.float "cargo_max_amount", default: 0.0
    t.integer "cargo_max_days", default: 0
    t.index ["dialing_code"], name: "index_countries_on_dialing_code", unique: true
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "driver_informations", force: :cascade do |t|
    t.string "national_id"
    t.date "expiry_date"
    t.string "license"
    t.date "license_expiry"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "license_documents", default: [], array: true
    t.string "emirate_documents", default: [], array: true
    t.json "avatar"
    t.index ["deleted_at"], name: "index_driver_informations_on_deleted_at"
  end

  create_table "gps_vehicles", force: :cascade do |t|
    t.string "imei", null: false
    t.string "supplier", null: false
    t.string "manufacturer", null: false
    t.integer "vehicle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "status"
    t.integer "country_id"
  end

  create_table "individual_informations", force: :cascade do |t|
    t.string "secondary_mobile_no"
    t.string "landline_no"
    t.string "national_id"
    t.string "expiry_date"
    t.boolean "news_update"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
    t.boolean "is_complete", default: false
    t.json "avatar"
    t.string "documents", default: [], array: true
    t.string "secondary_mobile_no_1"
    t.string "passport_no"
    t.string "passport_no_expiry"
    t.string "visa_no"
    t.string "visa_no_expiry"
    t.string "license_no"
    t.string "license_no_expiry"
    t.string "nationality"
    t.string "preferred_location"
    t.string "secondary_mobile_no_1_country", default: ""
    t.string "secondary_mobile_no_country", default: ""
    t.index ["company_id"], name: "index_individual_informations_on_company_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "pickup_location"
    t.string "pickup_lat"
    t.string "pickup_lng"
    t.string "pickup_city"
    t.string "drop_location"
    t.string "drop_lat"
    t.string "drop_lng"
    t.string "drop_city"
    t.decimal "rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "country_id"
    t.bigint "company_id"
    t.string "pickup_building_name"
    t.string "drop_building_name"
    t.integer "vehicle_type_id"
    t.float "loading_time", default: 0.0
    t.float "unloading_time", default: 0.0
    t.index ["company_id"], name: "index_locations_on_company_id"
  end

  create_table "not_interested_shipments", force: :cascade do |t|
    t.integer "user_id"
    t.integer "shipment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shipment_id"], name: "index_not_interested_shipments_on_shipment_id"
    t.index ["user_id"], name: "index_not_interested_shipments_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.integer "mentioned_by_id"
    t.integer "user_mentioned_id"
    t.boolean "viewed_flag", default: false
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.string "page_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "preferred_locations", force: :cascade do |t|
    t.string "country_name", null: false
    t.string "city_name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "primary_vehicles", force: :cascade do |t|
    t.text "name"
    t.integer "country_id"
    t.string "image_url", default: [], array: true
  end

  create_table "session_logs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "company_id", null: false
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipment_action_dates", force: :cascade do |t|
    t.string "state"
    t.date "performed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "shipment_id"
    t.integer "vehicle_id"
    t.time "performed_at_time"
  end

  create_table "shipment_fleets", force: :cascade do |t|
    t.integer "shipment_id"
    t.integer "fleet_cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
  end

  create_table "shipment_vehicles", force: :cascade do |t|
    t.integer "shipment_id"
    t.integer "vehicle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
  end

  create_table "shipmentestimations", force: :cascade do |t|
    t.string "name"
    t.string "destination"
    t.string "origin"
    t.integer "vehicle_type"
    t.integer "country"
    t.float "amount"
    t.string "contact_no"
    t.string "verification_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipments", force: :cascade do |t|
    t.string "pickup_location"
    t.string "pickup_lat"
    t.string "pickup_lng"
    t.string "drop_location"
    t.string "drop_lat"
    t.string "drop_lng"
    t.date "pickup_date"
    t.datetime "pickup_time"
    t.float "loading_time"
    t.date "expected_drop_off"
    t.float "unloading_time"
    t.integer "no_of_vehicles"
    t.text "cargo_description"
    t.string "cargo_packing_type"
    t.integer "company_id"
    t.integer "vehicle_id"
    t.boolean "approved", default: false
    t.integer "shipping_request_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state"
    t.decimal "amount"
    t.integer "location_id"
    t.string "pickup_city"
    t.string "drop_city"
    t.integer "country_id"
    t.string "process"
    t.integer "vehicle_type_id"
    t.integer "fleet_id"
    t.string "payment_option"
    t.string "cancel_by"
    t.decimal "cancel_penalty"
    t.text "cancel_reason"
    t.decimal "lorryz_share_amount"
    t.decimal "fleet_income"
    t.decimal "amount_tax"
    t.decimal "detention", default: "0.0"
    t.decimal "lorryz_detention_share_amount"
    t.decimal "fleet_detention_income"
    t.decimal "detention_tax"
    t.boolean "paid_to_fleet", default: false
    t.boolean "paid_by_cargo", default: false
    t.boolean "lorryz_comission_received", default: false
    t.string "tracking_code"
    t.date "invoice_date"
    t.decimal "amount_per_vehicle", default: "0.0"
    t.decimal "detention_per_vehicle", default: "0.0"
    t.datetime "deleted_at"
    t.boolean "cancel_penalty_amount_received", default: false
    t.float "discount", default: 0.0
    t.string "pickup_building_name", default: ""
    t.string "drop_building_name", default: ""
    t.float "distance_between_endpoints"
    t.boolean "is_pickup_now", default: false
    t.string "accepted_rate_currency", default: ""
    t.string "fleet_rate_currency", default: ""
    t.string "additional_rate_currency", default: ""
    t.integer "additional_rate"
    t.string "additional_rate_desc", default: ""
    t.boolean "is_createdby_admin", default: false
    t.decimal "fleet_net_rate"
    t.integer "vendor"
    t.string "additional_type", default: ""
    t.integer "additional_rate1"
    t.string "additional_rate_desc1", default: ""
    t.string "additional_type1", default: ""
    t.integer "additional_rate2"
    t.string "additional_rate_desc2", default: ""
    t.string "additional_type2", default: ""
    t.integer "additional_rate3"
    t.string "additional_rate_desc3", default: ""
    t.string "additional_type3", default: ""
    t.integer "additional_rate4"
    t.string "additional_rate_desc4", default: ""
    t.string "additional_type4", default: ""
    t.integer "additional_rate5"
    t.string "additional_rate_desc5", default: ""
    t.string "additional_type5", default: ""
    t.integer "additional_rate6"
    t.string "additional_rate_desc6", default: ""
    t.string "additional_type6", default: ""
    t.decimal "customer_net_amount"
    t.string "status"
    t.boolean "balance_paid"
    t.datetime "balance_paid_date"
    t.decimal "weight"
    t.string "final_paid_to"
    t.string "final_voucher", default: [], array: true
    t.boolean "VAT_applicable"
    t.boolean "fixed_destination"
    t.string "documents", default: [], array: true
    t.string "advance_amount"
    t.string "advance_paid_date"
    t.string "advance_paid_to"
    t.string "advance_voucher", default: [], array: true
    t.boolean "additional_cost_vat"
    t.boolean "additional_cost_1_vat"
    t.boolean "additional_cost_2_vat"
    t.boolean "additional_cost_3_vat"
    t.boolean "additional_cost_4_vat"
    t.boolean "additional_cost_5_vat"
    t.boolean "additional_cost_6_vat"
    t.boolean "is_fixed"
    t.string "invoice_comments"
    t.integer "primary_vehicle_category"
    t.string "category"
    t.index ["company_id"], name: "index_shipments_on_company_id"
    t.index ["deleted_at"], name: "index_shipments_on_deleted_at"
    t.index ["shipping_request_type_id"], name: "index_shipments_on_shipping_request_type_id"
    t.index ["vehicle_id"], name: "index_shipments_on_vehicle_id"
  end

  create_table "short_listed_bidders", force: :cascade do |t|
    t.integer "fleet_owner_id"
    t.integer "company_id"
    t.integer "shipment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "mobile"
    t.integer "company_id"
    t.integer "role"
    t.string "mobile_verified"
    t.string "verification_code"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.boolean "terms_accepted", default: false
    t.integer "country_id"
    t.text "device_id"
    t.string "os"
    t.datetime "deleted_at"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.boolean "is_online", default: true
    t.bigint "session_log_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vehicle_types", force: :cascade do |t|
    t.integer "code"
    t.text "name"
    t.integer "country_id"
    t.decimal "fleet_penalty_amount"
    t.decimal "cargo_penalty_amount"
    t.decimal "base_rate"
    t.float "free_kms"
    t.decimal "rate_per_km"
    t.decimal "waiting_charges"
    t.float "load_unload_free_hours"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_url", default: [], array: true
    t.integer "primary_category"
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "registration_number"
    t.string "insurance_number"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "expiry_date"
    t.string "authorization_letter"
    t.integer "vehicle_type_id"
    t.integer "user_id"
    t.float "latitude"
    t.float "longitude"
    t.boolean "available", default: true
    t.date "not_available_from"
    t.date "not_available_to"
    t.string "documents", default: [], array: true
    t.date "insurance_expiry_date"
    t.datetime "coordinates_updated_at"
    t.index ["company_id"], name: "index_vehicles_on_company_id"
  end

  add_foreign_key "locations", "companies"
end
