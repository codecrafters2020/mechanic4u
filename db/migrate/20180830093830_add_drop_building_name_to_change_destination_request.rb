class AddDropBuildingNameToChangeDestinationRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :change_destination_requests, :drop_building_name, :string, default: ""
  end
end
