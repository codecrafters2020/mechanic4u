class AddColumnsFleetShipmnts < ActiveRecord::Migration[5.2]
  def change
    change_column :shipment_fleets, :payment_cleared, :date

  end
end
