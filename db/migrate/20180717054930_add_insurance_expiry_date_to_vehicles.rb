class AddInsuranceExpiryDateToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :insurance_expiry_date, :date
  end
end
