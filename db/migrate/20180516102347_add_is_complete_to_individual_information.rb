class AddIsCompleteToIndividualInformation < ActiveRecord::Migration[5.2]
  def change
    add_column :individual_informations, :is_complete, :boolean , default: false
  end
end
