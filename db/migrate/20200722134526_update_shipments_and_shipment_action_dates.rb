class UpdateShipmentsAndShipmentActionDates < ActiveRecord::Migration[5.2]
  def change
    change_column :shipment_action_dates, :performed_at, :date
    add_column :shipment_action_dates, :performed_at_time, :time


  end
end
