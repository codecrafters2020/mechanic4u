class CreateGps < ActiveRecord::Migration[5.2]
  def change
    create_table :gps do |t|
      t.integer :gps_id, null: false
      t.string  :IMEI, null: false
      t.string :supplier, null: false
      t.string :manufacturer, null: false
      t.string :country, null: false
      t.string :vehicle_id
      t.string :status
      t.datetime :start_time
      t.datetime :end_time
      t.timestamps
    end
  end
end
