class AddTableShipnentEstimations < ActiveRecord::Migration[5.2]
  def change
      create_table :shipmentestimations do |t|
        t.string :name
        t.string :destination
        t.string :origin
        t.integer :vehicle_type
        t.integer :country
        t.float :amount
        t.string :contact_no
        t.string :verification_code   
  
        t.timestamps
      end
      
    
  end
end
