class CreateNotInterestedShipments < ActiveRecord::Migration[5.2]
  def change
    create_table :not_interested_shipments do |t|
      t.integer :user_id
      t.integer :shipment_id

      t.timestamps
    end
    add_index :not_interested_shipments, :user_id
    add_index :not_interested_shipments, :shipment_id
  end
end
