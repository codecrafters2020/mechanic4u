class AddAvailableFieldsToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :available, :boolean,default: true
    add_column :vehicles, :not_availabile_from, :date
    add_column :vehicles, :not_availabile_to, :date
  end
end
