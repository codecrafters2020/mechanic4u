class CreateShipmentActionDates < ActiveRecord::Migration[5.2]
  def change
    create_table :shipment_action_dates do |t|
      t.string :state
      t.datetime :performed_at

      t.timestamps
    end
  end
end
