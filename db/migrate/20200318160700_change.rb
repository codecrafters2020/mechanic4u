class Change < ActiveRecord::Migration[5.2]
  def change
    rename_column :gps, :IMEI, :imei
  end
end
