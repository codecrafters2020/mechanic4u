class AddTaxRegistrationNoToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :tax_registration_no, :string
  end
end
