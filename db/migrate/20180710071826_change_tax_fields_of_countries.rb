class ChangeTaxFieldsOfCountries < ActiveRecord::Migration[5.2]
  def change
    add_column :countries, :fleet_owner_income_tax, :float
    rename_column :countries, :tax, :lorryz_share_tax
  end
end
