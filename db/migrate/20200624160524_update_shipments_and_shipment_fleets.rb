class UpdateShipmentsAndShipmentFleets < ActiveRecord::Migration[5.2]
  def change
    remove_column :shipment_fleets, :advance_paid_to
    remove_column :shipment_fleets, :advance_voucher
    remove_column :shipment_fleets, :advance_amount
    remove_column :shipment_fleets, :payment_cleared


    add_column :shipments, :advance_amount, :string
    add_column :shipments, :advance_paid_date, :string
    add_column :shipments, :advance_paid_to, :string
    add_column :shipments, :advance_voucher, :string, array: true, default: []

 

  end
end
