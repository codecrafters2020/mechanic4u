class CreateChangeDestinationRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :change_destination_requests do |t|
      t.integer :shipment_id
      t.float :lat
      t.float :lng
      t.string :city
      t.string :address

      t.timestamps
    end
  end
end
