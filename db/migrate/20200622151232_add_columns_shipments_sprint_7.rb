class AddColumnsShipmentsSprint7 < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :VAT_applicable, :Boolean
    add_column :shipments, :fixed_destination, :Boolean
    add_column :shipments, :documents, :string, array: true, default: []
  end
end
