class AddCountryIdToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :country_id, :integer
    add_column :shipments, :process, :string
  end
end
