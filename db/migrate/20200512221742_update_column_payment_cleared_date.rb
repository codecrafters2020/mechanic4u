class UpdateColumnPaymentClearedDate < ActiveRecord::Migration[5.2]
  def change
    rename_column :shipments, :advance_paid, :balance_paid
    add_column :shipments, :balance_paid_date, :datetime

  end
end
