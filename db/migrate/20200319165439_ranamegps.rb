class Ranamegps < ActiveRecord::Migration[5.2]
  def change
    rename_table :gps, :gps_vehicle
  end
end
