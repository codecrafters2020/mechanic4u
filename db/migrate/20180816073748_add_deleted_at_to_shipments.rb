class AddDeletedAtToShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :deleted_at, :datetime
    add_index :shipments, :deleted_at
  end
end
