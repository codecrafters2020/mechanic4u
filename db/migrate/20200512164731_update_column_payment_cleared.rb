class UpdateColumnPaymentCleared < ActiveRecord::Migration[5.2]
  def change
   
    remove_column :shipment_fleets, :payment_cleared
    add_column :shipment_fleets, :payment_cleared, :datetime
  end
end
