class AddCompanyToLocation < ActiveRecord::Migration[5.2]
  def change
    add_reference :locations, :company, foreign_key: true
  end
end
