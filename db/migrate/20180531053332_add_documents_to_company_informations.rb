class AddDocumentsToCompanyInformations < ActiveRecord::Migration[5.2]
  def change
    add_column :company_informations, :documents, :string, array: true, default: []
  end
end
