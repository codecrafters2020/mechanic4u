class ChangeTypeOfDescriptionToBodyInBlogs < ActiveRecord::Migration[5.2]
  def change
    change_column :blogs, :title, :text
  end
end
