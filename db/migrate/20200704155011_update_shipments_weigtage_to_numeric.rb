class UpdateShipmentsWeigtageToNumeric < ActiveRecord::Migration[5.2]
  def change
    change_column :shipments, :weight, :numeric

  end
end
