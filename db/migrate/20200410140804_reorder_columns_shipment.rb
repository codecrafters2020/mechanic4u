class ReorderColumnsShipment < ActiveRecord::Migration[5.2]
  def change
    change_column :shipments, :amount, :numeric, after: :accepted_rate_currency
  end
end
