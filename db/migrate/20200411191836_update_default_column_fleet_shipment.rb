class UpdateDefaultColumnFleetShipment < ActiveRecord::Migration[5.2]
  def change
    change_column_default :shipment_fleets, :status, from: "booked", to: 1

  end
end
