class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :pickup_location
      t.string :pickup_lat
      t.string :pickup_lng
      t.string :pickup_city
      t.string :drop_location
      t.string :drop_lat
      t.string :drop_lng
      t.string :drop_city
      t.decimal :rate

      t.timestamps
    end
  end
end
