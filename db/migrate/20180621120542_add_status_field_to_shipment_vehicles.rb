class AddStatusFieldToShipmentVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :shipment_vehicles, :status, :integer
  end
end
