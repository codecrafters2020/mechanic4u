class RemovestartTime < ActiveRecord::Migration[5.2]
  def change
    remove_column :gps, :start_time, :timestamp
    remove_column :gps, :end_time, :timestamp
  end
end
