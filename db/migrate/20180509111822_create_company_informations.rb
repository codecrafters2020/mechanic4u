class CreateCompanyInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :company_informations do |t|
      t.string :name
      t.string :address
      t.string :landline_no
      t.string :website_address
      t.string :license_number
      t.string :license_expiry_date
      t.string :secondary_contact_name
      t.string :secondary_mobile_no
      t.boolean :news_update
      t.timestamps
    end
  end
end
