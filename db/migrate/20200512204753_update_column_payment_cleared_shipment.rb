class UpdateColumnPaymentClearedShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :advance_paid, :boolean

  end
end
