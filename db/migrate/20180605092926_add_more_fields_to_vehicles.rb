class AddMoreFieldsToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :expiry_date, :date
    add_column :vehicles, :authorization_letter, :string
    add_column :vehicles, :vehicle_type_id, :integer
    remove_column :vehicles, :vehicle_type, :string
    remove_column :vehicles, :load_capacity, :integer
  end
end
