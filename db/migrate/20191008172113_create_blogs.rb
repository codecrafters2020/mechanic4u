class CreateBlogs < ActiveRecord::Migration[5.2]
  def change
    create_table :blogs do |t|
      t.text :title
      t.text :description
      t.datetime :created_at
      t.datetime :updated_at
      t.string :image_url
      t.string :text

      t.timestamps
    end
  end
end
