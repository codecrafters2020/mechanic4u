class CreateCompanyRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :company_ratings do |t|
      t.integer :giver_id
      t.integer :receiver_id
      t.integer :shipment_id
      t.float :rating
      t.text :remarks

      t.timestamps
    end
  end
end
