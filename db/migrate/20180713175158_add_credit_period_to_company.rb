class AddCreditPeriodToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :credit_period, :float
  end
end
