module UserConcern
  extend ActiveSupport::Concern
  include ActionView::Helpers::DateHelper
  def email_authentication? params
   params[:session][:user][:email].match(/^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i)
  end

  def phone_authentication? params
   params[:user][:email].match(/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/)
  end

  def email_authentication_web? params
    params[:user][:login].match(/^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i)
  end

  def phone_authentication_web? params
    params[:user][:login].match(/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/)
  end

  def signout_blacklisted
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    # flash[:notice] = "You're Blacklisted. Please contact Lorryz."
    redirect_to new_user_session_path , notice: "Your account is being verified and you will be notified shortly by Lorryz Team. For any enquiries please send email to contact@lorryz.com or call 056 5466088"
  end

  def signout_driver
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    # flash[:notice] = "You're Blacklisted. Please contact Lorryz."
    redirect_to new_user_session_path , notice: "You are only allowed to access your account from mobile application. For any enquiries please send email to contact@lorryz.com or call 056 5466088"
  end

  
  def make_secondary_phone_number country_id , phone_number

    short_name = Country.find_by_id(country_id).try(:short_name)

    if short_name.present? and phone_number.present?

      return  TelephoneNumber.parse(phone_number, short_name).international_number(formatted: true).delete(" ")
    end
  end

	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end

  def make_phone_number country_id , phone_number
  
    short_name = Country.find_by_id(country_id).try(:short_name)
    if short_name.present? and phone_number.present?
      return  TelephoneNumber.parse(phone_number, short_name).international_number(formatted: true).delete(" ")
    end
  end

  def phone_number_valid?  country_id , phone_number
    short_name = Country.find_by_id(country_id).try(:short_name)
    return TelephoneNumber.valid?(phone_number, short_name, [:mobile, :fixed_line])
  end

  def admin_or_superadmin? user
    user.admin? or user.superadmin? 
  end

  def moderator? user
    user.moderator? 
  end
  
  def get_driver_bulk_coordinates vehicles


  Aws.config.update({
      region: "us-east-1",
      credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz'),
      # endpoint: "http://localhost:8000"
     
    })
    
    dynamodb = Aws::DynamoDB::Client.new
    
    table_name = 'vehicle_coordinates'
    


    @data = []
    @active_fleet_admin = User.busy_fleet_individual_owners.individual_fleet_owners 
    @active_fleet = User.busy_fleet_owners.individual_fleet_owners 
    @active_fleets =   @active_fleet.select(:company_id)
    @active_fleet_admins =@active_fleet_admin.select(:company_id)
    @active_fleet= @active_fleets.distinct
    @active_fleet_admin =@active_fleet_admins.distinct
    @gps_response = "not_responding"
    
    
    @vehicles.each do |vehicle|
     

      vehicleType = VehicleType.find(vehicle.vehicle_type_id)
     if vehicle.try(:driver).try(:is_online) == true      
      @status ='online'
     elsif vehicle.try(:driver).try(:is_online) == false  
      @status ='offline'
     end
     coordinates_updated_at = "N/A"

     @count= @active_fleet_admins.where(:company_id => vehicle.company_id).count + @active_fleets.where(:company_id => vehicle.company_id).count
     if @count >0 
      if @active_fleet_admins.where(:company_id => vehicle.company_id).count > 0
        @state =ShipmentFleet.shipment_active_fleet(vehicle.company_id).first.shipment_id
      end
      if @active_fleets.where(:company_id => vehicle.company_id).count > 0
        @state =Shipment.fleet_active_shipments(vehicle.company_id).first.id
      end
      else
     @state ="Free"
     end

     @updated_at =(DateTime.now().utc - vehicle.coordinates_updated_at) rescue ""
     @last_updated = @updated_at / 60  rescue ""

        if @last_updated !=  "" 

          if @last_updated < 30  
        vehicle_status = "Parked"
      end
      coordinates_updated_at = distance_of_time_in_words(@last_updated) if vehicle.coordinates_updated_at.present?

      if @state != "Free" 

        @gps_response = "responding"
      
      
      end
      
      end


     @imei = 0;
     @category=0;
     @imei = GpsVehicle.find_by_vehicle_id(vehicle.id).imei  if  GpsVehicle.find_by_vehicle_id(vehicle.id).present?

        
     @category = 1 if @imei != 0 && @state != "Free"   && @gps_response != "not_responding" ;
     
     @category = 2 if  @imei == 0 && @state != "Free" && @gps_response != "not_responding" ;

     @category = 3 if  @imei != 0 && @state == "Free" ;

     @category = 4 if  @imei == 0 && @state == "Free" &&  @status != "online";

     @category = 5 if  @imei == 0 && @state == "Free" && @status == "online";

     @category = 6 if  @imei != 0 && @state != "Free"  && @gps_response == "not_responding" ;

     @category = 7 if  @imei == 0 &&  @state != "Free" && @gps_response == "not_responding";
     
     
     @data.push({driver_name: (vehicle.driver.full_name rescue "") , lat: (vehicle.latitude.to_f rescue ""), lng: (vehicle.longitude.to_f rescue ""), is_online: (@status  rescue ""), registration_number: (vehicle.registration_number rescue ""), type: (vehicleType.name rescue "") ,phone:(vehicle.try(:driver).try(:mobile)), condition:@state, imei:@imei ,category:@category, updated_at: coordinates_updated_at } ) if  vehicle.latitude.present?  
     end


    @data
  end

end