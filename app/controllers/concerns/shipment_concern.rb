module ShipmentConcern
  extend ActiveSupport::Concern

  # In convert time in params to cargo users country and then to utc to be saved in db
  def set_time_zone time
    if time.present?
      time_zone = ISO3166::Country.new(current_user.country.short_name).timezones.zone_identifiers.first
      converted_time = Time.use_zone(time_zone) do
        time.to_datetime.change(offset: Time.zone.now.strftime("%z"))
      end
      return converted_time.utc
    else
      return time
    end
  end

  def set_time_zone_admin time ,country
    if time.present?

     
      time_zone = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
      converted_time = Time.use_zone(time_zone) do
        time.to_datetime.change(offset: Time.zone.now.strftime("%z"))
      end
      return converted_time.utc
    else
      return time
    end
  end

  def get_shipment_coordinates shipment_id
    @shipment = Shipment.find_by_id(shipment_id)
    return @shipment.shipment_vehicles.where(status: ShipmentVehicle::ONGOING_VEHICLES).map{|sv| {vehicle_name: sv.vehicle.vehicle_name ,lat: (sv.vehicle.latitude.to_f rescue ""), lng: (sv.vehicle.longitude.to_f rescue ""), status: (sv.status.humanize rescue "") ,id: sv.id.to_s} }
  end

  def get_shipment_bulk_coordinates shipments
    @data = []
    @shipments.each do |shipment|

      shipment.shipment_vehicles.map{|sv| 
   
      @data.push({vehicle_name: (sv.vehicle.vehicle_name rescue "") , lat: (sv.vehicle.latitude.to_f rescue ""), lng: (sv.vehicle.longitude.to_f rescue ""),shipment: shipment,pickup_full_address: shipment.pickup_full_address,drop_full_address: shipment.drop_full_address,vehicle_type: shipment.vehicle_type }) }
    end
    @data
  end

  def get_shipment_bulk_cargo_coordinates shipments
    @data = []
    distance = "Cargo offloaded"
    @shipments.each do |shipment|

      shipment.shipment_vehicles.map{|sv| 
      duration="N/A"
      distance="N/A"
      coordinates_updated_at ="N/A"
      km = 0
      vehicle_status = "Parked"
      @last_updated= ""
     @last_updated =(DateTime.now.utc - sv.vehicle.coordinates_updated_at) /1.hour if  sv.vehicle.coordinates_updated_at.present?

        if @last_updated !=  "" 

          if @last_updated < 0.5  
        vehicle_status = "Moving"
      end
      coordinates_updated_at = distance_of_time_in_words(@last_updated*3600) if sv.vehicle.coordinates_updated_at.present?
      # puts "\n\n\n\t\t @updated_at#{coordinates_updated_at} \n\n"
      # puts "\n\n\n\t\t @updated_at#{@last_updated} \n\n"

      end


   
      @data.push({vehicle_name: (sv.vehicle.vehicle_name rescue "") , lat: (sv.vehicle.latitude.to_f rescue ""), lng: (sv.vehicle.longitude.to_f rescue ""),shipment: shipment,pickup_full_address: shipment.pickup_full_address,drop_full_address: shipment.drop_full_address,vehicle_type: shipment.vehicle_type,vehicle_status: sv.status ,vehicle_state: vehicle_status, coordinates_updated_at: coordinates_updated_at})  if  sv.vehicle.latitude.present?}   
  
    end
    @data
  end

  def shipment_country_access_control shipment_id
    shipment = Shipment.find(shipment_id)
    redirect_to root_path unless (current_user.country.dialing_code == shipment.country.dialing_code)
  end

  def shipment_owner_access_control

  end

end