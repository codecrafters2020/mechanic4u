class Admin::UsersController < AdminController
  layout 'admin'

  include UserConcern
  before_action :set_user , only: [:show,:change_password,:update]
  before_action :set_users_link  , only: [:index , :show]
  before_action :check_if_super_admin , only: [:new , :create]

  def index
    @users = User.all.reverse
  end


  def get_all_drivers
    @vehicles = Vehicle.all
    render json: { coordinates:  get_driver_bulk_coordinates(@vehicles) }, status: :ok


  end
def change_password
 


  end

  def update 

    @user.update_attributes(password: params[:user][:password])

    redirect_to admin_users_path,notice: "password successfully updated"


  end 


  def get_all_drivers_filtered
     @vehicles = Vehicle.all

     @vehicles = @vehicles.filter_by_country_id(params[:country])  if params[:country].present?
     @vehicles = @vehicles.filter_by_imei(params[:imei])  if params[:imei].present?
     @vehicles = @vehicles.filter_by_vehicle(params[:vehicle_number])  if params[:vehicle_number].present?
     @vehicles = @vehicles.filter_by_shipment(params[:shipment_no])  if params[:shipment_no].present?
     @vehicles = @vehicles.filter_by_customer(params[:customer_name])  if params[:customer_name].present?

      render json: { coordinates:  get_driver_bulk_coordinates(@vehicles),result:"abc" }, status: :ok
  end
  
  def show
  end

  def new
    @admin_user  = User.new
  end

  def create
    country_id = params[:user][:country_id]
    initial_mobile_number = params[:user][:mobile]
    params[:user][:mobile] = make_phone_number country_id , params[:user][:mobile]
    phone_number_valid = phone_number_valid?( country_id,  params[:user][:mobile] )

    @password = params[:user][:password]
    @admin_user = User.new(admin_user_params)
    @admin_user.role = :admin

    if phone_number_valid and @admin_user.save
      UserMailer.send_admin_credentials_to_super_admin(current_user.email, @admin_user.id , @password).deliver
      redirect_to admin_users_path , :notice => "Admin created successfully!"
    else
      flash[:error] = "Invalid Phone number." unless phone_number_valid
      render :new
    end

  end

  def set_user
    @user = User.find(params[:id])
  end

  private

  def set_users_link
    @users_link= "active"
  end

  def admin_user_params
    params.require(:user).permit(:first_name, :last_name , :email, :mobile, :country_id ,:password)
  end

end
