class Admin::VehicleTypesController < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin
  before_action :set_vehicle_type , only: [:edit, :update,:destroy]
  before_action :set_vehicle_types_link , only: [:edit, :update , :new , :index ]
  layout 'admin'
  def index
    @vehicle_types = VehicleType.all
    respond_to do |format|
      format.html
      format.xlsx {
        start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
        end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
        @vehicle_types = VehicleType.where(created_at: start_date..end_date)
        render xlsx: 'xlsx_vehicle_types', filename: "all_vehicle_types.xlsx", disposition: 'attachment'
      }
    end
  end
  def new
    @vehicle_type = VehicleType.new
  end
  def edit
  end
  def create
    set_attachment_params
    @vehicle_type = VehicleType.new(vehicle_type_params)
    print vehicle_type_params
    if @vehicle_type.save
      redirect_to admin_vehicle_types_path , notice: "Vehicle Type Created Successfully!"
    else
      format.html { render :new }
    end
  end
  def update
    set_attachment_params
    if @vehicle_type.update(vehicle_type_params)
      redirect_to admin_vehicle_types_path , notice: "Vehicle Type Updated Successfully!"
    else
      format.html { render :edit}
    end
  end
  def destroy
    if @vehicle_type.destroy
      redirect_to admin_vehicle_types_path, notice: "Vehicle Type destroyed successfully"
    else
      redirect_back(fallback_location: admin_vehicle_types_path)
    end
  end
  private
  def set_vehicle_type
    @vehicle_type = VehicleType.find(params[:id])
  end
  def vehicle_type_params
    params.require(:vehicle_type).permit(:code,:primary_category, :name, :country_id, "fleet_penalty_amount", "cargo_penalty_amount", "base_rate", "free_kms", "rate_per_km", "waiting_charges", "load_unload_free_hours", :avatar,image_url: [])
  end
  def set_vehicle_types_link
    @vehicle_types_link = "active"
  end



  def set_attachment_params
     if params[:vehicle_type][:image_url].present?

      begin
        params[:vehicle_type][:image_url]= eval(params[:vehicle_type][:image_url].first) 
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end  end
      
    end
      
end
