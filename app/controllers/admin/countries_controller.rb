class Admin::CountriesController < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin
  before_action :set_country , only: [:edit,  :update, :destroy]
  before_action :set_countries_link ,only: [:edit,  :index , :new]
  layout 'admin'
  def index
    @countries = Country.all
    respond_to do |format|
      format.html
      format.xlsx {
        start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
        end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
        @countries= Country.where(created_at: start_date..end_date)
        render xlsx: 'xlsx_countries', filename: "all_countries.xlsx", disposition: 'attachment'
      }
    end
  end
  def new
    @admin_country = Country.new
  end
  def create
    set_country_params
    @admin_country = Country.new(country_params)
    if @admin_country.save
      redirect_to admin_countries_path , notice: "Country created successfully!"
    else
      render :new
    end
  end
  def edit
  end
  def update
    set_country_params
    if @admin_country.update(country_params)
      redirect_to admin_countries_path , notice: "Country updated successfully!"
    else
      render :edit
    end
  end
  def destroy
    if @admin_country.destroy
      redirect_to admin_countries_path , notice: "Country destroyed successfully"
    else
      redirect_back(fallback_location: admin_countries_path)
    end
  end
  private
  def country_params
    params.require(:country).permit(:name, :dialing_code,:short_name ,:process, :commision, :lorryz_share_tax,:fleet_owner_income_tax, :currency, :penalty_free_hours, :status, :cargo_max_amount, :cargo_max_days,:fleet_max_amount, :fleet_max_amount,:fleet_penalty_free_hours, :fleet_max_days)
  end
  def set_country
    @admin_country = Country.find(params[:id])
  end
  def set_countries_link
    @countries_link = "active"
  end
  def set_country_params
    name = params[:country][:name]
    country = ISO3166::Country.find_country_by_name(name)
    params[:country][:short_name] = country.gec
    params[:country][:dialing_code] = "+" + country.country_code.to_s rescue  ""
  end
end
