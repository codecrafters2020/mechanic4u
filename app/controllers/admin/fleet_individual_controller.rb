class Admin::FleetIndividualController < AdminController
  layout 'admin'

  include UserConcern
  before_action :set_user , only: [:show]
  before_action :set_users_link  , only: [:index , :show]
  before_action :check_if_super_admin , only: [:new , :create]

  require 's3_presigner'
  def presign_upload
    render json: UploadPresigner.presign("/users/avatar/", params[:filename], limit: 1.megabyte)
  end
  def index
    @users = User.all.order(created_at: :desc)
  end


  def get_all_drivers
    @vehicles = Vehicle.all
puts " asdfasf #{@vehicles}"
  #  @shipments = Shipment.includes(:vehicle_type,:shipment_vehicles).where(shipment_vehicles: {status: ShipmentVehicle::ONGOING_VEHICLES })
    render json: { coordinates:  get_driver_bulk_coordinates(@vehicles) }, status: :ok
  end
  
  def show
  end

  def new
    @admin_fleet  = User.new
  end

  def create
    country_id = params[:user][:country_id]
    initial_mobile_number = params[:user][:mobile]
    params[:user][:mobile] = make_phone_number country_id , params[:user][:mobile]
    phone_number_valid = phone_number_valid?( country_id,  params[:user][:mobile] )

    params[:user][:password] ="12345678"
    @admin_fleet = User.new(admin_fleet_params)
    @admin_fleet.role = :fleet_owner
    
    if @admin_fleet.save
      @admin_fleet.register_as_individual 
      @admin_fleet.update(mobile_verified: "verified", verification_code: "",terms_accepted: TRUE)
      get_individual_information
      # UserMailer.send_admin_credentials_to_super_admin(current_user.email, @admin_user.id , @password).deliver
    
    else
      flash[:error] = "Invalid Phone number." 
       render :new
      end
    

  end
  def get_individual_information

    @current_fleet = User.find_by_mobile(@admin_fleet.mobile)
   

    if( @current_fleet.company.company_type == "individual")
      if  @current_fleet.company.individual_information.present?
        @edit = true
        @info =  @current_fleet.company.individual_information
      else
        @info =  @current_fleet.company.build_individual_information
      end
    else
      redirect_to root_path , notice: "Invalid Request"
    end
    render :get_individual_information

  end
  def add_vehicle
    
    @vehicle=Vehicle.new
    puts "\n\n\n\t\t #{params} \n\n"
    @current_fleet = User.find params[:individual_information][:userid]

    render :add_vehicle

  end
  def vehicle_params
    params.require(:vehicle).permit(:registration_number, :insurance_number, :vehicle_type_id, :company_id,:expiry_date,:authorization_letter ,:available,:not_available_from,:not_available_to, :insurance_expiry_date,  documents: [])
  end
  def add_fleet_individual_vehicle
    set_attachment_vehicle_params
    @current_user = User.find params[:vehicle][:userid]

    @vehicle =  @current_user.company.vehicles.create(vehicle_params)
    @vehicle.update(user_id: @current_user.id) if @current_user.company.individual?
    redirect_to admin_fleet_owners_path , notice: "Vehicle created successfully!"
  end

  def add_individual_information
    set_attachment_params
   @current_fleet = User.find params[:individual_information][:userid]
   country_id = @current_fleet.country_id


    company = @current_fleet.company
    if company.individual_information.present?
      initial_mobile_number = params[:individual_information][:secondary_mobile_no_1]
      params[:individual_information][:secondary_mobile_no_1] = make_secondary_phone_number country_id , params[:individual_information][:secondary_mobile_no_1]
      initial_mobile_number1 = params[:individual_information][:secondary_mobile_no]
      params[:individual_information][:secondary_mobile_no] = make_secondary_phone_number country_id , params[:individual_information][:secondary_mobile_no_1]
     
      
      company.individual_information.update(individual_params)
      flash[:notice] = "Your Profile information is updated Successfully."  
      info = company.individual_information
    else
      initial_mobile_number = params[:individual_information][:secondary_mobile_no_1]
      params[:individual_information][:secondary_mobile_no_1] = make_secondary_phone_number country_id , params[:individual_information][:secondary_mobile_no_1]
      initial_mobile_number1 = params[:individual_information][:secondary_mobile_no]
      params[:individual_information][:secondary_mobile_no] = make_secondary_phone_number country_id , params[:individual_information][:secondary_mobile_no_1]
     

      info = company.build_individual_information(individual_params)
    end
    @current_user.company.update!(tax_registration_no: params[:company][:tax_registration_no]) if current_user.fleet_owner? and params[:company][:tax_registration_no].present?
    




    info.save

    
    company.update(blacklisted:false)


    add_vehicle
  end

  def set_user
    @user = User.find(params[:id])
  end
  def individual_params
    params.require(:individual_information).permit(:name ,:address ,:secondary_mobile_no, :landline_no,:secondary_mobile_no_1,:secondary_mobile_no_country,:secondary_mobile_no_1_country,:passport_no,:passport_no_expiry,:visa_no,:visa_no_expiry,:license_no,:license_no_expiry,:nationality,:preferred_location,:national_id, :expiry_date,:news_update,:avatar,documents: [])
  end

  private

  def set_users_link
    @users_link= "active"
  end

  def admin_fleet_params
    params.require(:user).permit(:first_name, :last_name , :email, :mobile, :country_id ,:password)
  end

  def set_attachment_params
    if params[:company_information].present?
      begin
        params[:company_information][:avatar] = eval(params[:company_information][:avatar]).first if params[:company_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
          params[:company_information][:documents] = eval(params[:company_information][:documents].first) if params[:company_information][:documents].present?
      rescue SyntaxError => e
        params[:company_information][:documents] = params[:company_information][:documents].first.split(" ") rescue ""
      end
      params[:company_information][:documents] = params[:company_information][:documents].blank? ? [] : params[:company_information][:documents]
    elsif params[:individual_information].present?
      begin
        params[:individual_information][:avatar] = eval(params[:individual_information][:avatar]).first if params[:individual_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
        params[:individual_information][:documents] = eval(params[:individual_information][:documents].first) if params[:individual_information][:documents].present?
      rescue SyntaxError => e
        params[:individual_information][:documents] = params[:individual_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:individual_information][:documents] = params[:individual_information][:documents].blank? ? [] : params[:individual_information][:documents]
    end
  end


  def set_attachment_vehicle_params

    if params[:vehicle][:documents].present?

      begin
        params[:vehicle][:documents] = eval(params[:vehicle][:documents].first) if params[:vehicle][:documents].present?
      rescue SyntaxError => e
        params[:vehicle][:documents] = params[:vehicle][:documents].first.split(" ") rescue ""
      end
      params[:vehicle][:documents] =  [] if params[:vehicle][:documents].blank?
    end
  end


end