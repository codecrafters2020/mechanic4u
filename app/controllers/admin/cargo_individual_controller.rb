class Admin::CargoIndividualController < AdminController
  layout 'admin'

  include UserConcern
  before_action :set_user , only: [:show]
  before_action :set_users_link  , only: [:index , :show]
  before_action :check_if_super_admin , only: [:new , :create]

  def index
    @users = User.all.reverse
  end


  def get_all_drivers
    @vehicles = Vehicle.all
  #  @shipments = Shipment.includes(:vehicle_type,:shipment_vehicles).where(shipment_vehicles: {status: ShipmentVehicle::ONGOING_VEHICLES })
    render json: { coordinates:  get_driver_bulk_coordinates(@vehicles) }, status: :ok
  end
  
  def show
  end

  def new
    @admin_cargo  = User.new
  end

  def create
    country_id = params[:user][:country_id]
    initial_mobile_number = params[:user][:mobile]
    params[:user][:mobile] = make_phone_number country_id , params[:user][:mobile]
    params[:user][:password] ="12345678"
    @password = params[:user][:password]
    @admin_cargo = User.new(admin_cargo_params)
    @admin_cargo.role = :cargo_owner
    
    if   @admin_cargo.save
    @admin_cargo.register_as_individual 
      @admin_cargo.update(mobile_verified: "verified", verification_code: "",terms_accepted: TRUE)
      get_individual_information
      # UserMailer.send_admin_credentials_to_super_admin(current_user.email, @admin_user.id , @password).deliver
    
    else 
      flash[:error] = @admin_cargo.errors.full_messages
       render :new
      end
    

  end
  def get_individual_information

    @current_cargo = User.find_by_mobile(@admin_cargo.mobile)
   

    if( @current_cargo.company.company_type == "individual")
      if  @current_cargo.company.individual_information.present?
        @edit = true
        @info =  @current_cargo.company.individual_information
      else
        @info =  @current_cargo.company.build_individual_information
      end
    else
      redirect_to root_path , notice: "Invalid Request"
    end
    render :get_individual_information

  end
  
  def add_cargo_individual_information
    set_attachment_params
   @current_cargo = User.find params[:individual_information][:userid]
   country_id = @current_cargo.country_id


    company = @current_cargo.company
    if company.individual_information.present?
      
      company.individual_information.update(individual_params)
      flash[:notice] = "Your Profile information is updated Successfully."  
      info = company.individual_information
    else
     

      info = company.build_individual_information(individual_params)
    end
    @current_user.company.update!(tax_registration_no: params[:company][:tax_registration_no]) if current_user.cargo_owner? and params[:company][:tax_registration_no].present?
    info.save
    redirect_to admin_cargo_owners_path , notice: "Cargo created successfully!"
  end

  def set_user
    @user = User.find(params[:id])
  end
  def individual_params
    params.require(:individual_information).permit(:name ,:address ,:secondary_mobile_no, :landline_no, :national_id, :expiry_date,:news_update,:avatar,documents: [])
  end

  private

  def set_users_link
    @users_link= "active"
  end

  def admin_cargo_params
    params.require(:user).permit(:first_name, :last_name , :email, :mobile, :country_id ,:password)
  end

  def set_attachment_params
    if params[:company_information].present?
      begin
        params[:company_information][:avatar] = eval(params[:company_information][:avatar]).first if params[:company_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
          params[:company_information][:documents] = eval(params[:company_information][:documents].first) if params[:company_information][:documents].present?
      rescue SyntaxError => e
        params[:company_information][:documents] = params[:company_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:company_information][:documents] = params[:company_information][:documents].blank? ? [] : params[:company_information][:documents]
    elsif params[:individual_information].present?
      begin
        params[:individual_information][:avatar] = eval(params[:individual_information][:avatar]).first if params[:individual_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
        params[:individual_information][:documents] = eval(params[:individual_information][:documents].first) if params[:individual_information][:documents].present?
      rescue SyntaxError => e
        params[:individual_information][:documents] = params[:individual_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:individual_information][:documents] = params[:individual_information][:documents].blank? ? [] : params[:individual_information][:documents]
    end
  end


end