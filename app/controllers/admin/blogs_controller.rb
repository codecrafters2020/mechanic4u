class Admin::BlogsController < AdminController
    before_action :authenticate_user!
    before_action :check_if_admin
    before_action :set_blog , only: [:edit, :update,:destroy]
    before_action :set_blog_link , only: [:edit, :update , :new , :index ]
    layout 'admin'
    def index
      @blogs = Blog.all
      respond_to do |format|
        format.html
        format.xlsx {
          start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
          end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
          @blogs = Blog.where(created_at: start_date..end_date)
          render xlsx: 'xlsx_blogs', filename: "all_blogs.xlsx", disposition: 'attachment'
        }
      end
    end
    def new
      @blog = Blog.new
    end
    def edit
    end
    def create
      @blog = Blog.new(blog_params)
      print blog_params
      if @blog.save
        redirect_to admin_blogs_path , notice: "Blog Created Successfully!"
      else
        format.html { render :new }
      end
    end
    def update
      if @blog.update(blog_params)
        redirect_to admin_blogs_path , notice: "Blogs Updated Successfully!"
      else
        format.html { render :edit}
      end
    end
    def destroy
      if @blog.destroy
        redirect_to admin_blogs_path, notice: "Blog destroyed successfully"
      else
        redirect_back(fallback_location: admin_blogs_path)
      end
    end
    private
    def set_blog
        @blog = Blog.find(params[:id])
      end
    def blog_params
      params.require(:blog).permit(:title, :initial_summary, :description, :avatar ,:slug)
    end
    def set_blog_link
      @blog_link = "active"
    end
  end
  