class Admin::CargoCompanyController < AdminController
  layout 'admin'

  include UserConcern
  before_action :set_user , only: [:show]
  before_action :set_users_link  , only: [:index , :show]
  before_action :check_if_super_admin , only: [:new , :create]

  def index
    @users = User.all.reverse
  end


  def new
    @admin_cargo  = User.new
  end

  def create
    country_id = params[:user][:country_id]
    initial_mobile_number = params[:user][:mobile]
    params[:user][:mobile] = make_phone_number country_id , params[:user][:mobile]
    phone_number_valid = phone_number_valid?( country_id,  params[:user][:mobile] )

   params[:user][:password] ="12345678"
    @admin_cargo = User.new(admin_cargo_params)
    @admin_cargo.role = :cargo_owner
    
    if @admin_cargo.save
    @admin_cargo.register_as_company
      @admin_cargo.update(mobile_verified: "verified", verification_code: "",terms_accepted: TRUE)
      get_company_information
      # UserMailer.send_admin_credentials_to_super_admin(current_user.email, @admin_user.id , @password).deliver
    
    else 
      flash[:error] = "Invalid Phone number." 
       render :new
      end
    

  end
  def get_company_information

    @current_cargo = User.find_by_mobile(@admin_cargo.mobile)
   

    if( @current_cargo.company.company_type == "company")
      if  @current_cargo.company.company_information.present?
        @edit = true
        @info =  @current_cargo.company.company_information
      else
        @info =  @current_cargo.company.build_company_information
      end
    else
      redirect_to root_path , notice: "Invalid Request"
    end
    render :get_company_information

  end
  
  def add_cargo_company_information
    set_attachment_params
   @current_cargo = User.find params[:company_information][:userid]
   country_id = @current_cargo.country_id


    company = @current_cargo.company
    if company.company_information.present?
      initial_mobile_number = params[:company_information][:secondary_mobile_no_1]
      params[:company_information][:secondary_mobile_no_1] = make_secondary_phone_number country_id , params[:company_information][:secondary_mobile_no_1]
      initial_mobile_number1 = params[:company_information][:secondary_mobile_no]
      params[:company_information][:secondary_mobile_no] = make_secondary_phone_number country_id , params[:company_information][:secondary_mobile_no_1]
     

      company.company_information.update(company_params)
      flash[:notice] = "Your Profile information is updated Successfully."  
      info = company.company_information
    else
      initial_mobile_number = params[:company_information][:secondary_mobile_no_1]
      params[:company_information][:company_information] = make_secondary_phone_number country_id , params[:company_information][:secondary_mobile_no_1]
      initial_mobile_number1 = params[:company_information][:secondary_mobile_no]
      params[:company_information][:secondary_mobile_no] = make_secondary_phone_number country_id , params[:company_information][:secondary_mobile_no_1]
     

      info = company.build_company_information(company_params)
    end
    @current_user.company.update!(tax_registration_no: params[:company][:tax_registration_no]) if current_user.cargo_owner? and params[:company][:tax_registration_no].present?
    info.save
    @current_cargo.company.update!(blacklisted: false)

    redirect_to admin_cargo_owners_path , notice: "Cargo created successfully!"
  end

  def set_user
    @user = User.find(params[:id])
  end
  def company_params
    params.require(:company_information).permit(:name ,:industry ,:license_number, :license_expiry_date,:website_address,:address,:trade_registration_no,:trade_registration_expiry,:contact1_first_name,:contact1_last_name,:contact1_mobile,:contact1_email,:contact1_designation,:contact1_office_no,:contact2_first_name,:contact2_last_name,:contact2_mobile,:contact2_email,:contact2_designation,:contact2_office_no,:contact3_first_name,:contact3_last_name,:contact3_mobile,:contact3_email,:contact3_designation,:contact3_office_no,:avatar,documents: [])
  end

  private

  def set_users_link
    @users_link= "active"
  end

  def admin_cargo_params
    params.require(:user).permit(:first_name, :last_name , :email, :mobile, :country_id ,:password)
  end

  def set_attachment_params
    if params[:company_information].present?
      begin
        params[:company_information][:avatar] = eval(params[:company_information][:avatar]).first if params[:company_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
          params[:company_information][:documents] = eval(params[:company_information][:documents].first) if params[:company_information][:documents].present?
      rescue SyntaxError => e
        params[:company_information][:documents] = params[:company_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:company_information][:documents] = params[:company_information][:documents].blank? ? [] : params[:company_information][:documents]
    elsif params[:company_information].present?
      begin
        params[:company_information][:avatar] = eval(params[:company_information][:avatar]).first if params[:company_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
        params[:company_information][:documents] = eval(params[:company_information][:documents].first) if params[:company_information][:documents].present?
      rescue SyntaxError => e
        params[:company_information][:documents] = params[:company_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:company_information][:documents] = params[:company_information][:documents].blank? ? [] : params[:company_information][:documents]
    end
  end


end