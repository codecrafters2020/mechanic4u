class Admin::VendorController < AdminController
  layout 'admin'

  include UserConcern
  before_action :set_user , only: [:show]
  before_action :set_vendors_link  , only: [:index , :show]
  before_action :check_if_super_admin , only: [:new , :create]

  def index
    @users = User.vendor.reverse
  end

  def show
  end

  def new
    @vendor  = User.new
  end

  def create
    country_id = params[:user][:country_id]
    initial_mobile_number = params[:user][:mobile]
    params[:user][:mobile] = make_phone_number country_id , params[:user][:mobile]
    phone_number_valid = phone_number_valid?( country_id,  params[:user][:mobile] )

    params[:user][:password] ="12345678"
    @vendor = User.new(vendor_params)
    @vendor.role = :vendor


    if phone_number_valid and @vendor.save
      @vendor.update(mobile_verified: "verified", verification_code: "",terms_accepted: TRUE)

      redirect_to admin_vendor_index_path , :notice => "Admin created successfully!"
    else
      flash[:error] = "Invalid Phone number." unless phone_number_valid
      render :new
    end

  end

  def set_user
    @user = User.find(params[:id])
  end

  private

  def set_vendors_link
    @vendors_link= "active"
  end

  def vendor_params
    params.require(:user).permit(:first_name, :last_name , :email, :mobile, :country_id ,:password)
  end

end
