class Admin::LocationsController < AdminController
  layout 'admin'

  # include VehicleTypeConcern

  before_action :set_location , only: [:edit, :update , :show , :destroy]
  before_action :set_location_link ,only: [:edit, :update , :show , :index]
  # before_action :set_vehicle_type, only: [:edit , :new, :create , :update]


  def index
    @locations = Location.all
  end
  def new
    @company = Company.find(params[:company_id])
    set_vehicle_type
    @location = Location.new
  end
  def create
    @location = Location.new(location_params)
    if @location.save
      redirect_to admin_cargo_contracts_path(company_id: @location.company.id) , notice: "Contract updated successfully!"
    else
      @company = Company.find(params[:location][:company_id])
      set_vehicle_type
      flash[:error] =   @location.errors.full_messages
      render :new
    end
  end
  def edit
    @company = @location.company
    set_vehicle_type
  end
  def update
    if @location.update(location_params)
      redirect_to admin_cargo_contracts_path(company_id: @location.company.id) , notice: "Contract updated successfully!"
    else
      @company = Company.find(params[:location][:company_id])
      set_vehicle_type
      render :edit
    end
  end
  def show

  end
  def destroy

  end
  private
  def set_location
    @location = Location.find(params[:id])
  end
  def set_location_link
    @locations_link = "active"
  end
  def location_params
    params.require(:location).permit(:pickup_location, :pickup_lat, :pickup_lng, :pickup_city,
                                     :drop_location, :drop_lat, :drop_lng, :drop_city,:rate,
                                     :company_id , :country_id, :pickup_building_name ,
                                     :drop_building_name , :loading_time , :unloading_time,
                                     :vehicle_type_id)
  end
  def set_vehicle_type
    @vehicle_types = VehicleType.where(:country_id => @company.owner.try(:country_id))
  end
end
