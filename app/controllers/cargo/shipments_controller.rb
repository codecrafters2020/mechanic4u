class Cargo::ShipmentsController < CargoController

  include ShipmentConcern
  # include VehicleTypeConcern
  require 'geocoder'
  before_action :set_cargo_shipment, only: [:show,:cancel, :edit, :update, :destroy,:update_destination,:get_coordinates]
  before_action :set_vehicle_type, only: [:edit , :new, :create , :update]
  before_action :set_pickup_time, only: [:create, :update]

  def show
    
    Aws.config.update({
      region: "us-east-1",
      credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
    })
    dynamodb = Aws::DynamoDB::Client.new
    table_name = 'vehicle_coordinates'

    zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")



    if  @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).present?  && @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).present?
      start_time = {"date"=>  @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).strftime("%H:%M") }
      start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
      tz = ISO3166::Country.new(@shipment.country.short_name).timezones.zone_identifiers.first
      start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
      start_timestamp = start_time_csv.utc
      complete_time = {"date"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).strftime("%H:%M") }
      complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
      complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
      complete_timestamp = complete_time_csv.utc
      vehicle_id  = @shipment.shipment_fleets.last.company.vehicles.first.try(:id) if @shipment.shipment_fleets.present? && @shipment.shipment_vehicles.blank?
			vehicle_id = @shipment.shipment_vehicles.first.try(:vehicle_id)  if @shipment.shipment_vehicles.present?
			   
      params = {

        table_name: table_name,
        key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
        expression_attribute_names: {
            "#created" => "created",
            "#vehicle_id" => "vehicle_id"
        },
        expression_attribute_values: {
          ":vehicle_id" => vehicle_id,
          ":start_time" =>   start_timestamp.to_s,
          ":end_time" => complete_timestamp.to_s
        }
      }
      if start_timestamp < complete_timestamp

      @resp = dynamodb.query(params)
      end

        

    elsif @shipment.is_createdby_admin

      vehicle_id  = @shipment.shipment_fleets.last.company.vehicles.first.try(:id)

      params = {

        table_name: table_name,
        key_condition_expression: "#vehicle_id = :vehicle_id ",
        expression_attribute_names: {
          "#vehicle_id" => "vehicle_id"
        },

        expression_attribute_values: {
          ":vehicle_id" => vehicle_id,
        }
      }
      @resp = dynamodb.query(params)
      elsif   @shipment.shipment_vehicles.first.try(:id).present?

      vehicle_id  =   @shipment.shipment_vehicles.first.try(:id)


      params = {
          table_name: table_name,
          key_condition_expression: "#vehicle_id = :vehicle_id ",
          expression_attribute_names: {
            "#vehicle_id" => "vehicle_id"
          },
       
          expression_attribute_values: {
            ":vehicle_id" => vehicle_id,
        }
      }
      @resp = dynamodb.query(params)

    end
    if(current_user.company.company_type == 'individual')
      @individual = true
      else
      @individual=false
      end 
   
    redirect_to root_path, error: "Unauthorized" if @shipment.state != "posted" and @shipment.company.id !=  current_user.company_id
    @title = __method__.capitalize
    instance_variable_set :"@#{@shipment.state}" , "active"
  end

  # GET /cargo/shipments/new
  def new
    instance_variable_set :"@#{__method__}" , "active"
    @title = "Upcoming"

    @shipment = Shipment.new
    if(current_user.company.company_type == 'individual')
      @individual = true
    else
      @individual=false
    end
    shipment = cookies[:shipment] ? eval(cookies[:shipment]) : nil

    @shipment.no_of_vehicles =1;
    if shipment

      shipment = eval(cookies[:shipment])
      # puts " #{@individual}"
      @shipment = Shipment.new(pickup_location: shipment["pickup_location"],drop_location: shipment["drop_location"],
                              vehicle_type_id: shipment["vehicle_type_id"],no_of_vehicles: shipment["no_of_vehicles"],pickup_building_name: shipment["pickup_building_name"],drop_building_name: shipment["drop_building_name"], is_pickup_now: @individual)
      cookies[:shipment] = nil


    end
  end

  # GET /cargo/shipments/1/edit
  def edit

      if(current_user.company.company_type == 'individual')
      @individual = true
      else
      @individual=false
      end 
      @title = __method__.capitalize
      if ["posted"].include?  @shipment.state
        # @shipment.pickup_date = @shipment.pickup_date.strftime("%d/%m/%Y")
        # @shipment.expected_drop_off = @shipment.expected_drop_off.strftime("%d/%m/%Y")
      else
        redirect_back(fallback_location: cargo_root_path)
      end
  end

  def calculate_fare
    @shipment = Shipment.new cargo_shipment_params
    fare = @shipment.prorate_formula
    currency = current_user.try(:country).try(:currency)
    render json: {fare: fare , currency: currency}, status: :ok
  end

  def posted

    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments1 = Shipment.where(company_id: current_user.company_id).posted

    @shipments = Shipment.where(company_id: current_user.company_id).posted

    if params[:query].present?

       if params[:query][:shipment_no].present?
    @shipments = @shipments.where(id: params[:query][:shipment_no])
       end 

       if params[:query][:vehicle_type_id].present?
        @shipments = @shipments.where(vehicle_type_id: params[:query][:vehicle_type_id])  
           end 

      if params[:query][:category].present?

            if params[:query][:category] == "All"

              @shipments =@shipments
            elsif params[:query][:category] == "Intra-Inter City"

              @shipments = @shipments.where('(category =? OR category =?) ',"Inter_City","Intra_City" )
            else 

              @shipments = @shipments.where('category =? ', params[:query][:category] )
            end    
          
          
          end 
        

      if params[:query][:pickup_date_to].present? && params[:query][:pickup_date_from]
            @shipments = @shipments.where("pickup_date <=?  AND pickup_date >=?", params[:query][:pickup_date_to],params[:query][:pickup_date_from] )
               end 


               if params[:query][:pickup_location].present? && params[:query][:drop_location].present?
            
                
                  @shipments = @shipments.where('pickup_location LIKE ? and drop_location LIKE ? ', "%#{params[:query][:pickup_location]}%", "%#{params[:query][:drop_location]}%")
                
                          end    
        
      else
        
      end

      respond_to do |format|
        format.html
       format.xlsx {
              render xlsx: 'cargo/index', filename: "cargo_shipments.xlsx", disposition: 'attachment', stream: true
           }
      end
  
    
  end

  def ongoing
    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments = Shipment.where(company_id: current_user.company_id).ongoing
    @shipments1 = Shipment.where(company_id: current_user.company_id).ongoing

    if params[:query].present?

      if params[:query][:shipment_no].present?
   @shipments = @shipments.where(id: params[:query][:shipment_no])
      end 
      if params[:query][:vehicle_type_id].present?
       @shipments = @shipments.where(vehicle_type_id: params[:query][:vehicle_type_id])  
          end 
          if params[:query][:category].present?

            if params[:query][:category] == "All"

              @shipments =@shipments
            elsif params[:query][:category] == "Intra-Inter City"

              @shipments = @shipments.where('(category =? OR category =?) ',"Inter_City","Intra_City" )
            else 

              @shipments = @shipments.where('category =? ', params[:query][:category] )
            end    
          
          
          end 

     if params[:query][:pickup_date_to].present? && params[:query][:pickup_date_from]
           @shipments = @shipments.where("pickup_date <=?  AND pickup_date >=?", params[:query][:pickup_date_to],params[:query][:pickup_date_from] )
              end 
              if params[:query][:pickup_location].present? && params[:query][:drop_location].present?
            
                
                @shipments = @shipments.where('pickup_location LIKE ? and drop_location LIKE ? ', "%#{params[:query][:pickup_location]}%", "%#{params[:query][:drop_location]}%")
              
                        end    
     else
       
     end
     respond_to do |format|
      format.html
     format.xlsx {
      @shipments = @shipments
            render xlsx: 'cargo/index', filename: "cargo_shipments.xlsx", disposition: 'attachment', stream: true
         }
    end
  end

  def rate_shipment
    if request.post?

    else
      respond_to do |format|
        format.html
        format.js
      end
    end
  end

  def upcoming
    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments = Shipment.where(company_id: current_user.company_id, state: ["accepted", "vehicle_assigned"])
    @shipments1 = Shipment.where(company_id: current_user.company_id, state: ["accepted", "vehicle_assigned"])

    if params[:query].present?

      if params[:query][:shipment_no].present?
   @shipments = @shipments.where(id: params[:query][:shipment_no])
      end 
      if params[:query][:vehicle_type_id].present?
       @shipments = @shipments.where(vehicle_type_id: params[:query][:vehicle_type_id])  
          end 
          if params[:query][:category].present?

            if params[:query][:category] == "All"

              @shipments =@shipments
            elsif params[:query][:category] == "Intra-Inter City"

              @shipments = @shipments.where('(category =? OR category =?) ',"Inter_City","Intra_City" )
            else 

              @shipments = @shipments.where('category =? ', params[:query][:category] )
            end    
          
          
          end 

     if params[:query][:pickup_date_to].present? && params[:query][:pickup_date_from]
           @shipments = @shipments.where("pickup_date <=?  AND pickup_date >=?", params[:query][:pickup_date_to],params[:query][:pickup_date_from] )
              end 
              if params[:query][:pickup_location].present? && params[:query][:drop_location].present?
            
                
                @shipments = @shipments.where('pickup_location LIKE ? and drop_location LIKE ? ', "%#{params[:query][:pickup_location]}%", "%#{params[:query][:drop_location]}%")
              
                        end    
     else
       
     end
     respond_to do |format|
      format.html
     format.xlsx {
            render xlsx: 'cargo/index', filename: "cargo_shipments.xlsx", disposition: 'attachment', stream: true
         }
    end
  end

  def cancel
    respond_to do |format|
      format.js
    end
  end

  def completed
    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments = Shipment.where(company_id: current_user.company_id).where('state =? OR (state=? AND cancel_by=?)', "completed",'cancel','cargo_owner')
    # @shipments = Shipment.where(company_id: current_user.company_id, state: ["completed", "cancel"])
    @shipments1 = Shipment.where(company_id: current_user.company_id).where('state =? OR (state=? AND cancel_by=?)', "completed",'cancel','cargo_owner')

    if params[:query].present?

      if params[:query][:shipment_no].present?
   @shipments = @shipments.where(id: params[:query][:shipment_no])
      end 
      if params[:query][:vehicle_type_id].present?
       @shipments = @shipments.where(vehicle_type_id: params[:query][:vehicle_type_id])  
          end 
          if params[:query][:category].present?

            if params[:query][:category] == "All"

              @shipments =@shipments
            elsif params[:query][:category] == "Intra-Inter City"

              @shipments = @shipments.where('(category =? OR category =?) ',"Inter_City","Intra_City" )
            else 

              @shipments = @shipments.where('category =? ', params[:query][:category] )
            end    
          
          
          end 
        

     if params[:query][:pickup_date_to].present? && params[:query][:pickup_date_from]
           @shipments = @shipments.where("pickup_date <=?  AND pickup_date >=?", params[:query][:pickup_date_to],params[:query][:pickup_date_from] )
              end 

              if params[:query][:pickup_location].present? && params[:query][:drop_location].present?
            
                @shipments = @shipments.where('lower(pickup_location) LIKE ? and lower(drop_location) LIKE ? ', "%#{params[:query][:pickup_location].downcase}%", "%#{params[:query][:drop_location].downcase}%")
              
                        end    
     else
       
     end
     respond_to do |format|
      format.html
     format.xlsx {
            render xlsx: 'cargo/index', filename: "cargo_shipments.xlsx", disposition: 'attachment', stream: true
         }
    end
  end
  def update_destination
    if request.post? or request.patch?
      old_destination = @shipment.drop_location
      @change_destination_request = ChangeDestinationRequest.new(
          lat: params[:shipment][:drop_lat],
          lng: params[:shipment][:drop_lng],
          city: params[:shipment][:pickup_city],
          address: params[:shipment][:drop_location],
          drop_building_name: params[:shipment][:drop_building_name],
          shipment_id: @shipment.id )
      if @change_destination_request.save
        ShipmentMailer.change_destination_mail_to_admin(@shipment, old_destination, @change_destination_request.address)
        redirect_back(fallback_location: cargo_shipment_path(@shipment)) #, notice: "Shipment updated Successfully!"
        # render 'show'
      else
        render json: { errors:  @shipment.errors.full_messages }, status: :bad_request
      end
    else
      respond_to do |format|
        format.html
        format.js
      end
    end
  end
  
  def pickup_location
  end
  
  
  def vehicle_type
     @vehicles = VehicleType.eager_load(:avatar_attachment).where(:country_id => current_user.try(:country_id)).order(code: :desc)
    respond_to do |format|
      format.html
      format.js
    end

  end

  
  def image_select
     
  @vehicle =""
  @pickup_time = ""
  @pickup_building_name = ""
  @drop_building_name = ""
  @pickup_date = ""
  @loading_time = ""
  @unloading_time = ""
  @vehicle_type_id = ""
  @no_of_vehicles = ""
  @cargo_description =""
  @cargo_packing_type = ""
  @payment_option = ""
  @address =""
  @drop_address =""
  @latitude = ""
  @longitude = ""
  @dropoff_lat= ""
  @dropoff_long= ""
  @city= ""
  @dcity=""

 
  @vehicle =params[:vehicle_id]
  @pickup_time = params[:pickup_time]
  @pickup_building_name = params[:pickup_building_name]
  @drop_building_name = params[:drop_building_name]
  @pickup_date = params[:pickup_date]
  @loading_time = params[:loading_time]
  @unloading_time = params[:unloading_time]
  @vehicle_type_id = params[:vehicle_type_id]
  @no_of_vehicles = params[:no_of_vehicles]
  @cargo_description = params[:cargo_description]
  @cargo_packing_type = params[:cargo_packing_type]
  @payment_option = params[:payment_option]
  @address =params[:address]
  @drop_address =params[:drop_address]
  @latitude = params[:latitude]
  @longitude = params[:longitude]
  @dropoff_lat= params[:dropoff_lat]
  @dropoff_long= params[:dropoff_long]
  @city= params[:city]
  @dcity= params[:dcity]


  redirect_to new_cargo_shipment_path(:vehicle=>@vehicle ,:pickup_building_name => @pickup_building_name,:drop_building_name => @drop_building_name,:pickup_date => @pickup_date,:loading_time => @loading_time,:unloading_time => @unloading_time,:no_of_vehicles => @no_of_vehicles,:cargo_description => @cargo_description,:cargo_description => @cargo_description,:cargo_packing_type => @cargo_packing_type,:payment_option => @payment_option,:address => @address,:drop_address => @drop_address,:latitude => @latitude,:longitude => @longitude,:dropoff_lat => @dropoff_lat,:dropoff_long => @dropoff_long,:city => @city,:dcity => @dcity)
   end
  
  def dropoff_location
    # data = @shipment.shipment_vehicles.where(status: ShipmentVehicle::ONGOING_VEHICLES).map{|sv| {lat: (sv.vehicle.latitude.to_f rescue ""), lng: (sv.vehicle.longitude.to_f rescue ""), status: (sv.status.humanize rescue "") ,id: sv.id.to_s} }
    # render json: { coordinates:  data}, status: :ok
   # render json: { coordinates:  get_shipment_coordinates(@shipment.id)}, status: :ok
  end

  def get_coordinates
    # data = @shipment.shipment_vehicles.where(status: ShipmentVehicle::ONGOING_VEHICLES).map{|sv| {lat: (sv.vehicle.latitude.to_f rescue ""), lng: (sv.vehicle.longitude.to_f rescue ""), status: (sv.status.humanize rescue "") ,id: sv.id.to_s} }
    # render json: { coordinates:  data}, status: :ok
    render json: { coordinates:  get_shipment_coordinates(@shipment.id)}, status: :ok
  end

  def get_bulk_coordinates
    # @shipments = current_user.shipments.includes(:vehicle_type,:shipment_vehicles)
    
    @shipments = Shipment.all.where('state =? AND company_id=? ', "ongoing", current_user.company_id )

    if params[:type].present?
  
        if params[:type] == "All"
          @shipments = Shipment.all.where('state =? AND company_id=? ', "ongoing", current_user.company_id )
        elsif params[:type] == "Intra-Inter City"
          @shipments = Shipment.all.where('state =? AND company_id=? AND (category =? OR category =?) ', "ongoing", current_user.company_id,"Inter_City","Intra_City" )
        else 
          @shipments = Shipment.all.where('state =? AND company_id=? AND category =? ', "ongoing", current_user.company_id, params[:type] )
        end

  
  
      
      end

      if params[:pickup_location].present? && params[:drop_location].present?
          
        @shipments = @shipments.where('lower(pickup_location) LIKE ? and lower(drop_location) LIKE ? ', "%#{params[:pickup_location].downcase}%", "%#{params[:drop_location].downcase}%")

        # @shipments = @shipments.where('pickup_location LIKE ? and drop_location LIKE ? ', "%#{params[:pickup_location]}%", "%#{params[:drop_location]}%")
      
                end    

    render json: { coordinates:  get_shipment_bulk_cargo_coordinates(@shipments) }, status: :ok
  end


  def get_bulk_filtered_coordinates
    # @shipments = current_user.shipments.includes(:vehicle_type,:shipment_vehicles)
    
    @index = params[:index]



     @shipments = Shipment.includes(:vehicle_type,:shipment_vehicles).where(id:  params[:shipment_id])
      # puts "\n\n\n\t\t \n\n\n\t\t \n\n\n\t\t \n\n\n\t\  @shipments cpiunt  #{  @shipments.id} \n\n"

     render json: { coordinates:  get_shipment_bulk_cargo_coordinates(@shipments) }, status: :ok
  end


  def eta_update()
  
    @shipment = Shipment.find(params[:id])

    @pickup_lat =params[:pickup_lat]

    @pickup_lng =params[:pickup_lng]

    @id =params[:id]


    @dropoff_lat =params[:dropoff_lat]

    @dropoff_lng =params[:dropoff_lng]


    @vehicle = @shipment.shipment_vehicles.where(status: ShipmentVehicle::ONGOING_VEHICLES).map{|sv| {vehicle_name: sv.vehicle.vehicle_name ,lat: (sv.vehicle.latitude.to_f rescue ""), lng: (sv.vehicle.longitude.to_f rescue ""), status: (sv.status.humanize rescue "")  ,id: sv.id.to_s} }

    @vehicle_lat= @vehicle[0][:lat]

    @vehicle_lng= @vehicle[0][:lng]
    @status= @vehicle[0][:status]

    if (@vehicle[0][:status].to_s == 'Start' ) 
      @lat = @pickup_lat
      @lng = @pickup_lng
    else
      @lat = @dropoff_lat
      @lng = @dropoff_lng

    end

    # url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{@vehicle_lat.to_f},#{@vehicle_lng.to_f}&destinations=#{@lat.to_f},#{@lng.to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
		# response = HTTParty.get(url)
		# distance = response["rows"].first["elements"].first["duration"].blank? ? 0 : (response["rows"].first["elements"].first["duration"]["text"] )  rescue ""
  

       render json: { coordinates: 0 }, status: :ok
  end
 
  def update_timer()

    @update_at =params[:shipment_updated]

    start_time = Time.parse(@update_at.to_s).to_i
    end_time = Time.now.utc.to_i
    @elapsed_seconds = ((end_time - start_time))

              
    render json: { coordinates: @elapsed_seconds }, status: :ok


  end



  def update_bids()
    @status =false
    @id =params[:id]
    @shipment = Shipment.find(params[:id])
    @previous_bids =params[:shipment_bids]
     @updated_bids = @shipment.bids.length
     @updated_bid = @shipment.bids
     @updated_bid.each do |i|
      if i.updated_at != i.created_at 
        start_time = Time.parse(i.updated_at .to_s).to_i
        end_time = Time.now.utc.to_i

        @elapsed_seconds = ((end_time - start_time))

        if(@elapsed_seconds)<15
            @status =true
        end

              
       end
     end
     if(@status)
      render json: { update: "rebid" }, status: :ok

     elsif (@previous_bids.to_i != @updated_bids.to_i)
      render json: { update: "true" }, status: :ok
  else
    render json: { update: "false" }, status: :ok
  end
  end

  def update_upcoming()

    @id =params[:id]
    @shipment = Shipment.find(params[:id])
    @shipment_status = @shipment.state
     
    if (@shipment_status.to_s == "cancel")
      render json: { status: "cancel" }, status: :ok
  elsif  (@shipment_status.to_s == "ongoing")
    render json: { status: "ongoing" }, status: :ok
  elsif  (@shipment_status.to_s == "cancel")
    render json: { status: "cancel" }, status: :ok
  else
    render json: { status: "accepted" }, status: :ok
  end
  end

  def no_bids()
    @id =params[:id]
    @shipment = Shipment.find(params[:id])
        if(current_user.company.company_type == 'individual')
          start_time = Time.parse(@shipment.updated_at.to_s).to_i
        end_time = Time.now.utc.to_i
        @elapsed_seconds = ((end_time - start_time))
                #  @limit = 600
                #  if @elasped_seconds.to_i > @limit.to_i
        ShipmentMailer.bidless_shipment(@id).deliver_now
        end
      end
  

  def no_selected()

    @id =params[:id]
        @shipment = Shipment.find(params[:id])

        if(current_user.company.company_type == 'individual')
          start_time = Time.parse(@shipment.updated_at.to_s).to_i
          end_time = Time.now.utc.to_i
          @elapsed_seconds = ((end_time - start_time))
          # @limit = 600
          # if @elasped_seconds.to_i > @limit.to_i
         ShipmentMailer.no_bids_accepted(@id).deliver_now
        end
      end
  

  def create
    # format_date_fields

    @shipment = Shipment.new(cargo_shipment_params)
    set_contract_fields if cargo_shipment_params[:location_id].present?
    @shipment.country_id = current_user.country_id


    if !cargo_shipment_params[:pickup_building_name].present?
      @shipment.pickup_building_name = "not specified"
    end
    if(current_user.company.company_type == 'individual')
      @shipment.is_pickup_now=true;
      else
        @shipment.is_pickup_now=false;
      end 
      @shipment.unloading_time =cargo_shipment_params[:loading_time] ;
      @shipment.payment_option="credit_period"

      if(current_user.company.company_type == 'individual')
      @shipment.pickup_time=Time.now ;
      @shipment.pickup_time=  @shipment.pickup_time+60;
      @shipment.pickup_date=DateTime.now.to_time.utc;
      @shipment.cargo_packing_type = "null"
      @shipment.payment_option="Before pickup"
      end

    if !cargo_shipment_params[:drop_building_name].present?
      @shipment.drop_building_name = "not specified"
    end

    respond_to do |format|
      if @shipment.save
        ShipmentMailer.new_shipment( @shipment.id ).deliver_now 

        format.html { redirect_to posted_cargo_shipments_path, notice: 'Shipment was successfully created.' }
        format.json { render :show, status: :created, location: @shipment }
      else
        flash[:error] = @shipment.errors.full_messages
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /cargo/shipments/1
  # PATCH/PUT /cargo/shipments/1.json
  def update
    if(current_user.company.company_type == 'individual')
    
      @shipment.pickup_time = Time.now ;
      params[:shipment][:pickup_time] =  @shipment.pickup_time+60;
      params[:shipment][:pickup_date] = DateTime.now.to_time.utc;
      params[:shipment][:cargo_packing_type] = "null"
      params[:shipment][:payment_option] ="Before pickup"

    end
    # format_date_fields
    respond_to do |format|
      if @shipment.update(cargo_shipment_params)

        @shipment.set_contract_fields
        @shipment.bids.destroy_all 
        @shipment.update_attributes(is_expired: "false")
        # unless @shipment.state == "cancel"
        if @shipment.state == "posted"
        @shipment.shipment_posted_notifications
        end
        format.html { redirect_to posted_cargo_shipments_path, notice: 'Shipment was successfully updated.' }
        format.json { render :show, status: :ok, location: @shipment }
      else
        flash[:error] = @shipment.errors.full_messages
        format.html { render :edit }
      end
    end
  end
  def destroy
    @shipment.destroy
    respond_to do |format|
      format.html { redirect_to cargo_shipments_url, notice: 'Shipment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share copoff_lat=ommon setup or constraints between actions.
    def set_cargo_shipment

      # shipment_country_access_control(params[:id])
      @shipment = Shipment.find(params[:id])

      
    end

    # Never trust parameters from the scary internet, only allow the white list through.

    def cargo_shipment_params


      params.require(:shipment).permit(:cancel_reason, :cancel_by,:amount_per_vehicle ,:state_event, :state, :id,:company_id, :country_id, :pickup_location, :pickup_date, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :payment_option, :pickup_building_name , :drop_building_name)
    end

    def format_date_fields
      params[:shipment][:pickup_date] = Date.strptime(params[:shipment][:pickup_date], "%d/%m/%Y")
      # params[:shipment][:expected_drop_off] = Date.strptime(params[:shipment][:expected_drop_off], "%d/%m/%Y") if params[:shipment][:expected_drop_off].present?
    end

    def set_pickup_time
      params[:shipment][:pickup_time] = set_time_zone(params[:shipment][:pickup_time]) if params[:shipment][:pickup_time].present?
    end

    def set_vehicle_type
      @vehicle_types = VehicleType.where(:country_id => current_user.try(:country_id)).order(:name)
      

    end

    def set_contract_fields

      location = Location.find(cargo_shipment_params[:location_id])
      @shipment.pickup_location = location.pickup_location
      @shipment.drop_location = location.drop_location
      @shipment.loading_time = location.loading_time
      @shipment.unloading_time= location.unloading_time
      @shipment.vehicle_type_id= location.vehicle_type_id
    end
end
