class Cargo::DashboardController < CargoController
  before_action :authenticate_user!
  before_action :check_if_cargo_owner
  def index
    country = ISO3166::Country.find_country_by_name(current_user.country.name) rescue ""
    @user_country_coordinates = {lat: country.latitude , lng: country.longitude}
    @shipments = current_user.shipments
    @shipment_dashboard = Shipment.all.where('state =? AND company_id=? ', "ongoing", current_user.company_id )

    if params[:query].present?
    if params[:query][:category].present? 

      if params[:query][:category] == "All"
        @shipment_dashboard = Shipment.all.where('state =? AND company_id=? ', "ongoing", current_user.company_id )
      elsif params[:query][:category] == "Intra-Inter City"
        @shipment_dashboard = Shipment.all.where('state =? AND company_id=? AND (category =? OR category =?) ', "ongoing", current_user.company_id,"Inter_City","Intra_City" )
      else 
        @shipment_dashboard = Shipment.all.where('state =? AND company_id=? AND category =? ', "ongoing", current_user.company_id, params[:query][:category] )
      end
    end

    if params[:query][:pickup_location].present? && params[:query][:drop_location].present?
          
      @shipment_dashboard = @shipments.where('lower(pickup_location) LIKE ? and lower(drop_location) LIKE ? ', "%#{params[:query][:pickup_location].downcase}%", "%#{params[:query][:drop_location].downcase}%")

      # @shipment_dashboard = @shipment_dashboard.where('pickup_location LIKE ? and drop_location LIKE ? ', "%#{params[:query][:pickup_location]}%", "%#{params[:query][:drop_location]}%")
    
              end    

  end
end

  
end
