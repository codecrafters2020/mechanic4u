class Api::V1::Cargo::ShipmentsController < Api::V1::ApiController

  before_action :verify_jwt_token
  before_action :set_shipment, only: [:show, :edit, :update, :destroy]

  # GET /shipments
  # GET /shipments.json
  def index
    @shipments = Shipment.all
    render json: @shipments, status: :ok
  end

  # GET /shipments/1
  # GET /shipments/1.json
  def show
  end

  # GET /shipments/new
  def new
    @shipment = Shipment.new
  end

  # GET /shipments/1/edit
  def edit
  end

  # POST /shipments
  # POST /shipments.json
  def create
    @shipment = Shipment.create shipment_params
    puts @shipment.errors.full_messages
    ShipmentMailer.new_shipment( @shipment.id ).deliver_now if  @shipment.errors.blank?

    @shipment.errors.blank? ? ( render "create" , status: :ok ) : (render json: { errors: @shipment.errors.full_messages }, status: :bad_request)
  end

  # PATCH/PUT /shipments/1
  # PATCH/PUT /shipments/1.json
  def update
    if @shipment.update(shipment_params)
			@shipment.bids.destroy_all
			@shipment.update_attributes(is_expired: "false")
      if @shipment.state == "posted"
        @shipment.shipment_posted_notifications
      end
      render json: {update: true, status: :ok}
    else
      puts @shipment.errors.full_messages
      render json: { errors:  @shipment.errors.full_messages }, status: :bad_request
    end
  end

  # DELETE /shipments/1
  # DELETE /shipments/1.json
  def destroy
    @shipment.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  def calculate_fare
    @shipment = Shipment.new shipment_params
    fare = @shipment.prorate_formula
    distance = @shipment.distance_between_start_and_end_point
    render json: {fare: fare, distance: distance}, status: :ok
  end

  def posted
    @shipments = Shipment.where(company_id: params[:company_id]).posted.page params[:page]
  end

  def ongoing
    @shipments = Shipment.where(company_id: params[:company_id]).ongoing.page params[:page]
  end

  def upcoming
    @shipments = Shipment.where(company_id: params[:company_id].to_i, state: ["accepted", "vehicle_assigned"]).page params[:page]
  end


  def completed
    @shipments = Shipment.where(company_id: params[:company_id].to_i).where('state =? OR (state=? AND cancel_by=?)', "completed",'cancel','cargo_owner').page params[:page]

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_shipment
      @shipment = Shipment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def shipment_params
      params.require(:shipment).permit(:pickup_building_name, :drop_building_name, :amount_per_vehicle, :cancel_reason, :cancel_by, :state_event,:payment_option, :state, :id,:company_id, :country_id, :pickup_location, :pickup_date, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :is_pickup_now)
    end
end
