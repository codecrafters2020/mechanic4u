class Api::V1::Cargo::BidsController < Api::V1::ApiController
  before_action :set_bid, only: [:show, :edit, :update, :destroy,:accept,:reject]

  # GET /bids
  # GET /bids.json
  def index
    @bids = Bid.all
  end

  # GET /bids/1
  # GET /bids/1.json
  def show
  end

  # GET /bids/new
  def new
    @bid = Bid.new
  end

  # GET /bids/1/edit
  def edit
  end

  # POST /bids
  # POST /bids.json
  def create
    @bid = Bid.new(bid_params)

    respond_to do |format|
      if @bid.save
        format.html { redirect_to @bid, notice: 'Bid was successfully created.' }
        format.json { render :show, status: :created, location: @bid }
      else
        format.html { render :new }
        format.json { render json: @bid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bids/1
  # PATCH/PUT /bids/1.json
  def update
    respond_to do |format|
      if @bid.update(bid_params)
        format.html { redirect_to @bid, notice: 'Bid was successfully updated.' }
        format.json { render :show, status: :ok, location: @bid }
      else
        format.html { render :edit }
        format.json { render json: @bid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bids/1
  # DELETE /bids/1.json
  def destroy
    @bid.destroy
    respond_to do |format|
      format.html { redirect_to bids_url, notice: 'Bid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  def accept
    @shipment = Shipment.find_by_id params[:shipment_id]
    if @shipment.process == "prorate"
      @shipment.update_attributes(state_event: "accepted", fleet_id: @bid.company_id)
    else
      @shipment.update_attributes(state_event: "accepted", fleet_id: @bid.company_id, amount_per_vehicle: @bid.amount / @shipment.no_of_vehicles, amount: @bid.amount, detention: @bid.detention * @shipment.no_of_vehicles, detention_per_vehicle: @bid.detention,fleet_net_rate: @bid.amount ,customer_net_amount: @bid.amount ,lorryz_share_amount: 0 )
      ShipmentFleet.create(shipment_id: @shipment.id, fleet_cost: @bid.amount,company_id: @bid.company_id)  

    end
    @bid.status = :accepted
    if @bid.save
      user = @shipment.fleet.fleet_owner
      Notification.create(
        notifiable_id: @shipment.id, 
        notifiable_type: "Shipment", 
        body: "Shipment won: Your bid is accepted on Shipment # #{@shipment.id}",
        user_mentioned_id: user.id)


        if(@shipment.is_pickup_now)    
      PushNotification.send_notification(
       user.os,
       user.device_id,
       "",
       @shipment.id,
       "Shipment won: Your bid is accepted on Shipment # #{@shipment.id}",
       "filhall_violin",
      ) 
    else
      PushNotification.send_notification(
        user.os,
        user.device_id,
        "",
        @shipment.id,
        "Shipment won: Your bid is accepted on Shipment # #{@shipment.id}",
       nil,
       ) 
      end
      
      
      
      
      if user.device_id
      # shipment.update_attribute(state_event: "accepted")
      shipment.bids.where.not(id: @bid.id).update_all(status: :rejected)
    end
  end
end

  def reject
    @shipment = Shipment.find_by_id params[:shipment_id]
    @bid.status = :rejected
    @bid.save
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bid
      @bid = Bid.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bid_params
      params.require(:bid).permit(:company_id, :amount, :status, :detention, :shipment_id)
    end

    def company 
      company = Company.find(params[:company_id])
    end
    def shipment
      company.shipments.find_by_id(params[:shipment_id])
    end
end
