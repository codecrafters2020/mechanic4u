class Api::V1::Fleet::DriversController < Api::V1::ApiController
	before_action :set_vehicle, only: [:show,:destroy]
	before_action :set_shipment, only: [:update_vehicle_status,:report_problem,:payment_received]

	def index		
		if params[:assignment]
			@drivers =  User.includes(:vehicle).where({vehicles: {user_id: nil}}).where(:company_id => @user.company_id,role:User.role.find_value(:driver).value)
			if params[:driver_id].present? && params[:driver_id] != 'null'
				current_assigned_driver = [User.find(params[:driver_id])]
				@drivers = @drivers | current_assigned_driver
			end
		else
			@drivers = User.where(:company_id => @user.company_id,role:User.role.find_value(:driver).value)
		end

		render 'index', status: :ok
	end

	def register_as_driver
		full_name = params[:driver][:full_name]
		firstname = full_name.split(" ")[0] rescue ''
		lastname = full_name.split(" ").drop(1).join("") rescue ''		
		mobile = params[:driver][:mobile]

		user = User.find_by_mobile(mobile)

		if user
			render json: {error: "Mobile number has already been taken"}, status: :unprocessable_entity	
		else
			
			password = (0...8).map { ('a'..'z').to_a[rand(26)] }.join
			@driver = User.new(:mobile => mobile, :first_name=> firstname,:last_name => lastname,
								:password => password, :password_confirmation => password,:role => User.role.find_value(:driver).value,
								:company_id => @user.company_id,:country_id => @user.country_id)

			@driver.build_driver_information(driver_information_params)
			@driver.save
			@driver.driver_login_sms()
			render json: @driver, status: :ok	
		end
	end

	def show
		
		if @driver = User.find_by_id(params[:id])
			render 'show', status: :ok
		else
			render json: {error: "Driver not found" },status: :ok			
		end
	end

	def update
		full_name = params[:driver][:full_name]
		firstname = full_name.split(" ")[0] rescue ''
		lastname = full_name.split(" ").drop(1).join("") rescue ''	
		
		mobile = params[:driver][:mobile]
		@driver = User.find_by_mobile(mobile)

		if @driver

			driver = @driver.update_attributes( 
								:first_name=> firstname,
								:last_name => lastname,
								:company_id => @user.company_id,
								:country_id => @user.country_id,
								password: params[:driver][:password],
								password_confirmation: params[:driver][:confirm_password]
								)
			driver_information = @driver.driver_information
			driver_information.update_attributes!(driver_information_params)
			
			if driver && driver_information
			  render "show" , status: :ok 
			else
			  render json: @driver.errors.full_messages, status: :unprocessable_entity 
			end	
		else
			render json:{"error": "Driver not found"}  , status: :ok 
		end
	end

	def destroy
	  @driver.destroy
	  respond_to do |format|
	    format.json { head :no_content }
	  end
	end


	def upcoming_shipments
		@shipments = @user.assigned_shipments.vehicle_assigned.uniq
		@shipments = paginate(@shipments)
		render 'driver_shipments', status: :ok
	end


	def ongoing_shipments
		@shipments = @user.assigned_shipments.ongoing.uniq
		@shipments = paginate(@shipments)
		render 'driver_shipments', status: :ok
	end

	def completed_shipments
		@shipments = @user.assigned_shipments.completed.uniq
		@shipments = paginate(@shipments)
		render 'driver_shipments', status: :ok
	end

	def paginate(shipments)
		Kaminari.paginate_array(shipments).page(params[:page]).per(5)
	end

	def update_vehicle_status
		
		if route_inprogress?

			@shipment.ongoing! unless @shipment.ongoing?
			if  @shipment.status.blank?
			@shipment.update_attributes(status: "Start")
			ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Start", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"))  
			end
			

			
			update_transport_status
			render_status({shipment: @shipment})
		elsif route_completed?
			shipment_distance_from_vehicle <= 5 ? shipment_completed! : render_status({error: "Shipment is not within 5km to destination"})
		end
	end

	def report_problem

		fleet_owner = @shipment.fleet.fleet_owner
		admin = User.find_by_email("admin@lorryz.com")

		@user.report_problem(fleet_owner,admin,@shipment)
		render json: {success: "Pasword has been sent your phone number"}
	end

	def payment_received
		@shipment.paid_by_cargo = true
		@shipment.paid_to_fleet = true
		@shipment.save
		render json: {success: "Payment Succesfully received"}
	end

	private
	  # Use callbacks to share common setup or constraints between actions.
	  def set_vehicle
	    @driver = User.find_by(id: params[:id])
	end

	def driver_information_params
	  params.require(:driver).permit(:national_id, :expiry_date, :license, :license_expiry, :user_id, {license_documents: []}, {emirate_documents: []}, :avatar)
	end

	def set_shipment
		@shipment = Shipment.find_by(id: params[:shipment_id])
	end

	def route_inprogress?
		statues = ['start','loading','enroute','unloading']
		statues.include?(params["status"])
	end

	def route_completed?
		params["status"] == 'finish'
	end

	def shipment_completed!
		update_transport_status
		
		if @shipment.all_vehicles_at_destination?
			@shipment.update_attributes(state_event: "completed") unless @shipment.completed? 
			@shipment.update_attributes(status: "completed")
			ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "complete", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"))  
			ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "completed", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"))  


		end
		
		render_status({shipment: @shipment})
	end

	def shipment_distance_from_vehicle
		@shipment.distance_from_vehicle(params[:vehicle_id])
	end

	def update_transport_status
		@shipment.shipment_vehicles.where(vehicle_id: params[:vehicle_id]).update(status: params[:status])
		if params[:status] == "loading" && @shipment.status != "At Loading"
			@shipment.update_attributes(status: "At Loading")
			ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Loading", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"))  
		end
		if params[:status] == "enroute" && @shipment.status != "Enroute to Destination"
			@shipment.update_attributes(status: "Enroute to Destination") 
			ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "Enroute to Destination", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"))  
		end
		if params[:status] == "unloading" && @shipment.status != "At Offloading Point"
			@shipment.update_attributes(status: "At Offloading Point")
			ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "At Offloading Point", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time:convert_to_shipment_country_timezone(@shipment.country , Time.now.utc).strftime("%I:%M %p"))  
		end
			
	    
	
	end

	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end

	def render_status(status)
		render json: status  , status: :ok 
	end

end