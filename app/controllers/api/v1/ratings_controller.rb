class Api::V1::RatingsController < Api::V1::ApiController
  
  def create
	  @company_ratings = CompanyRating.new(company_rating_params)
	  
	  if @company_ratings.save!
	  	
	    render json: { success:  'shipment rated successfully' }, status: :ok
	  else
	  	render json: { errors:  @company_ratings.errors.full_messages }, status: :bad_request
	  end  	
  end

  def show
  end


  private
  	def company_rating_params
  	  params.require(:company_rating).permit(:rating, :remarks,:shipment_id,:giver_id,:receiver_id)
  	end
  		
  		
end


