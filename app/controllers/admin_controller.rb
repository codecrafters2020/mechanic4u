class AdminController < ActionController::Base
  layout 'admin'
  before_action :authenticate_user!
  before_action :check_if_admin

  include UserConcern

  def check_if_admin
    redirect_to root_path unless current_user and admin_or_superadmin?(current_user)
  end
  def check_if_super_admin
    redirect_to root_path unless current_user and  current_user.superadmin?
  end
end
