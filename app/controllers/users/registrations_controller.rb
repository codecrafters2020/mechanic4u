class Users::RegistrationsController < Devise::RegistrationsController
  prepend_before_action :require_no_authentication, only: [:new, :create, :cancel,:gps_coordinates]
  prepend_before_action :authenticate_scope!, only: [:edit, :update, :destroy]
  prepend_before_action :set_minimum_password_length, only: [:new, :edit]
  protect_from_forgery :except => [:gps_coordinates]
  skip_before_action :verify_authenticity_token, only: [:gps_coordinates]
  include UserConcern

  # GET /resource/sign_up
  def new
    cookies[:shipment] = params if params
    build_resource
    @company = resource.build_company
    yield resource if block_given?
    respond_with resource
  end

  # POST /resource
  def create
    country_id = params[:user][:country_id]
    initial_mobile_number = params[:user][:mobile]
    params[:user][:mobile] = make_phone_number country_id , initial_mobile_number

    build_resource(sign_up_params)
    phone_number_valid = phone_number_valid?( country_id,  params[:user][:mobile] )
    resource.build_company_type(params) if phone_number_valid and params[:user][:company_attributes].present? and resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication? and resource.mobile_verified?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        unless resource.mobile_verified?
          redirect_to users_mobile_verification_path(unverified_user_id: resource)
        else
          set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
          expire_data_after_sign_in!
          respond_with resource, location: after_inactive_sign_up_path_for(resource)
        end
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      flash[:error] = resource.errors.full_messages
      unless phone_number_valid
        params[:user][:mobile] = initial_mobile_number
        flash[:error] << "Invalid Phone Number"
      end
      @company = resource.build_company
      render :new
      # respond_with resource
    end
  end
  def mobile_verification
    params[:verification_code] = params[:verification_code].split(" ").join if params[:verification_code].present?
    @unverified_user = User.find(params[:unverified_user_id])
    if request.post?
      if params[:verification_code] == @unverified_user.verification_code
        @unverified_user.update(mobile_verified: "verified", verification_code: "")
        resource = @unverified_user.id
        redirect_to user_session_path , notice: "Your code is verified, Please Sign in here"
      end
    end
  end


  def gps_coordinates
    @prams = params[:_json].first

    @gps =GpsVehicle.find_by_imei(@prams['device.name'])
  
    if @gps.vehicle_id.present?
     @vehicle = Vehicle.find_by_id(@gps.vehicle_id) 
     @vehicle.update_attributes(latitude: @prams['position.latitude'],longitude: @prams['position.longitude'],coordinates_updated_at:DateTime.now())   if  @prams['position.latitude'].present?
   Aws.config.update({
      region: "us-east-1",
      credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')     
    })
    
    dynamodb = Aws::DynamoDB::Client.new
    
    table_name = 'vehicle_coordinates'
    vehicle_id = @gps.vehicle_id
    if  @prams['position.latitude'].present?
    lat =@prams['position.latitude']
    lng = @prams['position.longitude']

    item = {
      vehicle_id: vehicle_id,
      created: DateTime.now.utc.to_s,
      lat: lat,
      lng: lng,
       
    }  
     params = {
        table_name: table_name,
        item: item
    }
    
    begin
        dynamodb.put_item(params)

    
    rescue  Aws::DynamoDB::Errors::ServiceError => error
        puts "Unable to add item:"
        puts "#{error.message}"
    end

  end

  end

    render json: { success:  'Coordinates updated successfulyy successfully' }, status: :ok



end

  def send_verification_code

    @user = User.find_by_id(params[:user_id])
    if @user.present? and !@user.mobile_verified?
      @user.assgin_verification_code
      @user.send_verification_code
      flash[:notice] = "Verification code sent"
    end
    respond_to do |f|
      f.js
    end
  end
  # GET /resource/edit
  def edit
    @user = current_user
    render :edit , layout: current_user.role.split("_").first
  end

  # PUT /resource
  # We need to use a copy of the resource because we don't want to change
  # the current user in place.
  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      bypass_sign_in resource, scope: resource_name
      respond_with resource, location: after_update_path_for(resource)
    else
      clean_up_passwords resource
      set_minimum_password_length
      # respond_with resource
      render :edit , layout: current_user.role.split("_").first
    end
  end


  protected
  # The path used after sign up for inactive accounts. You need to overwrite
  # this method in your own RegistrationsController.
  def after_inactive_sign_up_path_for(resource)
    scope = Devise::Mapping.find_scope!(resource)
    router_name = Devise.mappings[scope].router_name
    context = router_name ? send(router_name) : self
    context.respond_to?(:new_user_registration) ? context.root_path : "/users/sign_up"
  end

  # The default url to be used after updating a resource. You need to overwrite
  # this method in your own RegistrationsController.
  def after_update_path_for(resource)
    return dashboard_index_path
    # signed_in_root_path(resource)
  end

end
