class BlogsController < ApplicationController
    before_action :set_blog , only: [:destroy]
    before_action :set_blog_link , only: [ :index ]
    def index
      @blogs = Blog.all.order(created_at: :desc)
      respond_to do |format|
        format.html
        format.xlsx {
          start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
          end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
          @blogs = Blog.where(created_at: start_date..end_date)
          render xlsx: 'xlsx_blogs', filename: "all_blogs.xlsx", disposition: 'attachment'
        }
      end
    end
    def new
      @blog = Blog.new
    end
    def show
      
      @blog = Blog.find(params[:id])
    end
    private
    def set_blog
        @blog = Blog.find(params[:id])
      end
    def blog_params
      params.require(:blog).permit(:title,:initial_summary , :description, :avatar)
    end
    def set_blog_link
      @blog_link = "active"
    end
  end
  