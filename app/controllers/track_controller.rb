class TrackController < ApplicationController
	include ShipmentConcern
	def index
		if params[:code]
			@shipment = Shipment.find_by_tracking_code params[:code]
			if @shipment
				redirect_to track_path(@shipment.tracking_code)
			else
				render :index, error: "You entered wrong tracking code"
			end
		end
	end

	def show
		@shipment = Shipment.find_by_tracking_code params[:id]
	end

	def get_coordinates
		@shipment = Shipment.find_by_id params[:id]
		render json: { coordinates:  get_shipment_coordinates(@shipment.id)}, status: :ok
  end


end
