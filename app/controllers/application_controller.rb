class ApplicationController < ActionController::Base

	# We depend on our auth_token module here.
		require 'auth_token'

		# Prevent CSRF attacks by raising an exception.
		# For APIs, you may want to use :null_session instead.
		# protect_from_forgery with: :null_session
		before_action :configure_permitted_parameters, if: :devise_controller?
		
		alias_method :devise_current_user, :current_user
		
		def current_user
		  if request.headers['Authorization'].blank?
		    devise_current_user
		  else
		    User.fetch_user(request.headers['Authorization'].split(' ').last)
		  end   
		end

		
		protected

			
			
		  def verify_jwt_token
		  	if request.headers['Authorization'].blank? or !AuthToken.valid?(request.headers['Authorization'].split(' ').last)
		  		
		  		render json: { error: 'You are unauthorized to access this page' },status: :unauthorized
		  	else
		  		@user = User.fetch_user(request.headers['Authorization'].split(' ').last)
					current_user = @user
		  		check_blacklisted
		  	end
		  end

		  def check_blacklisted
		  	if @user.try(:company).try(:blacklisted)
		  		render json: { error: 'You are unauthorized to access this page' },status: :unauthorized
		  	end
		  end

		 	def render_resource_or_errors(resource, options = {})
		 	   if resource.errors.empty?
		 	     render options.merge({ json: { resource: resource } })
		 	   else
		 	     render_errors resource.errors.full_messages
		 	   end
		 	 end
		 	 
		   def render_errors(errors)
		     render json: { errors: errors }
		   end

	    def json_status(bool)
	      bool ? :ok : :unprocessable_entity
	    end

	    def configure_permitted_parameters   
	      devise_parameter_sanitizer.permit(:sign_up, keys: [:device_id,:os,:email,:first_name,:last_name,:country_id,:mobile,:company_id , :role, :terms_accepted])
	      devise_parameter_sanitizer.permit(:sign_in, keys: [:email,:password,:country_id])
	      devise_parameter_sanitizer.permit(:account_update, keys: [:device_id,:os,:email,:first_name,:last_name,:country_id,:mobile,:company_id , :role, :terms_accepted, :password, :password_confirmation, :current_password])
		    devise_parameter_sanitizer.permit(:invite , keys: [:email,:first_name,:last_name,:country_id,:mobile,:company_id , :role, :terms_accepted])
			end

end




