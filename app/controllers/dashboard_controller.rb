class DashboardController < ApplicationController
  before_action :authenticate_user! ,except: [:otp_shipments,:initial_shipments,:home,:email, :share_iphone, :terms_and_conditions_for_cargo,:share_android, :contact , :terms_and_conditions_for_fleet, :privacy_policy,:home_shipment]

  def index
    user_signed_in? ? (send(current_user.role) rescue rescue_redirection) : (rescue_redirection)
  end  



  def home
  end


  def email
    @vehicle_types = VehicleType.all.order(:country_id)

    @vehicle =VehicleType.find params[:vehicle_type]


    url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{params[:pickup_lat].to_f},#{ params[:pickup_lng].to_f}&destinations=#{params[:drop_lat].to_f},#{ params[:drop_lng].to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
		response = HTTParty.get(url)
	
    @pickup_country = params[:pickup_country]
    @drop_country = params[:drop_country]

    distance = response["rows"].first["elements"].first["distance"]["value"]/1000
    # puts "\n\n\n\t\t response#{distance1} \n\n"

		km_charges = (distance - @vehicle.free_kms) * @vehicle.rate_per_km
		@amount = @vehicle.base_rate + (km_charges > 0 ? km_charges : 0)
  
    
  end


  def initial_shipment

  

    redirect_to root_path, notice: "Shipment Request Created Successfully! Our team will contact you"


    
  end


  def otp_shipments

    @shipment = "a"
    @destination = params[:drop_building_name]
    @origin = params[:pickup_building_name]
    @name = params[:vehicle_type][:name]
    
    @vehicle = VehicleType.find_by_id(params[:vehicle_types])

    @country =Country.find_by_id( params[:vehicle_type][:country_id])
    @pickup_country = params[:pickup_country]
    @drop_country = params[:drop_country]

    @contact_no = make_phone_number @country.id ,  params[:vehicle_type][:contact_no]

    @amount =params[:amount]
    @code = unique_verification_code

    Shipmentestimation.create(name: @name, destination: @destination , origin: @origin , vehicle_type:@vehicle.id , country:@country.id , amount:@amount  , contact_no: @contact_no , verification_code:@code) 

    dispathcer = SendSMS.new
    message = dispathcer.message(@contact_no,"Verification Code: #{@code} \nTeam Lorryz")
 
  end

  def make_phone_number country_id , phone_number
  
    short_name = Country.find_by_id(country_id).try(:short_name)
    if short_name.present? and phone_number.present?
      return  TelephoneNumber.parse(phone_number, short_name).international_number(formatted: true).delete(" ")
    end
  end


  def verify_code? code
    if self.verification_code == code 
      true
    else
      false
    end
  end
  def send_verification_code

    @user = User.find_by_id(params[:user_id])
    if @user.present? and !@user.mobile_verified?
      @user.assgin_verification_code
      @user.send_verification_code
      flash[:notice] = "Verification code sent"
    end
    respond_to do |f|
      f.js
    end
  end

  def initial_shipments

    @shipment = "a"
    @otp = params[:vehicle_type][:otp]
    @destination = params[:drop_building_name]
    @origin = params[:pickup_building_name]
    @name = params[:name]
    @vehicle = VehicleType.find_by_id(params[:vehicle_types])
    @pickup_country = params[:pickup_country]
    @drop_country = params[:drop_country]
    @country =Country.find_by_id(params[:country])

    @contact_no = params[:contact]
    @amount =params[:amount]
    @self_otp = Shipmentestimation.where(contact_no: @contact_no).last
   

    ShipmentMailer.shipment_request(@destination,@origin,@name,@contact_no,@amount, @country.name,@vehicle.name).deliver_now

  end


  def home_shipment
    if user_signed_in?
      send("#{current_user.role}_redirect")
    else
      cookies[:shipment] = params if params 
      redirect_to new_user_session_path,notice: "You need to signin before creating shipment"  
    end
  end

  def share_iphone
    puts params[:phone_no]
    begin
      dispathcer = SendSMS.new
      message = dispathcer.message(params[:phone_no],"https://itunes.apple.com/pk/app/lorryz/id1451176427?mt=8&ign-mpt=uo%3D4 \nTeam Lorryz")
    rescue
    end
    redirect_to root_path, notice: "Your message has been delivered"
  end

  def share_android
    begin
      dispathcer = SendSMS.new
      message = dispathcer.message(params[:phone_no],"https://play.google.com/store/apps/details?id=com.LORRYZ \nTeam Lorryz")
    rescue
    end
    redirect_to root_path, notice: "Your message has been delivered"
  end

  def contact
    @user = params[:user]
    if(verify_recaptcha())
        begin
          UserMailer.send_customer_info(@user).deliver
        rescue StandardError => e

        end
        redirect_to root_path, notice: "We have received your information successfully. We will get back to you very soon. Thanks"  
    else      
      @error = true
    end
    
  end

  def terms_and_conditions_for_fleet 
    @page=Page.find_by(page_type: 'terms_and_conditions_for_fleet')
  end
  def terms_and_conditions_for_cargo
    @page=Page.find_by(page_type: 'terms_and_conditions_for_cargo')
  end
  
  def privacy_policy
    @page=Page.find_by(page_type: 'privacy_policy')
  end

  private

    def cargo_owner
      cookies[:shipment].present? ? (redirect_to(new_cargo_shipment_path)) : (redirect_to cargo_root_path)
    end

    def fleet_owner
      redirect_to fleet_root_path
    end


    def moderator
      redirect_to moderator_root_path
    end
      

    %w(admin superadmin).each do |method_name|
      define_method method_name do 
        redirect_to admin_root_path
      end
    end


    %w(admin_redirect superadmin_redirect fleet_owner_redirect).each do |method_name|
      define_method method_name do
        redirect_to_dashboard 
      end
    end  

    def rescue_redirection
      redirect_to dashboard_index_path
    end


    def cargo_owner_redirect
      cookies[:shipment] = params if params
      redirect_to new_cargo_shipment_path
    end

    def redirect_to_dashboard
      redirect_to home_dashboard_index_path,notice: "You are unauthorized to create shipment"
    end  

    def unique_verification_code
      loop do
        code = generate_code
        break code 

      end
    end

    def generate_code
      [0,0,0,0].map!{|x| (0..9).to_a.sample}.join
      # Devise.friendly_token(4).downcase.gsub(/[-,_,o,0]/,"x")
    end
end
