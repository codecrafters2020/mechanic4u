class RatingController < ApplicationController
  def rate
    @shipment = Shipment.find(params["shipment_id"])
    @company_ratings = CompanyRating.new
    if request.post?
      @company_ratings = CompanyRating.new(company_rating_params)
      @company_ratings.shipment_id = @shipment.id
      @company_ratings.giver_id = current_user.company.id
      receiver_id = [@shipment.company_id.to_i, @shipment.fleet_id.to_i].reject{|id| id == current_user.company.id}.first
      @company_ratings.receiver_id = receiver_id rescue nil
      if @company_ratings.save
        if receiver_id.present? and receiver_id != 0
          get_rated_user receiver_id
        end
        redirect_back(fallback_location: root_path)
      else
        flash[:error] = "Something went wrong!"
        redirect_back(fallback_location: root_path)
      end
    else
      respond_to do |format|
        format.js
      end
    end
  end
  private
  def company_rating_params
    params.require(:company_rating).permit(:rating, :remarks)
  end
  def get_rated_user company_id
    @user = Company.find(company_id).users.where(role: ["fleet_owner","cargo_owner"]).last rescue nil
  end
end
