class CompaniesController < WebApplicationController
  before_action :authenticate_user!

  skip_before_action :verify_authenticity_token
  respond_to :json
  layout :choose_layout

  require 's3_presigner'
  def presign_upload
    render json: UploadPresigner.presign("/users/avatar/", params[:filename], limit: 1.megabyte)
  end
  def edit
    if current_user.company && current_user.company.company_type == "individual"
      if current_user.company.individual_information.present?
        @edit = true
        @info = current_user.company.individual_information
      else
        @info = current_user.company.build_individual_information
      end
      render :get_individual_information
    elsif current_user.company && current_user.company.company_type == "company"
      if current_user.company.company_information.present?
        @edit = true
        @info = current_user.company.company_information
      else
        @info = current_user.company.build_company_information
      end
      render :get_company_information
    else
      redirect_to root_path , notice: "Invalid Request"
    end
  end
  def add_company_information
    set_attachment_params
    company = current_user.company
    if company.company_information.present?
      company.company_information.update(company_params)
      flash[:notice] = "Your Profile information is updated Successfully."
      info = company.company_information
    else
      info = company.build_company_information(company_params)
    end
    current_user.company.update!(tax_registration_no: params[:company][:tax_registration_no]) if current_user.fleet_owner? and params[:company][:tax_registration_no].present?
    info.save
    redirect_to dashboard_index_path
  end

  def add_individual_information
    set_attachment_params
    company = current_user.company
    if company.individual_information.present?
      company.individual_information.update(individual_params)
      flash[:notice] = "Your Profile information is updated Successfully."  
      info = company.individual_information
    else
      info = company.build_individual_information(individual_params)
    end
    current_user.company.update!(tax_registration_no: params[:company][:tax_registration_no]) if current_user.fleet_owner? and params[:company][:tax_registration_no].present?
    info.save
    redirect_to dashboard_index_path
  end
  def validate_and_update_password
    if password_update_request?
      password_match? ? update_password : render_response("Confirm password does not match")
    end
  end
  def get_individual_information
    if(current_user.company.company_type == "individual")
      if current_user.company.individual_information.present?
        @edit = true
        @info = current_user.company.individual_information
      else
        @info = current_user.company.build_individual_information
      end
    else
      redirect_to root_path , notice: "Invalid Request"
    end
  end

  def get_company_information
    if(current_user.company.company_type == "company")
      if current_user.company.company_information.present?
        @edit = true
        @info = current_user.company.company_information
      else
        @info = current_user.company.build_company_information
      end
    else
      redirect_to root_path , notice: "Invalid Request"
    end
  end

  def choose_layout
    if current_user.cargo? or current_user.cargo_owner?
      "cargo"
    elsif current_user.fleet? or current_user.fleet_owner?
      "fleet"
    end
  end

  private
  def individual_params
    params.require(:individual_information).permit(:name ,:address ,:secondary_mobile_no, :landline_no, :national_id, :expiry_date,:news_update,:avatar,documents: [])
  end
  def company_params
    params.require(:company_information).permit(:name , :address,:secondary_mobile_no, :landline_no,:website_address,:license_number, :license_expiry_date,:secondary_contact_name , :secondary_mobile_no, :news_update, :attachments,:avatar,documents: [])
  end
  def set_attachment_params
    if params[:company_information].present?
      begin
        params[:company_information][:avatar] = eval(params[:company_information][:avatar]).first if params[:company_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
          params[:company_information][:documents] = eval(params[:company_information][:documents].first) if params[:company_information][:documents].present?
      rescue SyntaxError => e
        params[:company_information][:documents] = params[:company_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:company_information][:documents] = params[:company_information][:documents].blank? ? [] : params[:company_information][:documents]
    elsif params[:individual_information].present?
      begin
        params[:individual_information][:avatar] = eval(params[:individual_information][:avatar]).first if params[:individual_information][:avatar].present?
      rescue SyntaxError => e
      end
      begin
        params[:individual_information][:documents] = eval(params[:individual_information][:documents].first) if params[:individual_information][:documents].present?
      rescue SyntaxError => e
        params[:individual_information][:documents] = params[:individual_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:individual_information][:documents] = params[:individual_information][:documents].blank? ? [] : params[:individual_information][:documents]
    end
  end

end
