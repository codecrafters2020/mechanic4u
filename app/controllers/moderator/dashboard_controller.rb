class Moderator::DashboardController < ModeratorController
  before_action :authenticate_user!
  before_action :check_if_moderator
  before_action :set_country , only: [:edit, :index , :new,  :update, :destroy]
  before_action :set_company, only: [:mark_verified,:mark_featured,:mark_blacklisted,:mark_safe_for_cash_on_delivery]
  # before_action :set_company, only: [:mark_verified, :mark_featured, :mark_blacklisted, :mark_safe_for_cash_on_delivery, :tax_invoice]
  before_action :set_user, only: [:edit_user, :update_user,:show_user]
  before_action :set_cargo_owners_link, only: [:cargo_owners, :cargo_contracts]
  before_action :set_fleet_owners_link, only: [:fleet_owners]
  
  layout 'moderator'

  def index
    puts "\n\n\n\t\t  in Index is\n\n"

    # @users=users.select{|c| c.company.try(:company_type) == "individkjlkual"}
    country = ISO3166::Country.find_country_by_name(current_user.country.name) rescue ""
    @user_country_coordinates = {lat: country.latitude , lng: country.longitude}
    @shipments = Shipment.where(status: [:enroute , :onoing])
  end
   def assigned_shipment
    @shipments = Shipment.filter_by_fleet(params[:company_id]) + Shipment.fleet_individual_shipments(params[:company_id])
    @fleet = User.find_by_company_id(params[:company_id])
    
   render:assigned_shipment
  end


  def cargo_owners
    @countries = Country.all.where(:id => current_user.country.try(:id))
    # @gps = GpsVehicle
    params[:query] ||=   {}
    @users = @cargo_owners = User.cargo_owners.where(:country_id => current_user.country.try(:id)).order(created_at: :desc).search_cargo(params[:query]).uniq
    #  @users = @cargo_owners =  @cargo_owners
    # Product
    @total =  User.company_cargo_owners.where(:country_id => current_user.country.try(:id)).count
    @shipments_total = Shipment.all.where(:country_id => current_user.country.try(:id))
    @date = Date.today() - 60.day
    @active_shipments = Shipment.filter_active_shipment(@date)

    @active_cargo = User.filter_by_active_shipments(@date).company_cargo_owners.where(:country_id => current_user.country.try(:id))
    # @active_cargo = @active_cargo.individual_cargo_owners
    @active_cargo =   @active_cargo.select(:company_id).distinct
   
   
    @inactive_cargo =@total -@active_cargo.count
    @total_cargo = "Total Cargo Owners  : " +@total.to_s
    @busy_cargo = "Active Cargo Owners : "+ @active_cargo.count.to_s
    @free_cargo = "InActive Cargo Owners :" +@inactive_cargo.to_s
   
    @total_indi =  User.individual_cargo_owners.where(:country_id => current_user.country.try(:id)).count
    @shipments_total = Shipment.all
    @active_cargo_indi = User.individual_cargo_owners.filter_by_active_shipments(@date).where(:country_id => current_user.country.try(:id))

    @active_cargo_indiv =   @active_cargo_indi.select(:company_id).distinct 
  
    @inactive_cargo_indi =@total_indi -@active_cargo_indiv.count
    @total_cargo_indi = "Total Cargo Owners  : " +@total_indi.to_s
    @busy_cargo_indi = "Active Cargo Owners : "+ @active_cargo_indiv.count.to_s
    @free_cargo_indi = "InActive Cargo Owners :" +@inactive_cargo_indi.to_s


    @filename = "Cargo Owners"
    respond_to_xls
  end

  def cargo_contracts
    @company = Company.find(params[:company_id]).where(:country_id => current_user.country.try(:id))
    @locations = @company.locations
  end

  def fleet_owners
   
    @countries = Country.all.where(:id => current_user.country.try(:id))
    params[:query] ||=   {}
    @users = @fleet_owners = User.fleet_owners.where(:country_id => current_user.country.try(:id)).order(created_at: :desc).search_fleet(params[:query]).uniq
    @total =  User.individual_fleet_owners.where(:country_id => current_user.country.try(:id)).count
    
    @active_fleet_admin = User.busy_fleet_individual_owners.individual_fleet_owners.where(:country_id => current_user.country.try(:id))
    @active_fleet = User.busy_fleet_owners.individual_fleet_owners.where(:country_id => current_user.country.try(:id))
    
    @active_fleets =   @active_fleet.select(:company_id)
    @active_fleet_admins =@active_fleet_admin.select(:company_id)
    @active_fleet= @active_fleets.distinct
    @active_fleet_admin =@active_fleet_admins.distinct

    @inactive_fleet=@total -@active_fleet.count-@active_fleet_admin.count
    @active_fleet_count= @active_fleet.count+  @active_fleet_admin.count
    
    @total_fleet = "Total Fleet Owners  : " +@total.to_s
    @busy_fleet = "Busy Fleet Owners : "+ @active_fleet_count.to_s
    @free_fleet = "Free Fleet Owners :" +@inactive_fleet.to_s
    @fleet_owners_link = "active"

    @filename = "Fleet Owners"
    respond_to_xls

  end

  def mark_verified
    @company.update(verified: !@company.verified?)
  end

  def tax_invoice
    @company.update(tax_invoice: !@company.tax_invoice?) 
  end

  def respond_to_xls
    respond_to do |format|
     format.html
     format.xlsx {
           render xlsx: 'xlsx_users', filename: "#{@filename}.xlsx", disposition: 'attachment'
         }
      end  
  end
  
  def mark_featured
    @company.update(featured: !@company.featured?)
  end
  
  def mark_blacklisted
     @company.update(blacklisted: !@company.blacklisted?)
  end
  
  def mark_safe_for_cash_on_delivery
    @company.update(safe_for_cash_on_delivery: !@company.safe_for_cash_on_delivery?)
  end
  
  def show_user
  end
  
  def edit_user

    if @user.company.company_type == "company"
      @company_information =  @user.try(:company).try(:company_information)
      @company_information = @user.company.build_company_information unless @company_information.present?
    elsif @user.company.company_type == "individual"
      @individual_information =  @user.try(:company).try(:individual_information)
      @individual_information  = @user.company.build_individual_information unless @individual_information.present?
    end
  end
  def vehicle_params
    params[:user][:vehicle].permit(:vehicle_type_id , :registration_number,:expiry_date, :insurance_number,:insurance_expiry_date,{documents: []}  )
  end
  
  def update_user

    set_attachment_params
    if params[:user][:vehicle].present?

      if @user.try(:company).try(:vehicles).present?

       @vehicle =  @user.company.vehicles[0]
       if params[:user][:vehicle][:documents].present?
       @vehicle.documents.each do |doc|
              params[:user][:vehicle][:documents].push (doc)  
       end
      end


       @vehicle.update(vehicle_params)

      end 
    end


    if params[:company].present? && params[:company][:tax_registration_no].present?

          @user.company.update_attributes(tax_registration_no: params[:company][:tax_registration_no])

         
        
         end 
    

    @user.update(params[:user].permit(:first_name , :last_name , :mobile , :email, :country_id))
    set_credit_period
    if params[:user][:company][:company_information].present?
      if @user.try(:company).try(:company_information).present?
        if    params[:user][:company][:company_information][:documents].present?
        @user.try(:company).try(:company_information).documents.each do |doc|
          params[:user][:company][:company_information][:documents].push (doc)
         end
        end
        @user.company.company_information.update(company_params)
      else
        @user.company.create_company_information(company_params)
      end
    elsif params[:user][:company][:individual_information].present?
      if @user.try(:company).try(:individual_information).present?
        if    params[:user][:company][:individual_information][:documents].present?

        @user.try(:company).try(:individual_information).documents.each do |doc|
          params[:user][:company][:individual_information][:documents].push (doc)
         end end
        @user.company.individual_information.update(individual_params)
      else
        @user.company.create_individual_information(individual_params)
      end
    end
    redirect_to moderator_show_user_path(user_id: @user.id) , notice: "User updated successfully!"
  end
  

  def edit_history
    @activities = Audit.all.order(created_at: :DESC).where(auditable_id:  params[:user_id] ,auditable_type: "User")
  end
  def add_fleet_dashboard_vehicle
    set_attachment_vehicle_params
    @current_user = User.find params[:vehicle][:userid]

    @vehicle =  @current_user.company.vehicles.create(vehicle_new_params)
    @vehicle.update(user_id: @current_user.id) if @current_user.company.individual?
    redirect_to moderator_fleet_owners_path , notice: "Vehicle created successfully!"
  end

  
  def add_vehicle
    
    @vehicle=Vehicle.new
   @current_fleet = User.find params[:id]

    render :add_vehicle

  end

  private

  def set_company
    @company = Company.find(params[:company_id])
  end

  def set_credit_period
    credit_period = params[:user][:company_attributes][:credit_period] rescue nil
    credit_amount = params[:user][:company_attributes][:credit_amount] rescue nil
    is_contractual = params[:user][:company_attributes][:is_contractual] rescue nil
    tax_registration_no  = params[:user][:company_attributes][:tax_registration_no] rescue nil
    if @user.company.category == "cargo"
      @user.company.update(credit_amount: credit_amount ,credit_period: credit_period ,is_contractual: is_contractual)
    elsif @user.company.category == "fleet"
      # @user.company.update(tax_registration_no: tax_registration_no)
    end
  end


  def vehicle_new_params
    params[:vehicle].permit(:vehicle_type_id , :registration_number,:expiry_date, :insurance_number,:insurance_expiry_date,{documents: []}  )
  end


  def set_user
    @user = User.find(params[:user_id])
    set_fleet_owners_link if @user.fleet_owner?
    set_cargo_owners_link if @user.cargo_owner?
  end

  def company_params
    params[:user][:company][:company_information].permit(:name , :address,:secondary_mobile_no, :landline_no,:website_address,:license_number, :license_expiry_date,:secondary_contact_name , :secondary_mobile_no, :news_update, :attachments,:industry,:trade_registration_no,:trade_registration_expiry,:contact1_first_name,:contact1_last_name,:contact1_mobile,:contact1_email,:contact1_designation,:contact1_office_no,:contact2_first_name,:contact2_last_name,:contact2_mobile,:contact2_email,:contact2_designation,:contact2_office_no,:contact3_first_name,:contact3_last_name,:contact3_mobile,:contact3_email,:contact3_designation,:contact3_office_no,:avatar,{documents: []})
  end

  

  def individual_params
    params[:user][:company][:individual_information].permit(:secondary_mobile_no, :landline_no, :national_id, :expiry_date, :news_update,:secondary_mobile_no_1, :nationality, :passport_no, :passport_no_expiry, :visa_no, :visa_no_expiry, :license_no, :license_no_expiry, :preferred_location,:avatar,{documents: []} )
  end

  def set_cargo_owners_link
    @cargo_owners_link = "active"
  end

  def set_fleet_owners_link
    @fleet_owners_link = "active"
  end
  def set_country
  end


  def set_attachment_vehicle_params

    if params[:vehicle][:documents].present?

      begin
        params[:vehicle][:documents] = eval(params[:vehicle][:documents].first) if params[:vehicle][:documents].present?
      rescue SyntaxError => e
        params[:vehicle][:documents] = params[:vehicle][:documents].first.split(" ") rescue ""
      end
      params[:vehicle][:documents] =  [] if params[:vehicle][:documents].blank?
    end
  end


  def set_attachment_params
    if params[:user][:vehicle].present?
      begin
        params[:user][:vehicle][:documents] = eval(params[:user][:vehicle][:documents].first) if params[:user][:vehicle][:documents].present?
        
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end  end
    if params[:user][:company][:company_information].present?
      begin
        params[:user][:company][:company_information][:avatar] = eval(params[:user][:company][:company_information][:avatar]).first if params[:user][:company][:company_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
          params[:user][:company][:company_information][:documents] = eval(params[:user][:company][:company_information][:documents].first) if params[:user][:company][:company_information][:documents].present?
      rescue SyntaxError => e
        params[:user][:company][:company_information][:documents] = params[:user][:company][:company_information][:documents].first.split(" ") rescue ""
      end
      params[:user][:company][:company_information][:documents] = params[:user][:company][:company_information][:documents].blank? ? [] : params[:user][:company][:company_information][:documents]
    elsif params[:user][:company][:individual_information].present?
      begin
        params[:user][:company][:individual_information][:avatar] = eval(params[:user][:company][:individual_information][:avatar]).first if params[:user][:company][:individual_information][:avatar].present?
      rescue SyntaxError => e
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      begin
        params[:user][:company][:individual_information][:documents] = eval(params[:user][:company][:individual_information][:documents].first) if params[:user][:company][:individual_information][:documents].present?
      rescue SyntaxError => e
        params[:user][:company][:individual_information][:documents] = params[:user][:company][:individual_information][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:user][:company][:individual_information][:documents] = params[:user][:company][:individual_information][:documents].blank? ? [] : params[:user][:company][:individual_information][:documents]
    end
  end

end
