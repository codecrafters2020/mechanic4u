class Moderator::OnlinedriversController < ModeratorController
  
  before_action :check_if_moderator
  layout 'moderator'

  def index
    country = ISO3166::Country.find_country_by_name(current_user.country.name) rescue ""
    @user_country_coordinates = {lat: country.latitude , lng: country.longitude}
  	@vehicles = Vehicle.all
  end
end