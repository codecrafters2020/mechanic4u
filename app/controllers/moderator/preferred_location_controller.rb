class Moderator::PreferredLocationController < ModeratorController
  before_action :authenticate_user!
  before_action :check_if_moderator
  before_action :set_preferred_location , only: [:edit,  :update, :destroy]
  layout 'moderator'
  def index
    @preferred_location = PreferredLocation.all
    respond_to do |format|
      format.html
    
    end
  end
  def new
    @admin_preferred_location = PreferredLocation.new
  end
  def create
    set_country_params
    @admin_preferred_location = PreferredLocation.new(preferredlocation_params)
    if @admin_preferred_location.save
      redirect_to moderator_preferred_location_index_path , notice: "Country created successfully!"
    else
      render :new
    end
  end
  def edit
  end

  def update

    if @admin_preferred_location.update(preferredlocation_params)
      redirect_to moderator_preferred_location_index_path , notice: "Preferred location updated successfully!"
    else
      render :edit
    end
  end
  
  private
  def preferredlocation_params
    params.require(:preferred_location).permit(:country_name, :city_name)
  end
  def set_preferred_location
    @admin_preferred_location = PreferredLocation.find(params[:id])
  end
  
  def set_country_params
    puts " asdfasf #{params}"
    country_name = params[:preferred_location][:country_name]
    country = ISO3166::Country.find_country_by_name(country_name)
 
  end
end
