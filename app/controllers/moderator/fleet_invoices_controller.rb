class Moderator::FleetInvoicesController < ModeratorController
  before_action :set_shipment , only: [:show]
  

  def show
    @name = "Advance Payment Receipt - VOC" +  Time.current.year.to_s + '%04d' % @shipment.id.to_s

    respond_to do |f|
      f.html
      f.js
      f.pdf do
        @pdf_view = true
        render pdf:  @name,
           template: "/moderator/fleet_invoices/pdf.html.erb",
           :encoding => "utf8",
           layout: "pdf.html.erb"
      end
    end
  end
  private
  def set_shipment
    
    puts"in the meantime"
    puts "\n\n\n\t\t #{params} \n\n"  
  
    @shipment = Shipment.find(params[:id])
    @fleet = User.find_by_id(params[:fleet_indi])
    @shipment_fleets = ShipmentFleet.all.where(shipment_id: @shipment.id ).order(:id)

    @category = params[:category]


    @cargo = User.find_by_company_id(@shipment.company_id) 
    
  end
end
