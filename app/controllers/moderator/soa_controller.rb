class Moderator::SoaController < ModeratorController
  before_action :authenticate_user!
  before_action :check_if_moderator

  before_action :set_soa_link ,only: [:edit,  :index , :new]
  
  layout 'moderator'
  require 'humanize'

  def index
    params[:query] ||=   {}

    if  params[:paramater_category].present?
    @category = params[:paramater_category]
  end
    if  params[:paramater_category].blank?
    params[:paramater_category] ="customer"
    @category = params[:paramater_category]

  end
 
    @listing =User.company_cargo_owners.where(:country_id => current_user.country.try(:id)).order(:company_id) if @category == "customer"
    @countries =Country.all.where(:id => current_user.country.try(:id)).order(:id) if @category == "country"
    
  end

  def country_accounts
    @customer =  User.cargo_owners.where(:country_id => params[:id])
    @country = Country.find_by_id( params[:id])
    

    respond_to do |f|
      f.html
      f.js
      f.pdf do
        @pdf_view = true
        render pdf: "SOA_Country",
           template: "/moderator/soa/country_accounts.html.erb",
           :encoding => "utf8",
           layout: "pdf.html.erb"
      end
    end

  end


  def customer_accounts
    @customer = User.find_by_company_id (params[:id])
    @shipments = Shipment.customer_shipments(params[:id])
    @shipment = Shipment.find_by_company_id(params[:id])

    respond_to do |f|
      f.html
      f.js
      f.pdf do
        @pdf_view = true
        render pdf: "SOA_Country",
           template: "/moderator/soa/customer_accounts.html.erb",
           :encoding => "utf8",
           layout: "pdf.html.erb"
      end
    end

  end
 
  def set_soa_link
    @soa_link = "active"
  end
  

end
