class Moderator::TcnController < ModeratorController
	before_action :set_shipment , only: [:show]
  def show
    respond_to do |f|
      f.html
      f.js
      f.pdf do
        @pdf_view = true
        render pdf: "TCN_UAE",
           template: "/moderator/tcn/pdf.html.erb",
           :encoding => "utf8",
           layout: "pdf.html.erb"
      end
    end
  end
  private
  def set_shipment
    
    puts"in the meantime"
    puts "\n\n\n\t\t #{params} \n\n"  
  
    @shipment = Shipment.find(params[:id])
    @fleet = User.find_by_company_id(params[:fleet_indi])
    @cargo = User.find_by_company_id(@shipment.company_id) 
    
  end
end
