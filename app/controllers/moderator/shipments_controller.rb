class Moderator::ShipmentsController < ModeratorController
  layout 'moderator'
  before_action :set_shipment , only: [:show ,:update]
  before_action :set_shipment_link
  before_action :set_vehicle_type, only: [:edit , :new, :create , :update]
  before_action :set_company_type, only: [:edit , :new, :create , :update]
  before_action :get_shipment , only: [:quit_bid, :show,:cancel, :assign_cost, :assign_vehicle,:update, :update_destination, :get_coordinates]
  before_action :set_individual_type, only: [:edit , :new, :create , :update]
  before_action :set_vendor, only: [:edit , :new, :create , :update,:assign_vehicle]
  before_action :set_pickup_time, only: [:create, :update]



  include ShipmentConcern

  require 's3_presigner'
  def presign_upload
    # puts "\n\n\n\t\t\n\n\n\t\t  \n\n\n\t\t  \n\n\n\t\t    here i m  params is #{params} \n\n"

    render json: UploadPresigner.presign("/users/avatar/", params[:filename], limit: 1.megabyte)
  end

  def index
    params[:query] ||=   {}
    if params[:query].present?
    @shipments = Shipment.search(params[:query]).where(:country_id => current_user.country.try(:id))

  else
    @date = Date.today() - 30.day
    @shipments =Shipment.where("updated_at >? ", @date ).where(:country_id => current_user.country.try(:id))
  end
    respond_to do |format|
      format.html
     format.xlsx {
            start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
            end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
            @shipments  = Shipment.where(pickup_date: start_date..end_date) #if params[:start_date].present? and params[:end_date].present?
            render xlsx: 'moderator/index', filename: "all_shipments.xlsx", disposition: 'attachment', stream: true
         }
    end

  end




  def new
    instance_variable_set :"@#{__method__}" , "active"
    @title = "Upcoming"
    @shipment = Shipment.new
    @individual = true
    shipment = cookies[:shipment] ? eval(cookies[:shipment]) : nil

    @shipment.no_of_vehicles =1;
    if shipment

      shipment = eval(cookies[:shipment])
      # puts " #{@individual}"
      @shipment = Shipment.new(pickup_location: shipment["pickup_location"],drop_location: shipment["drop_location"],
                              vehicle_type_id: shipment["vehicle_type_id"],no_of_vehicles: shipment["no_of_vehicles"],pickup_building_name: shipment["pickup_building_name"],drop_building_name: shipment["drop_building_name"], is_pickup_now: @individual)
      cookies[:shipment] = nil
    end
  end



  def get_shipment
    # shipment_country_access_control params[:id]
    @shipment = Shipment.find_by_id params[:id]
  end




  def create
    # format_date_fields
    set_attachment_params

    @shipment = Shipment.new(cargo_shipment_params)
    set_contract_fields if cargo_shipment_params[:location_id].present?


    if @shipment.cancel_by != ""
      @shipment.company_id =@shipment.cancel_by
      @shipment.cancel_by=nil
     end
     @shipment.unloading_time = @shipment.loading_time;

    if !cargo_shipment_params[:pickup_building_name].present?
      @shipment.pickup_building_name = "not specified"
    end


    if params[:shipment][:drop_location].blank?
      @shipment.is_fixed = false;
    else
      @shipment.is_fixed = true;

        end
     
    
      @shipment.is_createdby_admin = true;
      @shipment.fleet_id = User.find_by_id(current_user.id).company_id;
       @shipment.country_id=@shipment.try(:vehicle_type).country.try(:id)
       @shipment.amount =   @shipment.amount  *  @shipment.no_of_vehicles 
       @shipment.weight =   @shipment.weight  *  @shipment.no_of_vehicles 
 
    #  credit_period_days =  Country.find_by_id(@shipment.country_id).cargo_max_days

      @shipment.payment_option="credit_period"




    respond_to do |format|
      if @shipment.save
        # format.html { redirect_to moderator_shipments_path, notice: 'Shipment was successfully created.' }
              # format.html { add_shipment_details(@shipment.id) }
              format.html { redirect_to moderator_shipments_path, notice: 'Shipment was successfully created.' }

        # format.json { render :show, status: :created, location: @shipment }
      else
        flash[:error] = @shipment.errors.full_messages
        format.html { render :new }
      end
    end
  end

  def assign_cost
    @additional_cost = 0 ;
    @additional_cost = @shipment.additional_rate  + @additional_cost if @shipment.additional_rate.present?
    @additional_cost =  @shipment.additional_rate1 + @additional_cost if @shipment.additional_rate1.present?
    @additional_cost =  @shipment.additional_rate2 + @additional_cost if @shipment.additional_rate2.present?
    @additional_cost =  @shipment.additional_rate3 + @additional_cost if @shipment.additional_rate3.present?
    @additional_cost =  @shipment.additional_rate4 + @additional_cost if @shipment.additional_rate4.present?
    @additional_cost =  @shipment.additional_rate5 +  @additional_cost if @shipment.additional_rate5.present?
    @additional_cost =  @shipment.additional_rate6 + @additional_cost if @shipment.additional_rate6.present?
  
    @shipment_fleets = ShipmentFleet.all.where(shipment_id: @shipment.id ).order(:id)
    i = 1;
   #  @shipment.payable=  @shipment.fleet_net_rate if  @shipment.fleet_net_rate.present?
 
   @shipment.payable =0;
  @shipment.payable =  @shipment.advance_amount.to_i  if @shipment.advance_amount.present?
   @shipment.payable=@shipment.fleet_net_rate -  @shipment.payable if @shipment.fleet_net_rate.present?
        @shipment_fleets.each do |charge| 
 
     
     if i == 1
       @fleet_rate1 = charge.fleet_cost
       
     end
     if i == 2
       @fleet_rate2 = charge.fleet_cost
       
 
     end
     if i == 3
       @fleet_rate3 = charge.fleet_cost
      
 
     end
     if i == 4
       @fleet_rate4 = charge.fleet_cost
           end
     if i ==5
       @fleet_rate5 = charge.fleet_cost
         end
     if i == 6
       @fleet_rate6 = charge.fleet_cost
        end
     if i == 7
       @fleet_rate7 = charge.fleet_cost
        end
     if i == 8
       @fleet_rate8 = charge.fleet_cost
        end
     if i == 9
       @fleet_rate9 = charge.fleet_cost
         end
     if i == 10
       @fleet_rate10 = charge.fleet_cost
         end
     if i == 11
       @fleet_rate11 = charge.fleet_cost
         end
     if i == 12
       @fleet_rate12 = charge.fleet_cost
      end
     if i == 13
       @fleet_rate13 = charge.fleet_cost
       end
     if i == 14
       @fleet_rate14 = charge.fleet_cost
        end
     if i == 15
       @fleet_rate15 = charge.fleet_cost
       end
     if i == 16
       @fleet_rate16 = charge.fleet_cost
       end
     if i == 17
       @fleet_rate17 = charge.fleet_cost
          end
     if i == 18
       @fleet_rate18 = charge.fleet_cost
      end
     if i == 19
       @fleet_rate19 = charge.fleet_cost
        end
     if i == 20
       @fleet_rate20 = charge.fleet_cost
        end
     if i == 21
       @fleet_rate21 = charge.fleet_cost
     end
     if i ==22
       @fleet_rate22 = charge.fleet_cost
      end
     if i == 23
       @fleet_rate = charge.fleet_cost
      
     end
     if i == 24
       @fleet_rate24 = charge.fleet_cost
      
     end
     if i == 25
       @fleet_rate25= charge.fleet_cost
     
     end
    
    i+=1;
   end
 
     respond_to do |format|
       format.js
     end
   end
 

  def assign_vehicle
   
    respond_to do |format|
      format.js
    end
  end



  def set_cargo_shipment
    puts "\n\n\n\t\t set caro  is  \n\n"
    shipment_country_access_control(params[:id])
    @shipment = Shipment.find(params[:id])
  
  end

  # Never trust parameters from the scary internet, only allow the white list through.

  def cargo_shipment_params
    params.require(:shipment).permit( :VAT_applicable, :status,:weight,:vendor,:cancel_reason, :cancel_by,:amount_per_vehicle ,:state_event, :state, :id,:company_id, :country_id, :pickup_location, :pickup_date,:fleet_net_rate, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :payment_option, :pickup_building_name , :drop_building_name,:accepted_rate_currency,:amount,:is_createdby_admin,:balance_paid,:balance_paid_date)
  end

  def dropoff_location
    end
    def set_vendor
      @vendors = User.vendor.where(:country_id => current_user.country.try(:id)).order(:country_id)
    end

  def image_select
     
    @vehicle =""
    @pickup_time = ""
    @pickup_building_name = ""
    @drop_building_name = ""
    @pickup_date = ""
    @loading_time = ""
    @unloading_time = ""
    @vehicle_type_id = ""
    @no_of_vehicles = ""
    @cargo_description =""
    @cargo_packing_type = ""
    @payment_option = ""
    @address =""
    @drop_address =""
    @latitude = ""
    @longitude = ""
    @dropoff_lat= ""
    @dropoff_long= ""
    @city= ""
    @dcity=""
  
   
    @vehicle =params[:vehicle_id]
    @pickup_time = params[:pickup_time]
    @pickup_building_name = params[:pickup_building_name]
    @drop_building_name = params[:drop_building_name]
    @pickup_date = params[:pickup_date]
    @loading_time = params[:loading_time]
    @unloading_time = params[:unloading_time]
    @vehicle_type_id = params[:vehicle_type_id]
    @no_of_vehicles = params[:no_of_vehicles]
    @cargo_description = params[:cargo_description]
    @cargo_packing_type = params[:cargo_packing_type]
    @payment_option = params[:payment_option]
    @address =params[:address]
    @drop_address =params[:drop_address]
    @latitude = params[:latitude]
    @longitude = params[:longitude]
    @dropoff_lat= params[:dropoff_lat]
    @dropoff_long= params[:dropoff_long]
    @city= params[:city]
    @dcity= params[:dcity]
  
  
    redirect_to new_cargo_shipment_path(:vehicle=>@vehicle ,:pickup_building_name => @pickup_building_name,:drop_building_name => @drop_building_name,:pickup_date => @pickup_date,:loading_time => @loading_time,:unloading_time => @unloading_time,:no_of_vehicles => @no_of_vehicles,:cargo_description => @cargo_description,:cargo_packing_type => @cargo_packing_type,:payment_option => @payment_option,:address => @address,:drop_address => @drop_address,:latitude => @latitude,:longitude => @longitude,:dropoff_lat => @dropoff_lat,:dropoff_long => @dropoff_long,:city => @city,:dcity => @dcity)
     end


  def pickup_location
  end
  
	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end
  def format_date_fields
    params[:shipment][:pickup_date] = Date.strptime(params[:shipment][:pickup_date], "%d/%m/%Y")
    # params[:shipment][:expected_drop_off] = Date.strptime(params[:shipment][:expected_drop_off], "%d/%m/%Y") if params[:shipment][:expected_drop_off].present?
  end

  def set_pickup_time
    if params[:shipment][:vehicle_type_id].present?

    @vehicle = VehicleType.find_by_id(params[:shipment][:vehicle_type_id])
    @country = Country.find_by_id(@vehicle.country)

    params[:shipment][:pickup_time] = set_time_zone_admin(params[:shipment][:pickup_time],@country) if params[:shipment][:pickup_time].present?
    end
  end

  def set_vehicle_type
    @vehicles_types= VehicleType.all.where(:country_id => current_user.country.try(:id)).order(:country_id)
  end
  # .where(:country_id => current_user.country.try(:id))

  def set_company_type
	  @company_types =  CompanyInformation.cargo_company.filter_by_country(current_user.country_id).where.not(name: [""]).order(:name)
 
  end

  

  def set_individual_type
    @individual_types = User.individual_cargo_owners.where(:country_id => current_user.country.try(:id)).order(:first_name)
  end
  def edit
    @shipment = Shipment.find params[:id]
    @vehicle_types = VehicleType.all.order(:country_id)
  end


  def edit_history
    @activities = Audit.all.order(created_at: :DESC).where(auditable_id:  params[:id] ,auditable_type: "Shipment")

  end

  def email
    @shipment = Shipment.find params[:id]
    @vehicle_types = VehicleType.all.order(:country_id)

   
  end


  def get_bulk_coordinates
    @shipments = Shipment.includes(:vehicle_type,:shipment_vehicles).where(shipment_vehicles: {status: ShipmentVehicle::ONGOING_VEHICLES })
    render json: { coordinates:  get_shipment_bulk_coordinates(@shipments) }, status: :ok
  end
  

  def status_email

    
    @to = params[:shipment][:to]
    @cc = params[:shipment][:cc]
    @message = params[:shipment][:message]
    @subject = params[:shipment][:subject]
    @previous_status = params[:shipment][:previous_status]
    @current_status =params[:shipment][:current_status]
    @id = params[:shipment][:id]

   @shipment= Shipment.find_by_id(@id)
   ShipmentMailer.status_change(@to,@cc,@message,@subject,@previous_status,@current_status,@id).deliver_now
   @shipment.update_attributes(status: params["shipment"]["current_status"])


    redirect_to edit_moderator_shipment_path(@shipment)

  
  end
  def cancel
    Shipment.find(params[:id]).update_attributes!(state_event: :cancel , cancel_by: current_user.role)
    # Shipment.find(params[:id]).update!(state_event: :cancel , cancel_by: current_user.role)
    redirect_to moderator_shipments_path
    flash[:error]= "Shipment was canceled successfully"
  end
  

  def update   
    

    set_attachment_params

  respond_to do |format|
    @expired_fleets = 0;
    if params["shipment"]["vendor"].present?
      @shipment.update_attributes(vendor: params["shipment"]["vendor"][1])
    end

    if params["shipment"]["company_ids"].present?

    i = 0;
            ShipmentVehicle.where(shipment_id: @shipment.id).destroy_all

    params[:shipment][:company_ids].each do |company|
      if i == 1 
        @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
        @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
          if(@expired.length == 0)

            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_1] )
           ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?
        
           PushNotification.send_notification(
            @fleet.os,
            @fleet.device_id,
            "",
            "",
            "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
            nil,
           ) if @fleet.device_id
           Notification.create(notifiable_id: @fleet.id, 
             notifiable_type: "Shipment", 
             body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
             user_mentioned_id: @fleet.id)
     
          else
            flash[:error] = " Fleet is expired cannot assign"
            format.html { redirect_to moderator_shipments_path, notice: 'Shipment was successfully updated.' }

          end

        end
        if i == 2
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
           @expired =  User.filter_by_expired_fleet(company,Date.today())
           @fleet = User.find_by_company_id(company)

           @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_2] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
        else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 3
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_3] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 4
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length + @expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_4] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i ==5
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_5] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 6
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

         @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_6] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 7
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_7] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 8
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_8] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 9
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_9] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 10
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_10] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 11
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_11] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 12
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_12] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 13
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_13] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 14
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_14] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 15
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_15] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 16
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_16] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 17
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

         @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_17] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 18
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_18] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 19
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_19] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 20
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

         @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_20] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 21
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_21] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i ==22
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

           @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_22] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 23
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

         @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_23] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 24
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

         @expired =  User.filter_by_expired_fleet(company,Date.today())
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_24] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
        if i == 25
          @updated_fleet = ShipmentFleet.where(shipment_id: @shipment.id , company_id: company)
          @fleet = User.find_by_company_id(company)

          @expired =  User.filter_by_expired_fleet(company,Date.today()) 
          @expired_fleets= @expired.length +expired_fleets  if @expired.length != 0
          if(@expired.length == 0)
            ShipmentFleet.where(shipment_id: @shipment.id , company_id: company).update_all(fleet_cost: params[:shipment][:fleet_charges_25] )
            ShipmentVehicle.create(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id) ,status:  1) if  @fleet.company.vehicles.first.try(:id).present? && ShipmentVehicle.where(shipment_id: @shipment.id, vehicle_id: @fleet.company.vehicles.first.try(:id)).blank?

            PushNotification.send_notification(
              @fleet.os,
              @fleet.device_id,
              "",
              "",
              "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}",
              nil,
             ) if @fleet.device_id
             Notification.create(notifiable_id: @fleet.id, 
               notifiable_type: "Shipment", 
               body: "Hello #{@fleet.first_name}, you have been assigned a new shipment # #{@shipment.try(:id)}", 
               user_mentioned_id: @fleet.id)
       
          else
          flash[:error] = " Fleet is expired cannot assign"
        end
        end
    
         i+=1;
        end
      end
      if params[:shipment][:pickup_date].present?
      if (@shipment.pickup_date.to_date !=  params[:shipment][:pickup_date].to_date)
      params[:shipment][:created_at] = params[:shipment][:pickup_date]
      end 
     end 

     if(@expired_fleets == 0) 
      if @shipment.update!(shipment_params)
        if  params[:balance_paid]
          @shipment.update_attributes(balance_paid:  params[:balance_paid], balance_paid_date:  Date.strptime(params[:shipment][:balance_paid_date],'%d/%m/%Y') ,paid_to_fleet:  params[:balance_paid] )
        else
         @shipment.update_attributes(balance_paid:  params[:balance_paid], balance_paid_date:  nil ,paid_to_fleet:  params[:balance_paid] )
        end


      if params["shipment"]["amount"].present?
        @additional_cost = 0 ;
        @additional_cost = @shipment.additional_rate  + @additional_cost if @shipment.additional_rate.present?
        @additional_cost =  @shipment.additional_rate1 + @additional_cost if @shipment.additional_rate1.present?
        @additional_cost =  @shipment.additional_rate2 + @additional_cost if @shipment.additional_rate2.present?
        @additional_cost =  @shipment.additional_rate3 + @additional_cost if @shipment.additional_rate3.present?
        @additional_cost =  @shipment.additional_rate4 + @additional_cost if @shipment.additional_rate4.present?
        @additional_cost =  @shipment.additional_rate5 +  @additional_cost if @shipment.additional_rate5.present?
        @additional_cost =  @shipment.additional_rate6 + @additional_cost if @shipment.additional_rate6.present?
  
        @additional_cost  =   @additional_cost + params["shipment"]["amount"].to_i
        @shipment.update_attributes(customer_net_amount: @additional_cost)

        
  
  #  
  end
      end
         if params["shipment"]["company_ids"].present?
          company_ids= params["shipment"]["company_ids"].reject { |c| c.empty? }
           if company_ids.present?
          params["shipment"]["company_ids"].reject { |c| c.empty? }.each do |id|
            shipment = Shipment.find(params["id"])
          end
          if company_ids.present? and @shipment.state == "posted"
            @shipment.update!(state_event: :accepted) rescue (flash[:error] = @shipment.errors.full_messages)
          end  

          # if company_ids.present? and @shipment.state == "accepted"
          #   @shipment.update_attributes(state_event: "vehicle_assigned")
          # end  
          # if company_ids.present? and @shipment.state == "vehicle_assigned"
          #   @shipment.update_attributes(state_event: "ongoing")
          # end  
             
     end
    end

    

    if params["shipment"]["status"].present?
      ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  params["shipment"]["status"], performed_at:  DateTime.now().utc.strftime("%Y-%m-%d"), performed_at_time:  convert_to_shipment_country_timezone(@shipment.country ,Time.now().utc).strftime("%I:%M %p")) if  ShipmentActionDate.where(shipment_id: @shipment.id , state: params["shipment"]["status"]).blank?
      @shipment.vehicle_status_change_request params["shipment"]["status"]

      if params["shipment"]["status"] == "completed"
        if @shipment.state != "completed"
          @shipment.update_attributes(state_event: "completed")
          ShipmentActionDate.create(shipment_id: @shipment.id, vehicle_id: "",state:  "complete", performed_at:  DateTime.now.strftime("%Y-%m-%d"), performed_at_time: convert_to_shipment_country_timezone(@shipment.country ,Time.now.utc).strftime("%I:%M %p"))  
          ShipmentVehicle.where(shipment_id: @shipment.id ).update_all(status: 6 )

        end
       end  
       if params["shipment"]["status"] == "Start"
        @shipment.update_attributes(state_event: "ongoing")
        ShipmentVehicle.where(shipment_id: @shipment.id , status: 1).update_all(status: 2 )

       end 

       if params["shipment"]["status"].present? && @shipment.state !=  "ongoing"
        @shipment.update_attributes(state_event: "ongoing")
        ShipmentVehicle.where(shipment_id: @shipment.id , status: 1).update_all(status: 2 )

      end



       if params["shipment"]["status"] == "Enroute to Destination"
        ShipmentVehicle.where(shipment_id: @shipment.id).update_all(status: 4 )

       end 

       if params["shipment"]["status"] == "At Offloading Point"
        ShipmentVehicle.where(shipment_id: @shipment.id ).update_all(status: 5)

       end 

       if params["shipment"]["status"] == "At Loading"
        ShipmentVehicle.where(shipment_id: @shipment.id ).update_all(status: 3 )

       end       
     end   
 
        @shipment.recalculate_shipment_payments if params[:shipment][:state_event] == "completed" || params[:shipment][:discount]
        @shipment.set_contract_fields

    
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").try(:last).update_attributes(performed_at: Date.strptime( params[:shipment][:start].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:start_time])                                                               if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Start").present? &&  params[:shipment][:start].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:loading].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:loading_action_time] )                                                 if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Loading").present?  &&  params[:shipment][:loading].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:enroute_origin].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:enroute_origin_time] )                            if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Origin Border").present?  &&  params[:shipment][:enroute_origin].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Origin Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_origin].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_origin_time] )                                              if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Origin Border").present?  &&  params[:shipment][:at_origin].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Transit Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_transit].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_transit_time] )                                           if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Transit Border").present?  &&  params[:shipment][:at_transit].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Destination Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_destination].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_destination_time] )                               if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Destination Border").present? &&  params[:shipment][:at_destination].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Cleared From Border").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:cleared_border].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:cleared_border_time] )                                 if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Cleared From Border").present? &&  params[:shipment][:cleared_border].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:enroute_dest].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:enroute_dest_time] )                                  if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Enroute to Destination").present? &&  params[:shipment][:enroute_dest].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Offloading Point").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:at_offloading].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:at_offloading_time] )                                       if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "At Offloading Point").present? &&  params[:shipment][:at_offloading].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "Break Down").try(:last).update_attributes(performed_at:Date.strptime( params[:shipment][:breakdown].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:breakdown_time] )                                                   if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "Break Down").present? &&  params[:shipment][:breakdown].present?
        ShipmentActionDate.where(shipment_id: @shipment.id , state: "completed").try(:last).update_attributes(performed_at: Date.strptime( params[:shipment][:completed].split(" ").first,'%d/%m/%Y'),performed_at_time:params[:shipment][:completed_time] )                                                   if  ShipmentActionDate.where(shipment_id: @shipment.id , state: "completed").present? &&  params[:shipment][:completed].present?

        format.html { redirect_to moderator_shipments_path, notice: 'Shipment was successfully updated.' }
      else
        format.html { render :index }
      end
    end
  end

  def cargo_invoice
  
     InvoiceMailer.send_cargo_invoice_in_email(:id => params[:format]).deliver_now 
  end

  def show

    Aws.config.update({
      region: "us-east-1",
      credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
    })
    dynamodb = Aws::DynamoDB::Client.new
    table_name = 'vehicle_coordinates'

    zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")



    if  @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).present?  && @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).present? && @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).present?
      start_time = {"date"=>  @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  @shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).strftime("%H:%M") }
      start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
      tz = ISO3166::Country.new(@shipment.country.short_name).timezones.zone_identifiers.first
      start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
      start_timestamp = start_time_csv.utc
      complete_time = {"date"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  @shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).strftime("%H:%M") }
      complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
      complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) } 
      complete_timestamp = complete_time_csv.utc
      vehicle_id  = @shipment.shipment_fleets.last.company.vehicles.first.try(:id) if @shipment.shipment_fleets.present? && @shipment.shipment_vehicles.blank?
			vehicle_id = @shipment.shipment_vehicles.first.try(:vehicle_id)  if @shipment.shipment_vehicles.present?
			   
      params = {

        table_name: table_name,
        key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
        expression_attribute_names: {
            "#created" => "created",
            "#vehicle_id" => "vehicle_id"
        },
        expression_attribute_values: {
          ":vehicle_id" => vehicle_id,
          ":start_time" =>   start_timestamp.to_s,
          ":end_time" => complete_timestamp.to_s
        }
      }

      if start_timestamp  < complete_timestamp
      @resp = dynamodb.query(params)

      end
    end


  end

  private

  def shipment_params
    params.require(:shipment).permit(:additional_cost_vat,:accepted_rate_currency,:additional_cost_1_vat,:additional_cost_2_vat,:additional_cost_3_vat,:additional_cost_4_vat,:additional_cost_5_vat,:additional_cost_6_vat,:advance_amount,:advance_paid_date,:advance_paid_to,:VAT_applicable,:final_paid_to,:status,:weight,:vendor,:drop_building_name, :pickup_building_name, :completed_by, :discount, :cancel_reason, :cancel_by,:amount_per_vehicle ,:state_event, :state,:fleet_net_rate, :id,:company_id, :country_id, :pickup_location, :pickup_date, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :payment_option ,:created_at,:fleet_income,:fleet_individual_rate,:lorryz_share_amount,:fleet_rate_currency,:additional_type,:additional_rate,:additional_rate_desc,:additional_type1,:additional_rate1,:additional_rate_desc1,:additional_type2,:additional_rate2,:additional_rate_desc2,:additional_type3,:additional_rate3,:additional_rate_desc3,:additional_type4,:additional_rate4,:additional_rate_desc4,:additional_type5,:additional_rate5,:additional_rate_desc5,:additional_type6,:additional_rate6,:additional_rate_desc6,:balance_paid,:balance_paid_date,:customer_net_amount,company_ids: [],final_voucher: [],documents: [] ,advance_voucher: [])
  end

  def set_shipment
    @shipment = Shipment.find(params[:id])
  end

  def set_shipment_link
    @shipment_link = "active"
  end

  def set_attachment_params


  if params[:shipment][:documents].present?
    begin
      params[:shipment][:documents] = eval(params[:shipment][:documents].first) 
    rescue SyntaxError => e
      puts "\n\n\n\t\t #{e.message} \n\n"
    end  end




  if params[:shipment][:final_voucher].present?

    begin
      params[:shipment][:final_voucher] = eval(params[:shipment][:final_voucher].first) 
    rescue SyntaxError => e
      puts "\n\n\n\t\t #{e.message} \n\n"
    end  end
    



  if params[:shipment][:advance_voucher].present?

    begin
      params[:shipment][:advance_voucher] = eval(params[:shipment][:advance_voucher].first) 
    rescue SyntaxError => e
      puts "\n\n\n\t\t #{e.message} \n\n"
    end  end

          
          
end







end
