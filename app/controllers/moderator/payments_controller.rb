class Moderator::PaymentsController < ModeratorController
  before_action :set_shipment , only: [:mark_lorryz_comission_received ,:mark_paid_by_cargo, :mark_paid_to_fleet,:cancel_penalty_amount_received]
  before_action :set_payments_link ,only: [:index]
  def index
    @shipments = Shipment.where(state: [:completed, :cancel] ,:country_id => current_user.country.try(:id))
    respond_to do |format|
      format.html
      format.xlsx {
        start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
        end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
        @shipments  = @shipments.where(created_at: start_date..end_date ,:country_id => current_user.country.try(:id))
        render xlsx: 'xlsx_payments', filename: "xlsx_xlsx_payments.xlsx", disposition: 'attachment'
      }
    end
  end
  def mark_paid_by_cargo
    @shipment.update(paid_by_cargo: !@shipment.paid_by_cargo?)
  end
  def mark_paid_to_fleet
    @shipment.update(paid_to_fleet: !@shipment.paid_to_fleet?)
  end
  def mark_lorryz_comission_received
    @shipment.update(lorryz_comission_received: !@shipment.lorryz_comission_received?)
  end

  def cancel_penalty_amount_received
    @shipment.update(cancel_penalty_amount_received: !@shipment.cancel_penalty_amount_received)
  end

  private
  def set_shipment
    @shipment = Shipment.find(params[:shipment_id])
  end
  def set_payments_link
    @payments_link = "active"
  end
end


