class Fleet::VehiclesController < FleetController
  before_action :check_if_individual_and_vehicles_present , only: [:new , :create , :change_driver]
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy,:change_driver]
  before_action :set_vehicle_title , only: [:index , :edit , :new , :show]
  before_action :set_attachment_params , only: [:create, :update]
  
  def index

    instance_variable_set :"@#{__method__}" , "active"
    if vehicle_assignment?
      @vehicles = Vehicle.reterive_for_assignment(current_user)
    elsif vehicle_filter?
      @vehicles = Vehicle.search_by_filters(current_user,params)
    else 
      @vehicles = Vehicle.fetch_by_company(current_user.company_id)
    end
  end



  # GET /vehicles/1
  # GET /vehicles/1.json
  def show
    instance_variable_set :"@#{__method__}" , "active"
  end

  # GET /vehicles/new
  def new
    instance_variable_set :"@#{__method__}" , "active"
    @vehicle = Vehicle.new
  end

  # GET /vehicles/1/edit
  def edit
    instance_variable_set :"@#{__method__}" , "active"
  end

  def vehicle_types
    render json: VehicleType.where(:country_id => current_user.country_id), status: :ok
  end
  # POST /vehicles
  # POST /vehicles.json
  def create
    @vehicle =  current_user.company.vehicles.create(vehicle_params)
    @vehicle.update(user_id: current_user.id) if current_user.company.individual?
    redirect_to fleet_vehicles_path , notice: "Vehicle created successfully!"
    # @vehicle.errors.blank? ? ( render "create" , status: :ok ) : (render json: { errors: @vehicle.errors.full_messages }, status: :bad_request)

  end
  # PATCH/PUT /vehicles/1
  # PATCH/PUT /vehicles/1.json
  def update
    if @vehicle.update(vehicle_params)
      redirect_to fleet_vehicles_path , notice: "Vehicle updated successfully!"
    else
      flash[:error] = @vehicle.errors.full_messages
      redirect_to edit_fleet_vechicles_path(@vehicle)
      # render json: { errors: @vehicle.errors.full_messages }, status: :bad_request
    end
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    @vehicle.destroy
    respond_to do |format|
      format.html do
        redirect_back(fallback_location: fleet_vehicles_path)
      end
      format.json { head :no_content }
    end
  end

  def change_driver
    if request.post?
      status = @vehicle.user_id.present? ?  "changed" : "assigned"
      @vehicle.user_id = params[:vehicle][:user_id]
      if @vehicle.save
        redirect_to fleet_vehicles_path , notice: "Driver #{status} successfully!"
      else
        render js: "alert('Something went Wrong')"
        # redirect_to fleet_vehicles_path , notice: "Driver changed successfully!"
      end
    else
      respond_to do |f|
        f.js
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_vehicle
    @vehicle = Vehicle.find(params[:id])
  end
  def set_vehicle_title
    @vehicles_title = __method__
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def vehicle_params
    params.require(:vehicle).permit(:registration_number, :insurance_number, :vehicle_type_id, :company_id,:expiry_date,:authorization_letter ,:available,:not_available_from,:not_available_to, :insurance_expiry_date,  documents: [])
  end

  def set_attachment_params
    if params[:vehicle].present?
      begin
        params[:vehicle][:documents] = eval(params[:vehicle][:documents].first) if params[:vehicle][:documents].present?
      rescue SyntaxError => e
        params[:vehicle][:documents] = params[:vehicle][:documents].first.split(" ") rescue ""
        puts "\n\n\n\t\t #{e.message} \n\n"
      end
      params[:vehicle][:documents] =  [] if params[:vehicle][:documents].blank?
    end
  end

  def vehicle_assignment?
    params[:assignment].present?
  end

  def vehicle_filter?
    params[:state].present? && params[:state] != 'undefined'
  end
end
