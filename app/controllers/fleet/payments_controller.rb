class Fleet::PaymentsController < FleetController
  def index
    @payment = :active
    # @shipments = Shipment.where(state: "completed", fleet_id: params[:company_id].to_i, paid_to_fleet: false, lorryz_comission_received: false)
		@shipments = Shipment.where("(state = ? OR (state = ? AND cancel_by = ?)) AND fleet_id = ? AND (paid_to_fleet = ? OR lorryz_comission_received =?) AND cancel_penalty_amount_received = ?",'completed','cancel','fleet_owner', current_user.company_id.to_i, false, false,false)
  end
end
