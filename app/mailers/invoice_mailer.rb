class InvoiceMailer < ApplicationMailer
	default from: 'Lorryz <no-reply@lorryz.com>'
	add_template_helper(ApplicationHelper)

	def send_in_email(user,shipment_id)
		@shipment = Shipment.find(shipment_id)
		attachments["Invoice_#{shipment_id}.pdf"] = WickedPdf.new.pdf_from_string(
				render_to_string(pdf: 'Invoice', template: '/invoices/pdf.html.erb',:encoding => "utf8",layout: "pdf.html.erb")
		)
		mail(to: "contact@lorryz.com", subject: "Lorryz Invoice for Shipment# #{shipment_id}" )
	end

	def send_cargo_invoice_in_email(param)
     
		@shipment = Shipment.find(param[:id])
		@user = User.find_by_company_id(@shipment.company_id)
		attachments["Invoice_#{param[:id]}.pdf"] = WickedPdf.new.pdf_from_string(
				render_to_string(pdf: 'Invoice', template: 'admin/invoices/pdf.html.erb',:encoding => "utf8",layout: "pdf.html.erb")
		)
		mail(to: @user.email, subject: "Lorryz Invoice for Shipment# #{param[:id]}" )
		
	end
end