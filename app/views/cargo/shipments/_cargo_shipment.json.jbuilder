json.extract! cargo_shipment, :id, :created_at, :updated_at
json.url cargo_shipment_url(cargo_shipment, format: :json)
