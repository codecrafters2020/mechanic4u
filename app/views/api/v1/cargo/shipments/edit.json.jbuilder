json.extract! @shipment, :id, :pickup_location, :drop_location, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng,
 :pickup_date, :no_of_vehicles, :loading_time, :expected_drop_off, :unloading_time, :pickup_building_name, :drop_building_name, 
 :cargo_description, :cargo_packing_type, :pickup_time, :state, :amount, :process, :payment_option,:updated_at,:paid_by_cargo,:paid_to_fleet,:vehicle_type_id, :location_id,
              :is_pickup_now

json.company_id @shipment.company_id
json.fleet_id @shipment.fleet_id
json.image url_for(@shipment.vehicle_type.avatar) if @shipment.vehicle_type.avatar.attached?
json.image   @shipment.vehicle_type.try(:image_url).first if !@shipment.vehicle_type.avatar.attached?
