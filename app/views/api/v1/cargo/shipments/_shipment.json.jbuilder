json.extract! shipment, :id, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng,
 :pickup_date, :no_of_vehicles, :loading_time, :expected_drop_off, :unloading_time,
 :cargo_description, :cargo_packing_type, :state, :process, :payment_option,:updated_at,:paid_by_cargo,:paid_to_fleet
json.pickup_location shipment.pickup_full_address
json.drop_location shipment.drop_full_address
json.payment_option shipment.payment_method
json.is_contractual shipment.location.present? ? true : false
json.vehicle_type shipment.try(:vehicle_type).try(:name)
json.vehicle_shipments shipment.shipment_vehicles
json.is_pickup_now shipment.is_pickup_now
json.distance shipment.distance_between_endpoints


json.assigned_vehicle assigned_vehicle_for_driver(shipment)


json.route shipment_route(shipment) if shipment.completed?
json.distance_km shipment_distance(shipment) if shipment.completed?


json.pickup_time shipment.pickup_time
json.cancel_penalty shipment.cargo_cancel_penalty_charges_within_24hours
json.cancel_charges shipment.cancel_penalty
json.cancel_by shipment.cancel_by
json.amount number_with_precision(shipment.amount, precision: 2)
json.amount_per_vehicle number_with_precision(shipment.amount_per_vehicle, precision: 2)
json.detention number_with_precision(shipment.detention, precision: 2)
json.detention_per_vehicle number_with_precision(shipment.detention_per_vehicle, precision: 2)
json.lorryz_share_amount number_with_precision(shipment.lorryz_share_amount, precision: 2)
json.invoice_date shipment.invoice_date
json.cargo_company_rating shipment.try(:company).try(:average_rating)
json.company_id shipment.company_id
json.fleet_id shipment.fleet_id
json.company_ratings shipment.company_ratings
json.fleet_company_name shipment.try(:fleet).try(:company_name)
json.cargo_company_name shipment.try(:company).try(:company_name)
json.fleet_owner shipment.fleet ? shipment.fleet.fleet_owner: ''
json.cargo_owner_number shipment.try(:company).try(:cargo_owner).try(:mobile)
json.fleet_owner_number shipment.try(:fleet).try(:fleet_owner).try(:mobile)
json.tracking_code shipment.tracking_code
json.is_createdby_admin shipment.is_createdby_admin

json.bids do
 json.array! shipment.bids.sort_by{ |a|  a.amount  } do |bid|
  json.id bid.id
  json.company_id bid.company_id
	json.amount number_with_precision(bid.amount, precision: 2)
	json.lat bid.lat
	json.lng bid.lng
  json.status bid.status
  json.detention number_with_precision(bid.detention, precision: 2)
  json.shipment_id bid.shipment_id
  json.verified bid.company.verified

  json.fleet_average_rating bid.company.average_rating if bid.company
  json.rated_count bid.company.received_ratings.count if bid.company

 end
end
json.best_bid shipment.bids.best_bid
json.image url_for(shipment.vehicle_type.avatar) if shipment.vehicle_type.avatar.attached?
json.image   shipment.vehicle_type.try(:image_url).first if !shipment.vehicle_type.avatar.attached?

json.vehicles shipment.vehicles do |vehicle|
	json.id vehicle.id
	json.registration_number vehicle.registration_number
	json.insurance_number vehicle.insurance_number
	json.company_id vehicle.company_id
	json.created_at vehicle.created_at
	json.updated_at vehicle.updated_at
	json.expiry_date vehicle.expiry_date
	json.authorization_letter vehicle.authorization_letter
	json.vehicle_type_id vehicle.vehicle_type_id
	json.user_id vehicle.user_id
	json.driver vehicle.driver
	json.latitude vehicle.latitude
	json.longitude vehicle.longitude
end

