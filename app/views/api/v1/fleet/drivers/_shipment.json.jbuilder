json.extract! shipment, :id, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng,
 :pickup_date, :no_of_vehicles, :loading_time, :expected_drop_off, :unloading_time, 
 :cargo_description, :cargo_packing_type, :pickup_time, :state, :amount, :process, :payment_option
json.pickup_location shipment.pickup_full_address
json.drop_location shipment.drop_full_address
json.payment_option shipment.try(:payment_option).try(:camelcase)
json.vehicle_type shipment.try(:vehicle_type).try(:name)
json.detention shipment.try(:detention)
json.detention_per_vehicle shipment.try(:detention_per_vehicle)
json.fleet_owner shipment.fleet ? shipment.fleet.fleet_owner: ''
json.vehicle_shipments = shipment.shipment_vehicles.where(vehicle_id: @user.vehicle.id)
json.vehicles do
	  json.array! shipment.shipment_vehicles.where(vehicle_id: @user.vehicle.id) do |shipment_vehicle|
	  json.id shipment_vehicle.try(:vehicle).try(:id)
	  json.registration_number shipment_vehicle.try(:vehicle).try(:registration_number)
	  json.driver_name shipment_vehicle.try(:vehicle).try(:driver).try(:full_name)
	  json.driver_mobile shipment_vehicle.try(:vehicle).try(:driver).try(:mobile)
	  json.status shipment_vehicle.status
	end
end
