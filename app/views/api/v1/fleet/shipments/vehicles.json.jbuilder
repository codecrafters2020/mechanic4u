json.array! @vehicles do |vehicle|

	json.id vehicle.id
    json.registration_number vehicle.registration_number
    json.vehicle_type vehicle.try(:vehicle_type).try(:name)
	json.driver_name vehicle.try(:driver).try(:full_name)
	json.driver_mobile vehicle.try(:driver).try(:mobile)
    json.latitude vehicle.latitude
    json.longitude vehicle.longitude
    json.pickup_location @shipment.pickup_location
    json.drop_location @shipment.drop_location
    json.shipment_id @shipment.id
end
