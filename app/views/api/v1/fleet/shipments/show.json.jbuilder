json.extract! @shipment, :id, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng,
 :pickup_date, :no_of_vehicles, :loading_time, :expected_drop_off, :unloading_time, 
 :cargo_description, :cargo_packing_type, :state, :amount, :process, :payment_option,:updated_at,:paid_by_cargo,:paid_to_fleet
json.pickup_location @shipment.pickup_full_address
json.drop_location @shipment.drop_full_address
json.payment_option @shipment.try(:payment_option).try(:camelcase)
json.vehicle_type @shipment.try(:vehicle_type).try(:name)
if @shipment.is_createdby_admin

else
	json.amount_per_vehicle number_with_precision(@shipment.amount_per_vehicle, precision: 2)
end
json.pickup_time @shipment.pickup_time
json.detention @shipment.try(:bids).try(:best_bid).try(:detention)
json.vehicle_ids @shipment.vehicles.pluck(:id)
json.vehicle_shipments @shipment.shipment_vehicles
json.cargo_owner_number @shipment.try(:company).try(:cargo_owner).try(:mobile)
json.fleet_owner_number @shipment.try(:fleet).try(:fleet_owner).try(:mobile)
json.tracking_code @shipment.tracking_code
json.vehicles do
	# if @shipment.is_createdby_admin
	# 	json.array! @shipment.shipment_vehicles.where(vehicle_id: current_user.vehicle.id) do |shipment_vehicle|
	# 		json.id shipment_vehicle.try(:vehicle).try(:id)
	# 		json.registration_number shipment_vehicle.try(:vehicle).try(:registration_number)
	# 		json.driver_name shipment_vehicle.try(:vehicle).try(:driver).try(:full_name)
	# 		json.driver_mobile shipment_vehicle.try(:vehicle).try(:driver).try(:mobile)
	# 		json.status_code shipment_vehicle.status_code
	# 		json.status shipment_vehicle.status
	# 	end
	# else
		json.array! @shipment.shipment_vehicles do |shipment_vehicle|
			json.id shipment_vehicle.try(:vehicle).try(:id)
			json.registration_number shipment_vehicle.try(:vehicle).try(:registration_number)
			json.driver_name shipment_vehicle.try(:vehicle).try(:driver).try(:full_name)
			json.driver_mobile shipment_vehicle.try(:vehicle).try(:driver).try(:mobile)
			json.status_code shipment_vehicle.status_code
			json.status shipment_vehicle.status
		# end
	end
end
