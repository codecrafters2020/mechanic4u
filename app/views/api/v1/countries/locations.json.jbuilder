json.array! @locations do |location|
  json.id location.id
  json.name location.contractual_locations
  json.rate location.rate
  json.vehicle_type_id location.vehicle_type_id
  json.loading_time location.loading_time
  json.unloading_time location.unloading_time

  json.pickup_building_name location.pickup_building_name
  json.pickup_location location.pickup_location
  json.pickup_city location.pickup_city
  json.pickup_lat location.pickup_lat
  json.pickup_lng location.pickup_lng
  
  json.drop_building_name location.drop_building_name
  json.drop_location location.drop_location
  json.drop_city location.drop_city
  json.drop_lat location.drop_lat
  json.drop_lng location.drop_lng
end
