json.array! @notifications do |n|
  json.id n.id
  json.body n.try(:body)
  json.notifiable_id n.try(:notifiable_id)
  json.notifiable_type n.try(:notifiable_type)
  json.creation_time n.creation_time
  json.created_at n.created_at
end