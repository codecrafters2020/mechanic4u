
#
#  Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
#  This file is licensed under the Apache License, Version 2.0 (the "License").
#  You may not use this file except in compliance with the License. A copy of
#  the License is located at
# 
#  http://aws.amazon.com/apache2.0/
# 
#  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
require "aws-sdk"

Aws.config.update({
    region: "us-east-1",
    credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz'),
  })

dynamodb = Aws::DynamoDB::Client.new

params = {
    table_name: "vehicle_coordinates",
    key_schema: [
        {
            attribute_name: "vehicle_id",
            key_type: "HASH"  #Partition key
        },
        {
            attribute_name: "created",
            key_type: "RANGE" #Sort key 
        },
       
    ],
    attribute_definitions: [
        {
            attribute_name: "vehicle_id",
            attribute_type: "N"
        },
        {
            attribute_name: "created",
            attribute_type: "S"
        },
        

    ],
    provisioned_throughput: { 
        read_capacity_units: 10,
        write_capacity_units: 10
  }
}

begin
    result = dynamodb.create_table(params)
    puts "Created table. Status: " + 
        result.table_description.table_status;

rescue  Aws::DynamoDB::Errors::ServiceError => error
    puts "Unable to create table:"
    puts "#{error.message}"
end