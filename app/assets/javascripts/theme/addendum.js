$(function() {
    setInterval(function(){
        $('.alert').slideUp(1000);
    }, 4000);
});

var map;
function initMap() {
    if($("#map").length > 0){
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 25.2769, lng: 55.2962},
            zoom: 10
        });
    }
}

function initMapWithCenter(center) {
    if($("#map").length > 0){
        map = new google.maps.Map(document.getElementById('map'), {
            center: center,
            // center: {lat: 27.994402, lng: -81.760254},
            zoom: 10
        });
    }
}

function populate_data() {
    location_id = $("#shipment_location_id").val();
    if(location_id && location_id != "")
    {
        $.ajax({
            type: "get",
            url:  "/countries/location/" + location_id,
            dataType: 'json',
            success: function (resp) {
                $("#shipment_pickup_location").prop("disabled" , true); $("#shipment_pickup_location").val(resp.pickup_location);
                // $("#shipment_pickup_building_name").prop("disabled", "disabled");
                $("#shipment_pickup_building_name").val(resp.pickup_building_name);
                $("#shipment_pickup_lat").val(resp.pickup_lat); $("#shipment_pickup_lng").val(resp.pickup_lng);
                $("#shipment_drop_location").prop("disabled" , "disabled"); $("#shipment_drop_location").val(resp.drop_location)
                // $("#shipment_drop_building_name").prop("disabled", "disabled");
                $("#drop_building_name").val(resp.drop_building_name);
                $("#shipment_drop_lat").val(resp.drop_lat); $("#shipment_drop_lng").val(resp.drop_lng);
                $("#shipment_amount").val(resp.rate)
                $("#shipment_drop_city").val(resp.drop_city);
                $("#shipment_pickup_city").val(resp.pickup_city);
                $("#shipment_vehicle_type_id").val(resp.vehicle_type_id);$("#shipment_vehicle_type_id option:not(:selected)").prop("disabled" , true);
                $("#shipment_loading_time").prop("disabled" , "disabled"); $("#shipment_loading_time").val(resp.loading_time);
                $("#shipment_unloading_time").prop("disabled" , "disabled"); $("#shipment_unloading_time").val(resp.unloading_time);


            },
            error: function (resp) {
                console.log("Error getting Location")
            }

        })
    }
    else if (location_id == "")
    {
        $("#shipment_pickup_location").removeProp("disabled");$("#shipment_pickup_location").val("");
        $("#shipment_pickup_building_name").removeProp("disabled");$("#shipment_pickup_building_name").val("")
        $("#shipment_drop_building_name").removeProp("disabled");$("#shipment_drop_building_name").val("")

        $("#shipment_pickup_lat").val(""); $("#shipment_pickup_lng").val("");
        $("#shipment_drop_location").removeProp("disabled"); $("#shipment_drop_location").val("")
        $("#shipment_drop_lat").val(""); $("#shipment_drop_lng").val("");

        // $('option:not(:selected)')
        $("#shipment_vehicle_type_id option:not(:selected)").removeProp("disabled"); $("#shipment_vehicle_type_id").val("");
        $("#shipment_loading_time").removeProp("disabled"); $("#shipment_loading_time").val("");
        $("#shipment_unloading_time").removeProp("disabled"); $("#shipment_unloading_time").val("");

    }
}

$(document).on('turbolinks:load', function() {
    console.log("=======Turblinks:load=========")
    $('.js-example-basic-multiple').select2();
    if($('.form-visual').length > 0){
        $("#vehicle_type_id").attr("disabled","true").addClass("disabled");
    }
    $(".list-group-item img").error(function () {
        $(this).attr("src", "/document_silhouette.jpg")
    })
    $(".documents_list img").error(function () {
        $(this).attr("src", "/assets/document_silhouette.jpg")
    })
    $(".documents_list img").error(function () {
        $(this).attr("src", "/document_silhouette.jpg")
    })
    $('.search-datepicker').datepicker({
        format: 'yyyy-mm-dd'
    })
    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date(),
        orientation: "bottom"

    })
    $(".datepicker1").datepicker({
        format: 'yyyy-mm-dd',
        startDate: "1990-01-01",
        orientation: "bottom"

    })
    $(".datepicker2").datepicker({
        format: 'dd/mm/yyyy',
        startDate: "01/01/1990",
        orientation: "bottom"

    })



    $('.timepicker').timepicker({
        showInputs: false
    })
    // $("#shipment_pickup_date , #shipment_expected_drop_off").focusout(function () {
    //     if ( $("#shipment_expected_drop_off").val() !== "" &&  !($("#shipment_pickup_date").datepicker('getDate') <= $("#shipment_expected_drop_off").datepicker('getDate')))
    //     {
    //         alert("Pick up date should be less than expected drop off date")
    //         $("#shipment_expected_drop_off").val("")
    //     }
    // })
    $(".greater_than_zero").focusout(function (e) {
        // debugger
        if($(this).val() &&  $(this).val() <= 0) {
            $(this).val("")
            if ($(this).parent().find(".error-block").length == 0)
            {
                $(this).parent().append("<span class=\"error-block\">Value should be greater than zero.</span>")
            }
        }
        else {
            $(this).parent().find(".error-block").remove()
        }
    })
    if($('#vehicle_available').is(":checked")){
        $(".not_available").hide()
        $("#vehicle_not_available_from").removeAttr("required");
        $("#vehicle_not_available_to").removeAttr("required");

    }

    $('#vehicle_available').change(function() {
       if($(this).is(":checked")) {
           $(".not_available").hide()
           $("#vehicle_not_available_from").removeAttr("required");
           $("#vehicle_not_available_to").removeAttr("required");
       }else{
        $(".not_available").show()
       $("#vehicle_not_available_from").attr("required" , "true")
       $("#vehicle_not_available_to").attr("required" , "true")
       }
   });
    // window.shipment_pickup_location = ""
    if($(".edit_shipment").length == 1)
    {
        if($("#shipment_location_id").val() != "" &&  $("#shipment_location_id").val() != undefined)
        {
            $("#shipment_pickup_location").prop("disabled" , "disabled");
            $("#shipment_drop_location").prop("disabled" , "disabled");
            $("#shipment_loading_time").prop("disabled" , "disabled");
            $("#shipment_unloading_time").prop("disabled" , "disabled");
            $("#shipment_vehicle_type_id option:not(:selected)").prop("disabled" , true);
        }

    }
    if($("#shipment_bids").length > 0)
    {
        if(!$.fn.dataTable.isDataTable( '#shipment_bids' ))
        {
            setTimeout(function(){
                var shipment_bids_table = $("#shipment_bids").DataTable(dataTable_options());
            })
        }
    }
    if($("#shipments_listing").length > 0)
    {
        if(!$.fn.dataTable.isDataTable( '#shipments_listing' ))
        {
            setTimeout(function(){
                var shipments_listing_table = $("#shipments_listing").DataTable(dataTable_options());
            })
        }
    }

    if($(".admin_listings").length > 0)
    {
        if(!$.fn.dataTable.isDataTable( '.admin_listings' ))
        {
            setTimeout(function(){
                var admin_listings = $(".admin_listings").DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    responsive: true,
                    "order": []
                } );
            })

        }
    }
    if($(".activity_listings").length > 0)
    {
        if(!$.fn.dataTable.isDataTable( '.activity_listings' ))
        {
          setTimeout(function () {
            var activity_listings = $(".activity_listings").DataTable( {
                dom: 'Bfrtip',
                bPaginate: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                responsive: true,
                "order": []
            } );
          })  
        }
    }

    if($("#pending").length > 0)
    {
        if(!$.fn.dataTable.isDataTable( '#pending' ))
        {
            setTimeout(function () {
                var pending_payments= $("#pending").DataTable( dataTable_options());
            })
        }
    }
    $('#pending , #completed').on( 'page.dt , length.dt , search.dt  ,order.dt', function ( e, settings, len ) {
        init_payment_status_listeners();
    });
    $(".card-body a.nav-link").click(function () {
        if($("#completed").length > 0)
        {
            setTimeout(function () {
                if (!$.fn.DataTable.isDataTable( '#completed' )){
                    var completed_payments = $("#completed").DataTable( dataTable_options());
                    datatable_row_expand_listener()
                }
            })
        }
        if($("#company").length > 0)
        {
            setTimeout(function () {
                if (!$.fn.DataTable.isDataTable( '#company' )){
                    var company_listings = $("#company").DataTable(dataTable_options());
                    datatable_row_expand_listener()
                }
            })
        }
    })
    document.addEventListener("turbolinks:before-cache", function() {
        if (shipment_bids_table && shipment_bids_table !== null) {
            shipment_bids_table.destroy();
            shipment_bids_table = null;
        }
        if(shipment_bids_table && shipments_listing_table  !== null){
            shipments_listing_table.destroy();
            shipments_listing_table = null;
        }
        if (shipment_bids_table && admin_listings != null){
            admin_listings.destroy();
            admin_listings = null;
        }
    });
    $('#rating ,.rating').barrating({
        theme: 'fontawesome-stars',
        allowEmpty: true,
        deselectable: true,
        readonly: true
    });



    $("ul.navbar-nav li a.openmenu").click(function(){
        if($("body").hasClass("sidebar-collapse")){
            $(".logo").removeClass("small-logo");

            if($(window).width()<= 991){

                $("#map").css("margin-left", 0);
                $("#map").css("transition", "all linear 0.5s");
                $("#map").css("width", "100%");


            }
            else {
                $("#map").css("margin-left", 250+"px");
                $("#map").css("transition", "all linear 0.5s");
                $("#map").css("width", "calc(100% - 74px)");

                $(".navbar-fixed-top").css("width","calc(100% - 250px)");
                $(".navbar-fixed-top").css("margin-left","250px");
            }
        }
        else {
            $(".logo").addClass("small-logo");

            if($(window).width()<= 991){

                $("#map").css("margin-left", "0");
                $("#map").css("transition", "all linear 0.5s");
                $("#map").css("width", "100%");

                $(".navbar-fixed-top").css("width","100%");
                $(".navbar-fixed-top").css("margin-left","0");
            }
            else {

                $("#map").css("margin-left", 74+"px");
                $("#map").css("transition", "all linear 0.5s");
                $("#map").css("width", "calc(100% - 74px)");

                $(".navbar-fixed-top").css("width","calc(100% - 4.6rem)");
                $(".navbar-fixed-top").css("margin-left","4.6rem");
            }

            // if($(window).width()<= 991){
            //     $(".navbar-fixed-top").css("width","calc(100% - 250px)");
            //     $(".navbar-fixed-top").css("margin-left","250px");
            // }

        }
    });
    $("ul.notifications").click(function () {
        if($(".notifications_count").length > 0)
        {
            $.ajax({
                type: "post",
                url: "/notifications/mark_viewed",
                success: function (resp) {
                    $("span.notifications_count").remove()
                    console.log("SUCCESS NOTIF")
                },
                error: function (resp) {
                    console.log("Failure NOTIF")
                }
            })
        }
    })
    datatable_row_expand_listener()
    init_company_status_listeners()
    init_payment_status_listeners()
    direct_upload_to_s3()
})
function datatable_row_expand_listener() {
    $('#company tbody , #individual tbody').on('click', 'td.details-control', function () {
        setTimeout(function () {
            init_company_status_listeners()
            init_payment_status_listeners()
        })
    })
}
function dataTable_options() {
    return {
        responsive: true,
        autoWitdh: false,
        "info": false,
        "order": [],
        "fnDrawCallback": function(oSettings){
            // var rowCount = this.fnSettings().fnRecordsDisplay();
            // if(rowCount<=10){
            //     $('.dataTables_length').hide();
            // }
        }
    }
}
function init_payment_status_listeners() {
    setTimeout(function () {
        elements = $("input.payment_paid_by_cargo , input.payment_paid_to_fleet , input.cancel_penalty_amount_received, input.payment_lorryz_comission_received,input.payment_moderator_paid_by_cargo , input.payment_moderator_paid_to_fleet , input.cancel_moderator_penalty_amount_received, input.payment_moderator_lorryz_comission_received")
        $(elements).unbind("click");
        $(elements).off()
        $("input.payment_paid_by_cargo").click(function (e) {
            shipment_id = $(this).attr("data-id")
            if (confirm("Reconfirm amount is paid by cargo owner?")) {
                $.ajax({
                    type: "post",
                    url: "/admin/payments/mark-paid-by-cargo/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })
        $("input.payment_moderator_paid_by_cargo").click(function (e) {
            shipment_id = $(this).attr("data-id")
            if (confirm("Reconfirm amount is paid by cargo owner?")) {
                $.ajax({
                    type: "post",
                    url: "/moderator/payments/mark-paid-by-cargo/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })
        $("input.payment_paid_to_fleet").click(function (e) {
            shipment_id = $(this).attr("data-id")
            if (confirm("Reconfirm fleet owner has been paid?")) {
                $.ajax({
                    type: "post",
                    url: "/admin/payments/mark-paid-to-fleet/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })
        $("input.payment_moderator_paid_to_fleet").click(function (e) {
            shipment_id = $(this).attr("data-id")
            if (confirm("Reconfirm fleet owner has been paid?")) {
                $.ajax({
                    type: "post",
                    url: "/moderator/payments/mark-paid-to-fleet/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })

        $("input.cancel_penalty_amount_received").click(function (e) {
            shipment_id = $(this).attr("data-id")
            if (confirm("Reconfirm cancel penalty amount received?")) {

                $.ajax({
                    type: "post",
                    url: "/admin/payments/cancel_penalty_amount_received/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })
       
        $("input.cancel_moderator_penalty_amount_received").click(function (e) {
            shipment_id = $(this).attr("data-id")
            if (confirm("Reconfirm cancel penalty amount received?")) {

                $.ajax({
                    type: "post",
                    url: "/moderator/payments/cancel_penalty_amount_received/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })

        $("input.payment_lorryz_comission_received").click(function (e) {
            if (confirm("Reconfirm Lorryz commission received?")) {
                shipment_id = $(this).attr("data-id")
                $.ajax({
                    type: "post",
                    url: "/admin/payments/mark-lorryz-comission-received/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                        // setTimeout(function () {
                        //     $(e.target).prop("checked", !($(e.target).val()));
                        // })
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })
        $("input.payment_moderator_lorryz_comission_received").click(function (e) {
            if (confirm("Reconfirm Lorryz commission received?")) {
                shipment_id = $(this).attr("data-id")
                $.ajax({
                    type: "post",
                    url: "/moderator/payments/mark-lorryz-comission-received/" + shipment_id,
                    success: function (resp) {
                        console.log("success")
                        // setTimeout(function () {
                        //     $(e.target).prop("checked", !($(e.target).val()));
                        // })
                    },
                    error: function (resp) {
                        console.log("Something went wrong!")
                    }
                })
            }
            else {
                e.preventDefault()
            }
        })
    })
}
function init_company_status_listeners() {
    setTimeout(function () {
        elements = $("input.company_verified ,input.company_moderator_verified, input.company_featured , input.company_moderator_featured , input.company_blacklisted , input.company_moderator_blacklisted , input.company_safe_for_cash_on_delivery, input.company_moderator_safe_for_cash_on_delivery, input.tax_invoice")
        $(elements).unbind("click");
        $(elements).off()
        $("input.company_verified").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/admin/dashboard/mark-verified/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
        $("input.company_moderator_verified").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/moderator/dashboard/mark-verified/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
        $("input.company_featured").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/admin/dashboard/mark-featured/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
        $("input.company_moderator_featured").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/moderator/dashboard/mark-featured/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })

        $("input.company_blacklisted , .company_blacklisted .iCheck-helper").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/admin/dashboard/mark-blacklisted/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
        $("input.company_moderator_blacklisted , .company_blacklisted .iCheck-helper").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/moderator/dashboard/mark-blacklisted/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
        $("input.company_safe_for_cash_on_delivery ,.company_safe_for_cash_on_delivery .iCheck-helper").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/admin/dashboard/mark-safe-for-cash-on-delivery/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
        $("input.company_moderator_safe_for_cash_on_delivery ,.company_safe_for_cash_on_delivery .iCheck-helper").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/moderator/dashboard/mark-safe-for-cash-on-delivery/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
        $("input.tax_invoice ,.tax_invoice .iCheck-helper").click(function(e){
            company_id = $(this).attr("data-id")
            $.ajax({
                type: "post",
                url: "/admin/dashboard/tax_invoice/"+ company_id,
                success: function (resp) {
                    console.log("success")
                },
                error: function (resp) {
                    console.log("Something went wrong!")
                }
            })
        })
    })
}
function geolocate(obj, country) {
    console.log("-----GeoLocate----called")
    google_auto_complete = new google.maps.places.Autocomplete((obj));
    google_auto_complete.inputId = obj.id;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            google_auto_complete.setBounds(circle.getBounds());
        });
    }
    google.maps.event.addListener(google_auto_complete, 'place_changed', function (event) {

        var country_name;
        var result = this.getPlace();
        if(result.name)
        {
            if (this.inputId == "shipment_pickup_location")
            {
                
                $("#shipment_pickup_building_name").val(result.name)
            }
       


            else if (this.inputId == "shipment_drop_location")
            {
                $("#shipment_drop_building_name").val(result.name)
            }
            else if(this.inputId == "location_pickup_location" )
            {
                $("#location_pickup_building_name").val(result.name)
            }
            else if (this.inputId == "location_drop_location")
            {
                $("#location_drop_building_name").val(result.name)
            }
            else if(this.inputId == "pickup_location" )
            {
            
                $("#pickup_building_name").val(result.name)
            }
            else if (this.inputId == "drop_location")
            {
                $("#drop_building_name").val(result.name)
            }
        }
        if(this.inputId == 'pickup_location' && $('.form-visual').length > 0){
            for(var i = 0; i < result.address_components.length; i += 1) {
                var addressObj = result.address_components[i];
                for(var j = 0; j < addressObj.types.length; j += 1) {
                    if (addressObj.types[j] === 'country') {
                        country_name = addressObj.long_name
                    }
                }
            }

            $.ajax({
                type: "get",
                url:  "/countries/get_vehicles_by_country?filter_by=" + country_name,
                dataType: 'json',
                success: function (resp) {
                    if (resp.vehicle_types.length > 0) {
                        $("#vehicle_type_id").removeAttr("disabled").removeClass("disabled");
                        var $el = $("#vehicle_type_id");
                        $el.empty(); // remove old options
                        $el.append($("<option> Specify Type of Truck Required? </option>"));
                        $.each(resp.vehicle_types, function(index,obj) {
                            $el.append($("<option></option>")
                                .attr("value", obj.id).text(obj.name));
                        });
                    }else{
                        $("#vehicle_type_id").empty().append($("<option> Specify Type of Truck Required? </option>"));
                        $("#vehicle_type_id").attr("disabled","true").addClass("disabled");
                    }


                },
                error: function (resp) {
                    console.log("Error getting Location")
                    $("#vehicle_type_id").empty().append($("<option> Specify Type of Truck Required? </option>"));
                    $("#vehicle_type_id").attr("disabled","true").addClass("disabled");
                }
            })
        }

        if(this.inputId.indexOf("shipment_pickup_location") >= 0)
        {
          
            $("#shipment_pickup_lat").val(this.getPlace().geometry.location.lat())
            $("#shipment_pickup_lng").val(this.getPlace().geometry.location.lng())
            set_city(this.getPlace(), "shipment_pickup_city")
            

        }
        else if(this.inputId.indexOf("pickup_location") >= 0){
            $("#pickup_lat").val(this.getPlace().geometry.location.lat())
            $("#pickup_lng").val(this.getPlace().geometry.location.lng())
            set_city(this.getPlace(), "pickup_city")
            set_country(this.getPlace(), "pickup_country")

        }
        else if(this.inputId.indexOf("drop_location") >= 0){
            $("#drop_lat").val(this.getPlace().geometry.location.lat())
            $("#drop_lng").val(this.getPlace().geometry.location.lng())
            set_city(this.getPlace(), "drop_city")
            set_country(this.getPlace(), "drop_country")

        }
        
         if(this.inputId.indexOf("shipment_drop_location") >= 0){

            $("#shipment_drop_lat").val(this.getPlace().geometry.location.lat())
            $("#shipment_drop_lng").val(this.getPlace().geometry.location.lng())
            set_city(this.getPlace(), "shipment_drop_city")
            
        }
        if(this.inputId.indexOf("location_pickup_location") >= 0)
        {   
            $("#location_pickup_lat").val(this.getPlace().geometry.location.lat())
            $("#location_pickup_lng").val(this.getPlace().geometry.location.lng())
            set_city(this.getPlace(), "location_pickup_city")
        }
        else if(this.inputId.indexOf("location_drop_location") >= 0){
            $("#location_drop_lat").val(this.getPlace().geometry.location.lat())
            $("#location_drop_lng").val(this.getPlace().geometry.location.lng())
            set_city(this.getPlace(), "location_drop_city")
        }

    });
}
function set_city(place , id) {
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
            if (place.address_components[i].types[j] == "locality") {
                return  $("#"+id).val(place.address_components[i].long_name)
            }
        }
    }
}


function set_country(place , id) {

    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
            if (place.address_components[i].types[j] == "country") {
                return  $("#"+id).val(place.address_components[i].long_name)
            }
        }
    }
}
var get_prorate_calculation, validate_for_pro_rate_calculation;

get_prorate_calculation = function() {
    $('#new_shipment').focusout(function() {
        console.log('getting prorate');
        if (validate_for_pro_rate_calculation()) {
            $.ajax({
                type: 'POST',
                url: ' /cargo/shipments/calculate_fare',
                data: $('#new_shipment').serialize(),
                success: function(resp) {
                    var per_vehicle_fare, total_fare;
                    $('.prorate').removeClass('hide');
                    per_vehicle_fare = parseInt(resp.fare);
                    $('form .per_vehicle_fare').text(per_vehicle_fare);
                    total_fare = parseInt($('#shipment_no_of_vehicles').val() * resp.fare);
                    $('form .total_fare').text(total_fare);
                    $('form .currency').text(resp.currency)
                    $(".row.prorate").removeClass("hide")
                    console.log('-------__Success------------=');
                },
                error: function(resp) {
                    console.log('===========-=-=FAILED==========-=-');
                }
            });
        }
        else {
            $(".row.prorate").addClass("hide")
        }
    });
};

validate_for_pro_rate_calculation = function() {
    return  $('#shipment_pickup_location').val() !== '' && $('#shipment_drop_location').val() !== '' && $('#shipment_loading_time').val() !== '' && $('#shipment_unloading_time').val() !== '' && $('#shipment_vehicle_type_id').val() !== '';
};

direct_upload_to_s3 = function () {
    window.file_urls = []
    var upload, uploadHandler, uploadWithUrl;
    uploadWithUrl = function(form, file, presignedUrl, publicUrl, field) {
        var submit, xhr;
        submit = form.find("button[type='submit'], input[type='submit']").attr('disabled', true);
        //
        $(field).parents(".form-group ").find('.js-signed-upload-status').text('Uploading...');
        xhr = new XMLHttpRequest();
        xhr.open('PUT', presignedUrl);
        xhr.setRequestHeader('Content-Type', file.type);
        xhr.onload = function() {
            //
            submit.removeAttr('disabled');
            if (xhr.status === 200) {
                // $('.js-signed-upload-value').val(publicUrl);
                //  $(field).parents(".form-group ").find(".attached_files.container").text("");
                return $(field).parents(".form-group ").find('.js-signed-upload-status').text("");

            }
        };
        xhr.onerror = function() {
            submit.removeAttr('disabled');
            return $('.js-signed-upload-status').text('Failed to upload. Try uploading a smaller file.');
        };
        xhr.send(file);
    };

    upload = function(form, file, url, field) {
        return $.ajax({
            url: url + '?filename=' + file.name + '&filetype=' + file.type,
            method: 'PUT',
            accept: 'json'
        }).success(function(data) {
            window.file_urls.push(data.public_url)
            return uploadWithUrl(form, file, data.presigned_url, data.public_url,field);
        });
    };

    uploadHandler = function(field) {

        var file, form;
        file = field.files[0];
            if (file === null) {
            return;
        }
        if(!validate_file(file, field)){
            alert("Invalid File Format!")
            $(field).val("")
            return;
        }
        form = $(field).parents("form").eq(0);
        $.each(field.files, function (index, file) {
            upload(form, file, field.dataset.presignUrl, field);
        })
    };
    var validate_file = function (file, field) {
        filetype = file.type
        if ($(field).parents("div").first().find("#company_information_avatar , #individual_information_avatar ").length == 1)
        {
            return filetype.includes("jpg") || filetype.includes("jpeg") || filetype.includes("png")
        }
        else {
            return filetype.includes("jpg") || filetype.includes("jpeg") || filetype.includes("png")
                || filetype.includes("xls") ||   filetype.includes("xlsx") || filetype.includes("docx") ||
                filetype.includes("pdf") || filetype.includes("doc") || filetype.includes("vnd.ms-excel")
            || filetype.includes("vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        }
    }
    return $('.js-signed-upload').change(function() {
        uploadHandler(this);
        var self = this
        setTimeout(function () {
            old_values = ""
            if($(self).prev().val() != "")
            {
                try {
                    old_values = eval($(self).prev().val())
                } catch (e) {
                    old_values = $(self).prev().val().split(" ")
                }
            }
            window.file_urls  = $.merge(window.file_urls , old_values).filter(function(e){return e});
            $(self).prev().val( JSON.stringify(window.file_urls));
            window.file_urls = []
        },1000)
    });
}

function append_date_range_params(obj) {
    start_date = $("#start_date").val();
    end_date = $("#end_date ").val();
    obj.href = obj.href.split('?')[0]
    link = "?start_date="+start_date+"&end_date="+end_date
    obj.href = obj.href+ link
}