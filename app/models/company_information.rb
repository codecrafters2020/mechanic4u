class CompanyInformation < ApplicationRecord
	audited
  belongs_to :company
  after_create :blacklist_company
  # has_many_attached :attachments # , dependent: :destroy
  scope :filter_by_country, ->(country_id){joins(:company).merge(Company.get_by_country(country_id))}
  scope :cargo_company, -> {joins(:company).merge(Company.cargo_companies)}

	scope :filter_expired_company, -> (date) { where('license_expiry_date  <=? or ( (  length(trade_registration_expiry) > 0   ) and (trade_registration_expiry  <=?))   ',date,date)}
  scope :fleet_company, -> {joins(:company).merge(Company.fleet_companies)}

  # scope :cargo_company, -> { joins("INNER JOIN users on users.company_id = company_informations.company_id").merge(User.company_cargo_owners) }

  # mount_base64_uploader :avatar, AvatarUploader
  # mount_base64_uploaders :documents, DocumentUploader
  def complete?
    name.present? and address.present? and license_number.present? and license_expiry_date.present?
  end

  def blacklist_company
		self.company.update_column(:blacklisted, true)
	end
end
