class Notification < ApplicationRecord
	require 'action_view'
	require 'action_view/helpers'
	include ActionView::Helpers::DateHelper
	paginates_per 10
		
	belongs_to :notifiable, polymorphic: true
  belongs_to :mentioned_by, class_name: "User",optional: true
  belongs_to :user_mentioned, class_name: "User", optional: true


  def creation_time
  	time_ago_in_words(self.created_at) + ' ago'
	end

	def self.from_last_week user
		user.notifications.order(created_at: :desc)
	end
  
end
