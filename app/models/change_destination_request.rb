class ChangeDestinationRequest < ApplicationRecord
	audited
	belongs_to :shipment
end
