class PreferredLocation < ApplicationRecord
	audited

	extend Enumerize
	has_many :users
	
	enumerize :process, in: {:bidding => 1, :prorate => 2}, scope: true, predicates:true

  def display_name
    "#{country_name.try(:capitalize)} | #{city_name.try(:capitalize)} "
	end
end
