class ShipmentFleet < ApplicationRecord
	audited
	belongs_to :shipment
	belongs_to :company
	validates_presence_of :shipment_id, :company_id
	# scope :shipment_active, ->{where(shipment_fleets: { status: ["booked","start","loading","enroute","unloading"]})}
	scope :shipment_active, ->{joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").merge(Shipment.shipment_active)}
	
	scope :shipment_driver, -> (shipment_id,fleet_id) { where(:company_id => fleet_id,:shipment_id => shipment_id) }

	scope :individual_shipment_active, ->(shipment_id) { where('shipment_fleets.shipment_id =?',shipment_id) }
	scope :fleet_individual_shipments, -> (fleet_id) { where('shipment_fleets.company_id =?',fleet_id)}

	scope :shipment_active_fleet, -> (fleet_id) {joins("INNER JOIN shipments on shipments.id = shipment_fleets.shipment_id").merge(Shipment.shipment_active).where('shipment_fleets.company_id =?',fleet_id)}

	extend Enumerize
	enumerize :status, in: {
							:booked => 1,
							:start => 2,
							:loading => 3,
							:enroute => 4,
							:unloading => 5,
							:finish => 6 }, scope: true, predicates: true

	def status_code
		if status == "booked"
			100
		elsif status == 'start'
			200
		elsif status == 'loading'
			300
		elsif status == 'enroute'
			400
		elsif status == 'unloading'
			500
		elsif status == 'finish'
			600
		end 
	end


end
