class Company < ApplicationRecord

	audited
	has_one :individual_information , dependent: :destroy
	has_one :company_information , dependent: :destroy
	has_many :shipments

    has_many :locations
	has_many :vehicles
	has_many :fleet_shipments, foreign_key: :fleet_id, class_name: 'Shipment'
	has_many :given_ratings, foreign_key: :giver_id, class_name: 'CompanyRating'
	has_many :received_ratings, foreign_key: :receiver_id, class_name: 'CompanyRating'

	has_many :users
	accepts_nested_attributes_for :individual_information# ,reject_if: proc { |attributes| attributes['title'].blank? }
	accepts_nested_attributes_for :company_information # ,reject_if: proc { |attributes| attributes['title'].blank? }

	has_many :bids
	has_one :fleet_owner, -> { where(role: :fleet_owner) }, class_name: 'User'
	has_one :cargo_owner, -> { where(role: :cargo_owner) }, class_name: 'User'
	extend Enumerize

	enumerize :category, in: {:cargo => 1, :fleet => 2}, scope: true, predicates: true
	enumerize :company_type, in: {:individual => 1, :company => 2}, scope: true, predicates:true

	scope :cargo, -> { where(category: 'cargo') }
	
	scope :individuals, -> { where(company_type: "individual") }
	scope :companies, -> { where(company_type: "company") }
	scope :cargo_companies, -> { where(company_type: "company" ,category: :cargo) }
	scope :fleet_companies, -> { where(company_type: "company" ,category: :fleet) }
	scope :cargo_indiv, -> { where(company_type: "individual" ,category: :cargo) }
	scope :fleet_indiv, -> { where(company_type: "individual" ,category: :fleet) }

  	scope :fleet, -> { where(category: 'fleet') }
  	scope :get_by_country, -> (country_id){joins(:users).where('users.country_id = ?', country_id)}


	# has_many :drivers, foreign_key: :company_id, class_name: 'User'
	#equivalent to but includes fleet_owner as well
	has_many :drivers, -> {where(role: :driver)}, class_name: 'User'

	validates :company_type, presence: true

	def available_drivers
		users.where.not(role: :fleet_owner).reject{|driver| driver.vehicle.present? }
	end
	def average_rating
		received_ratings.count > 0 ? (received_ratings.sum(:rating) / received_ratings.count).round(1) : 5 rescue 0
	end

	def get_payment_options
		# [ "Pay by Credit/Debit Card",:credit_card ]
		credit_period_days = credit_period.to_f > 0 ? credit_period : self.country.cargo_max_days
		payment_options = [["Cash / Bank transfer during Pick-up",:before_pick_up]]
		payment_options << ["Cash / Bank transfer prior delivery",:after_delivery] if safe_for_cash_on_delivery?
		payment_options << ["Credit " + credit_period_days.try(:to_i).try(:to_s) + " Days", "credit_period"]
		payment_options
	end

	def owner
		users.where(role: ["cargo_owner" , "fleet_owner"]).first
	end

	def profile
		return self.individual_information if self.company_type == "individual"
		return self.company_information if self.company_type == "company"
	end

	def company_name
		return self.try(:company_information).try(:name)  if self.company_type == "company"
		return self.try(:users).try(:first).try(:full_name) if self.company_type == "individual"
	end

	def company_contact
		return self.try(:users).try(:first).try(:mobile) #if self.company_type == "individual"
	end

	def company_address
		return self.try(:company_information).try(:address)  if self.company_type == "company"
		"" if self.company_type == "individual"
	end

	def company_phone
		return self.try(:users).try(:first).try(:mobile)  if self.company_type == "company"
		return self.try(:users).try(:first).try(:mobile) if self.company_type == "individual"
	end

	def create_profile
		self.individual_information if self.company_type == "individual"
		self.company_information if self.company_type == "company"
	end
	def individual_cargo?
		individual? && cargo?
	end

	def company_cargo?
		company? && cargo?
	end

	def individual_fleet?
		individual? && fleet?
	end

	def company_fleet?
		company? && fleet?
	end

	def country
		users.try(:first).try(:country)
	end

	def is_max_period_amount_overdue?
		if cargo?
			cargo_max_period_amount_overdue?
		elsif fleet?
			fleet_max_period_amount_overdue?
		end
	end

	def cargo_max_period_amount_overdue?
		c_shipments = shipments.where(state: "completed", paid_by_cargo: false)
		if self.is_contractual
			(c_shipments.sum(:amount) > credit_amount || c_shipments.where("invoice_date < ?", credit_period.days.ago).count > 0) rescue false
		else
			(c_shipments.sum(:amount) > country.cargo_max_amount || c_shipments.where("invoice_date < ?", country.cargo_max_days.days.ago).count > 0) rescue false
		end
	end

	def fleet_max_period_amount_overdue?
		receivable = fleet_shipments.where(state: "completed",paid_to_fleet: false, payment_option: ["credit_card","bank_remittance","credit_period"]).sum(:fleet_income)

		payable_shipments = fleet_shipments.where(state: "completed", lorryz_comission_received: false, payment_option: ["before_pick_up","after_delivery"])
		payable = payable_shipments.sum(:lorryz_share_amount)

		total_payable = payable - receivable
		(total_payable > country.fleet_max_amount || payable_shipments.where("invoice_date < ?", country.fleet_max_days.days.ago).count > 0) rescue false
	end

	def credit_days
		if credit_period and credit_period > 0
			credit_period
		else
			country.cargo_max_days
		end
	end

end
