class IndividualInformation < ApplicationRecord
  audited
  belongs_to :company
  after_create :blacklist_company
  # has_many_attached :attachments , dependent: :destroy
  scope :cargo_company, -> {joins(:company).merge(Company.cargo_companies)}
  scope :cargo_indi, -> {joins(:company).merge(Company.cargo_indiv)}
  scope :fleet_indi, -> {joins(:company).merge(Company.fleet_indiv)}


  scope :filter_expired_cargo_indiv, -> (date) { where(' (( length(expiry_date) > 0   ) and (expiry_date  <=?)) ',date)}
  scope :filter_expired_fleet_indiv, -> (date) { where(' (( length(expiry_date) > 0   ) and (expiry_date  <=?))  or ( (  length(passport_no_expiry) > 0   ) and (passport_no_expiry  <=?)) or ( (  length(visa_no_expiry) > 0   ) and (visa_no_expiry  <=?)) or ( (  length(license_no_expiry) > 0   ) and (license_no_expiry  <=?))   ',date,date,date,date)}

  scope :filter_by_country, ->(country_id){joins(:company).merge(Company.get_by_country(country_id))}


  # mount_base64_uploader :avatar, AvatarUploader
  # mount_base64_uploaders :documents, DocumentUploader
  def complete?
    national_id.present? and expiry_date.present?
  end

  def blacklist_company
    if self.company.company_type.individual? and self.company.category.cargo?
            self.company.update_column(:blacklisted, false)
    else
            self.company.update_column(:blacklisted, true)
    end
	end
end
