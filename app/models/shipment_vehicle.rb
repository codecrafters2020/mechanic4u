class ShipmentVehicle < ApplicationRecord
	audited
	belongs_to :shipment
	belongs_to :vehicle
	validates_presence_of :shipment_id, :vehicle_id
	after_save :send_push_notifications
	after_save :create_action_date
	extend Enumerize
	enumerize :status, in: {
							:booked => 1,
							:start => 2,
							:loading => 3,
							:enroute => 4,
							:unloading => 5,
							:finish => 6 }, scope: true, predicates: true

	ONGOING_VEHICLES = [:enroute, :start , :loading, :unloading , :finish]

	ONGOING_LOADING_VEHICLES = [:enroute, :start , :loading,:booked ]

	def status_code
		if status == "booked"
			100
		elsif status == 'start'
			200
		elsif status == 'loading'
			300
		elsif status == 'enroute'
			400
		elsif status == 'unloading'
			500
		elsif status == 'finish'
			600
		end 
	end

	def send_push_notifications
		if status == 'start'
			notification("Shipment no #{shipment_id} - Vehicle (#{vehicle.registration_number}) is on its way to pick up the cargo")
		elsif status == 'loading'
			notification("Shipment no #{shipment_id} - Vehicle (#{vehicle.registration_number}) is loading cargo at pick up location")
		elsif status == 'enroute'
			notification("Shipment no #{shipment_id} - Vehicle (#{vehicle.registration_number}) is on its way to delivery location")
		elsif status == 'unloading'
			notification("Shipment no #{shipment_id} - Vehicle (#{vehicle.registration_number}) is unloading cargo at delivery location")
		elsif status == 'finish'
			notification("Shipment no #{shipment_id} - Vehicle (#{vehicle.registration_number}) has finished unloading cargo")
		end
	end

	def create_action_date
    ShipmentActionDate.create(shipment_id: self.shipment_id, vehicle_id: self.vehicle_id,
															state: status, performed_at: DateTime.now)
  	end

	def notification text
		# Cargo Notification 
    user = shipment.company.cargo_owner
    # dispathcer = SendSMS.new
    # message = dispathcer.message(user.mobile, text)
    Notification.create(
        notifiable_id: shipment_id, 
        notifiable_type: "Shipment", 
        body: text, 
        user_mentioned_id: user.id)
    PushNotification.send_notification(
     user.os,
     user.device_id,
     "",
     shipment_id,
     text,
	 nil,
    ) if user.device_id  && user.os != "ios"


	# Fleet Notification 
	if shipment.fleet.present?
	  if shipment.fleet.company_type == "company" && shipment.fleet.fleet_owner.present?
	    user = shipment.fleet.fleet_owner
	    # dispathcer = SendSMS.new
	    # message = dispathcer.message(user.mobile, text)
	    Notification.create(
	        notifiable_id: shipment_id, 
	        notifiable_type: "Shipment", 
	        body: text, 
	        user_mentioned_id: user.id)
	    PushNotification.send_notification(
	     user.os,
	     user.device_id,
	     "",
	     shipment_id,
	     text,
		 nil,
	    ) if user.device_id && user.os != "ios"
	  end
	end
end
end
