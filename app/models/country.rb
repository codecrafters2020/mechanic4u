class Country < ApplicationRecord
	audited

	extend Enumerize
	has_many :users
	has_many :gps
	has_many :shipments
	has_many :vehicle_types
  has_many :locations, dependent: :destroy

	validates_uniqueness_of :dialing_code

	enumerize :process, in: {:bidding => 1, :prorate => 2}, scope: true, predicates:true

  def display_name
    "#{name.try(:capitalize)} (#{dialing_code})"
	end
end
