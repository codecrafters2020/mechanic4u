class User < ApplicationRecord
  audited
  # tracked owner: Proc.new{ |controller, model| controller.try(:current_user).present? ? controller.current_user : model.full_name}

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  acts_as_paranoid
  validates :email, :uniqueness => {:allow_blank => true}
  attr_writer :login
  audited
  # has_many_attached :attachments
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
    validates :mobile, presence: true, uniqueness: true
    # validates :role, presence: true
    extend Enumerize
    include PhoneVerificationConcern
    enumerize :role, in: {
  												:superadmin => 1,
  												:admin => 2,
  												:cargo_owner => 3,
  												:fleet_owner => 4,
  												:cargo => 5,
  												:fleet=> 6,
                          :driver=> 7,
                          :moderator => 8,
                          :vendor => 9 }, scope: true, predicates: true

    belongs_to :company, optional: true
    belongs_to :country, optional: true
    has_one :driver_information
    has_one :session_log
    has_one :vehicle
    scope :drivers, -> {where(role: :driver)}
    scope :fleet_owners ,-> {where(role: :fleet_owner)}
    scope :individual_fleet_owners ,-> {joins(:company).merge(Company.individuals).where(role: :fleet_owner)}
    scope :individual_cargo_owners ,-> {joins(:company).merge(Company.individuals).where(role: :cargo_owner)}
    scope :expired_fleet_users, ->(date){ joins("INNER JOIN individual_informations on users.company_id = individual_informations.company_id").merge(IndividualInformation.filter_expired_fleet_indiv(date))}
    scope :filter_by_expired_fleet, -> (company_id ,date) { where('users.company_id =?',company_id).merge(User.expired_fleet_users(date)) }

    scope :company_fleet_owners ,-> {joins(:company).merge(Company.companies).where(role: :fleet_owner)}
    scope :company_cargo_owners ,-> {joins(:company).merge(Company.companies).where(role: :cargo_owner)}

    scope :cargo_owners ,-> {where(role: :cargo_owner)}
    scope :admins ,-> {where(role: :admin)}
    scope :superadmins ,-> {where(role: :superadmin)}
    scope :moderator ,-> {where(role: :moderator)}
    scope :vendor ,-> {where(role: :vendor)}
    scope :filter_by_user, -> (user_id) { where('users.id =?',user_id) }
    scope :filter_by_active_shipments, ->(date){joins("INNER JOIN shipments on users.company_id = shipments.company_id").merge(Shipment.filter_active_shipment(date))}
    scope :filter_by_company, -> (company_id) { where('users.company_id =?',company_id) }

    scope :filter_by_country_id, -> (country_id) { where('users.country_id =?',country_id)}
    # scope :busy_fleet_owners ,-> {joins(:shipments).merge(Shipments\.shippment_active)}
    scope :busy_fleet_owners, -> { joins("INNER JOIN shipments on users.company_id = shipments.fleet_id").merge(Shipment.shipment_active) }
    scope :busy_fleet_individual_owners, -> { joins("INNER JOIN shipment_fleets on users.company_id = shipment_fleets.company_id").merge(ShipmentFleet.shipment_active) }
    scope :shipment_fleet_individual_owners, ->(shipment_id){ joins("INNER JOIN shipment_fleets on users.company_id = shipment_fleets.company_id").merge(ShipmentFleet.individual_shipment_active(shipment_id))}
    scope :fleet_individual_owners, ->(shipment_id){ joins("INNER JOIN shipment_fleets on users.company_id = shipment_fleets.company_id").where('shipment_fleets.shipment_id =?',shipment_id)}

    #cargo company Shipments
    #fleet company shipments
    accepts_nested_attributes_for :company #,reject_if: proc { |attributes| attributes['title'].blank? }
    has_many :shipments, through: :company
    #driver
    has_one :vehicle
    has_many :assigned_shipments, through: :vehicle,source: :shipments
    has_many :not_interested_shipments
    has_many :notifications, class_name: "Notification", foreign_key: :user_mentioned_id
    has_many :unread_notifications, -> { where("notifications.viewed_flag = ?", false) },  class_name: "Notification", foreign_key: :user_mentioned_id
    def self.fetch_user token
      decoded_token = JWT.decode(token, Rails.application.secrets.secret_key_base)
      User.find(decoded_token[0]["user_id"])
    end

    def set_credit_period_and_amount
      if self.company.present?
        company = self.company
        if fleet_owner?
          credit_amount = country.fleet_max_amount if [0.0 , nil].include? company.credit_amount
          credit_period = country.fleet_max_days if [0.0 , nil].include? company.credit_period
          company.update(credit_amount: credit_amount ,credit_period: credit_period  )
        elsif cargo_owner?
          credit_amount = country.cargo_max_amount if [0.0 , nil].include? company.credit_amount
          credit_period = country.cargo_max_days if [0.0 , nil].include? company.credit_period
          company.update(credit_amount: credit_amount ,credit_period: credit_period  )
        end
      end
    end
    def self.find_by_email_or_phone(params)

      find_by(mobile: params[:user][:email]) || find_by(email: params[:user][:email])

    end

    def self.find_by_email_or_phone_web(params)
      find_by(mobile: params[:user][:email]) || find_by(email: params[:user][:email])
    end

    def get_company_category
      (cargo? or cargo_owner?) ? Company.category.find_value(:cargo).value : Company.category.find_value(:fleet).value
    end

    def build_company_type params
      if params[:user][:company_attributes] and params[:user][:company_attributes][:company_type] == "Individual"
        register_as_individual
      elsif params[:user][:company_attributes] and params[:user][:company_attributes][:company_type] == "Company"
        register_as_company
      end
    end
    def register_as_individual
      build_company(company_type: Company.company_type.find_value(:individual).value, category: get_company_category )
      save
      set_credit_period_and_amount
    end

    def register_as_company
      build_company(company_type: Company.company_type.find_value(:company).value, category: get_company_category )
      save
      set_credit_period_and_amount
    end

    def login=(login)
      @login = self.email
    end

    def email_required?
      false
    end

    def login
      @login || self.mobile || self.email
    end

    def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions.to_h).where(["lower(mobile) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      elsif conditions.has_key?(:mobile) || conditions.has_key?(:email)
        where(conditions.to_h).first
      end
    end
  def full_name
    "#{first_name} #{last_name}"
  end
  class << self
    def change_online_status_at_day_end
      puts "user get!!0"

      online_users = User.individual_fleet_owners
      puts "user get!!1"
      online_users.each do |user|
        d = DateTime.now
        session = nil
        if(user.is_online == true )
          if(user.session_log_id?)
            session = SessionLog.find(user.session_log_id)
            session.end_time = d
            session.save
          else
            session = SessionLog.new(:user_id => user.id, :company_id => user.company_id, :start_time => d, :end_time => d)
            session.save
          end
        end
        session_id = (session != nil) ? session.id : nil
        if user.update_attributes(is_online: "false", :session_log_id => session_id)
          puts "ok #{user}"
        else
          puts "@"*100, user.errors.full_messages

        end
        puts "end funcUser: #{user}"
        #user.update_attributes(:is_online=> "false")
      end
    end
  end
	def formatted_name
		"#{first_name} #{last_name} | #{company.vehicles.try(:first).try(:registration_number)} "
    end
    def formatted_cargo_name
      "#{first_name} #{last_name} "
      end
      def formatted_cargo_name_phone
        " #{first_name} #{last_name} | #{mobile}"
        end
    

    def self.search_cargo(query)
      return all if query_blank?(query)
      # result =GpsVehicle.all
      # User.filter_by_active_shipments(@date).individual_cargo_owners

      @date = Date.today() - 60.day

     result =User.cargo_owners
      result = result.filter_by_country_id(query["country_id"])  if query["country_id"].present?
      result  = result.filter_by_active_shipments(@date).cargo_owners if query["status"]=="Active" &&  query["status"].present? 
      result = result-= result.filter_by_active_shipments(@date).cargo_owners  if query["status"]!="Active"  &&  query["status"].present? 
      result
    end 
    def self.search_fleet(query)
      return all if query_blank?(query)
      # result =GpsVehicle.all
      # User.filter_by_active_shipments(@date).individual_cargo_owners

      @date = Date.today() - 60.day

     result =User.individual_fleet_owners
      result = result.filter_by_country_id(query["country_fleet"])  if query["country_fleet"].present?
      result =result.busy_fleet_owners + result.busy_fleet_individual_owners if query["status_fleet"]=="Busy"  &&  query["status_fleet"].present? 
      result  = result - result.busy_fleet_owners -  result.busy_fleet_individual_owners if query["status_fleet"]!="Busy" &&  query["status_fleet"].present? 

      result
    end 
    def self.query_blank?(query)
      # puts "\n\n\n\t\t  here i m  ss#{query.values.reject(&:blank?).length} \n\n"
  
      query.values.reject(&:blank?).length == 0
    end

    def get_payment_options ()
      # [ "Pay by Credit/Debit Card",:credit_card ]
      # credit_period_days = credit_period.to_f > 0 ? credit_period : self.country.cargo_max_days
      payment_options = [["Cash / Bank transfer during Pick-up",:before_pick_up]]
      payment_options << ["Cash / Bank transfer prior delivery",:after_delivery]
      payment_options << ["Credit " + credit_period_days.try(:to_i).try(:to_s) + " Days", "credit_period"]
      payment_options
    end
   
end
