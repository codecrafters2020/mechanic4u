module PhoneVerificationConcern
  extend ActiveSupport::Concern

  included do
    after_create :assgin_verification_code, :if => :phone_verificaton_required?
    after_create :send_verification_code, :if => :phone_verificaton_required?
    scope :get_verification_code , -> (code) { where(verification_code: code) }

    def send_verification_code
      begin
        dispathcer = SendSMS.new
        message = dispathcer.message(self.mobile,"Verification Code: #{self.verification_code} \nTeam Lorryz")
        self.update_attribute :mobile_verified , "code_sent"
      rescue
        self.update_attribute :mobile_verified , "error"
      end
    end

    def assgin_verification_code
      self.update_attribute :verification_code , unique_verification_code
    end

    def verify_code? code
      if self.verification_code == code 
        self.update_attributes(verification_code: "",mobile_verified: "verified") 
        true
      else
        false
      end
    end

    def not_code_sent?
      self.mobile_verified == "not_sent" || self.mobile_verified == "error"
    end

    def mobile_verified?
      self.mobile_verified == "verified"
    end

    def send_reset_password_code(password)
      begin
        dispathcer = SendSMS.new
        message = dispathcer.message(self.mobile,"use this Password #{password} to login into app \nTeam Lorryz")
      rescue
        puts "got error"
      end
    end

    def driver_login_sms()
      begin
        dispathcer = SendSMS.new
        message = dispathcer.message(self.mobile,"Hi, #{self.first_name} you have been added to lorryz. use these credentials to login to app\n username: #{self.mobile} \n  Password #{password}")
      rescue
        puts "got error"
      end
    end


    def report_problem(user,admin,shipment)
      begin
        dispathcer = SendSMS.new
        msg = "Subject: Driver #{self.first_name} has reported a problem delivering a shipment #{shipment.id}\n
          Message: Shipment Details: #{shipment.id}, #{shipment.pickup_full_address} -- #{shipment.drop_full_address}..
          Driver Details: #{self.first_name}, #{self.mobile} Unfortunately, I not able to proceed with my shipment due to a problem. Please contact me as soon as possible. \n
          Thank you!"
        message = dispathcer.message(user.mobile,msg)  if user
        admin_message = dispathcer.message(admin.mobile,msg) if admin
      rescue
        puts "got error #{message}"
      end
    end

    def send_assigned_vehicle_notification(shipment,vehicle)
      PushNotification.send_notification(
       self.os,
       self.device_id,
       "",
       "",
       "Hello #{self.first_name}, you have been assigned a new shipment # #{shipment.try(:id)}",
       nil,
      ) if self.device_id
      Notification.create(notifiable_id: shipment.id, 
        notifiable_type: "Shipment", 
        body: "Hello #{self.first_name}, you have been assigned a new shipment # #{shipment.try(:id)}", 
        user_mentioned_id: self.id)
  
    end

    def self.send_notification_to_receiver(shipment,receiver)

      PushNotification.send_notification(
       receiver.os,
       receiver.device_id,
       "",
       "",
       "Hi #{receiver.first_name}, you have been rated against shipment #{shipment.id}\n. View Shipment Details to see your ratings",
       nil,
      ) if receiver.device_id

      begin
        ShipmentMailer.shipment_rating(shipment.id, receiver.id).deliver
        # dispathcer = SendSMS.new

        # msg = "Subject: #{receiver.first_name} Rated you on Shipment# #{shipment.id}\n\n
        #   Message: Hi #{receiver.first_name}, you have been rated against shipment #{shipment.id}\n Login to app to see your ratings"
        # message = dispathcer.message(receiver.mobile,msg)  if receiver
      rescue
        puts "got error"
      end
    end

    private

    def unique_verification_code
      loop do
        code = generate_code
        break code unless User.get_verification_code(code).exists?
      end
    end

    def generate_code
      [0,0,0,0].map!{|x| (0..9).to_a.sample}.join
      # Devise.friendly_token(4).downcase.gsub(/[-,_,o,0]/,"x")
    end

    def phone_verificaton_required?
      if self.driver?
        self.update_attributes(verification_code: "",mobile_verified: "verified") 
        false
      else
        true
      end
    end

  end

end