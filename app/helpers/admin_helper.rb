module AdminHelper
  def company_details_column_names fleet_owner
    columns= ["full_name","Company Name","status", "mobile","email", "Country","License","Blacklisted","Verified","Featured","Safe for cash on delivery","Actions"]
    columns.delete("Safe for cash on delivery") 
    columns.delete("status") if fleet_owner
    create_thead columns

  end

  def individual_details_column_names fleet_owner

    columns = ["full_name","email", "mobile","Status", "Country","Natinal ID","Registration Date","Verified","Featured","Blacklisted","Safe for cash on delivery","Actions"]if !fleet_owner
    columns = ["full_name", "mobile","Country", "Truck No", "Status", "Preferred Location","Verified","Featured","Blacklisted","Actions" ]if fleet_owner

  
    create_thead columns
  end

  def create_thead columns
    str = " <thead> <tr>"
    columns.map do |col|
      str = str + " <th> #{col.humanize} </th> "
    end
    str + "</tr> </thead>"
    str.html_safe
  end

end
