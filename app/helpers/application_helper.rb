module ApplicationHelper
	def bootstrap_class_for_flash(flash_type)
    case flash_type
    when 'success'
      'alert-success'
    when 'error'
      'alert-danger'
    when 'alert'
      'alert-warning'
    when 'notice'
      'alert-info'
    else
      flash_type.to_s
    end
  end

	def slider_range_value(shipment_vehicle)
		if shipment_vehicle.status == "booked"
			0
		elsif shipment_vehicle.status == 'start'
			20
		elsif shipment_vehicle.status == 'loading'
			35
		elsif shipment_vehicle.status == 'enroute'
			55
		elsif shipment_vehicle.status == 'unloading'
			77
		elsif shipment_vehicle.status == 'finish'
			92
		end 
	end

	def signup?
		params[:controller] == "companies" and params[:action] == "get_company_information"
	end

	def format_amount_to_currency amount  , unit = "" , precision = 2
		return "#{ number_to_currency(amount, precision: precision, unit: unit)}"
	end
	def format_float number
		return "#{ number_with_precision(number, precision: 2,strip_insignificant_zeros: true)}"
	end
	def shipment_currency shipment
		shipment.try(:country).try(:currency).to_s rescue ""
	end
	def user_currency
		current_user.try(:country).try(:currency).to_s rescue ""
	end


	def shipment_states
		Shipment.states.map(&:clone).delete_if{|element| element ==["Initial","initial"]}
	end

	def call_geolocate
		return "geolocate(this)"
		# if current_user.present? and current_user.country.present?
		# 	"geolocate(this ,'#{current_user.country.short_name.downcase}')" if current_user.present?
		# else
		# 	"geolocate(this)"
		# end
	end

	def display_date_format
		"%d-%m-%Y"
	end

	def excel_date_format
		"%d-%m-%Y"
	end

	def excel_datetime_format
		' %d-%m-%Y  %I:%M %p '
	end

	def activity_date_format
		"%d-%m-%Y  %I:%M %p"
	end

	def local_time_format datetime
		local_time(datetime, "%I:%M %p") rescue ""
	end

	def assigned_vehicle_for_driver(shipment)
		if @user.vehicle
			shipment.shipment_vehicles.find_by_vehicle_id @user.vehicle.id
		end
	end

	def shipment_route(shipment)
		
		zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")

			Aws.config.update({
				region: "us-east-1",
				credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
			  })  
			  dynamodb = Aws::DynamoDB::Client.new
			  table_name = 'vehicle_coordinates'


			  
			  if  shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).present? &&shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).present?  && shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).present? && shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).present?
				start_time = {"date"=>  shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).strftime("%H:%M") }
				start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
				tz = ISO3166::Country.new(shipment.country.short_name).timezones.zone_identifiers.first
				start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
				
				start_timestamp = start_time_csv.utc
				complete_time = {"date"=>  shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).strftime("%H:%M") }
				complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
				complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }

				complete_timestamp = complete_time_csv.utc
				vehicle_id  = shipment.shipment_fleets.last.company.vehicles.first.try(:id) if shipment.shipment_fleets.present? && shipment.shipment_vehicles.blank?
				vehicle_id = shipment.shipment_vehicles.first.try(:vehicle_id)  if shipment.shipment_vehicles.present?
				params = {
	  
			table_name: table_name,
			key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
			expression_attribute_names: {
				"#created" => "created",
				"#vehicle_id" => "vehicle_id"
			},
			expression_attribute_values: {
			  ":vehicle_id" => vehicle_id,
				":start_time" =>   start_timestamp.to_s,
				":end_time" => complete_timestamp.to_s
			} }



			
		@resp = dynamodb.query(params)  

		@resp.items
	  
	  
			end




	end




	def shipment_distance(shipment)
		zone = ActiveSupport::TimeZone.new("Eastern Time (US & Canada)")

		
		Aws.config.update({
			region: "us-east-1",
			credentials: Aws::Credentials.new('AKIAYX6CX7KTXA4ORGKZ', 'JFk7e0hTK5KEwsyGJdnsIVHNp03YftjijMi58Iuz')
		  })  
		  dynamodb = Aws::DynamoDB::Client.new
		  table_name = 'vehicle_coordinates'

		 




		  if  shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).present? &&shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).present?  && shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).present? && shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).present?
			start_time = {"date"=>  shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at).strftime("%d/%m/%y") , "time"=>  shipment.shipment_action_dates.where(state: "Start").try(:last).try(:performed_at_time).strftime("%H:%M") }
			start_time_csv = Time.strptime("#{start_time["date"]}:#{start_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
			tz = ISO3166::Country.new(shipment.country.short_name).timezones.zone_identifiers.first
            start_time_csv = Time.use_zone(tz) { start_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
			start_timestamp = start_time_csv.utc
			complete_time = {"date"=>  shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at).strftime("%d/%m/%y") ,  "time"=>  shipment.shipment_action_dates.where(state: "complete").try(:last).try(:performed_at_time).strftime("%H:%M") }
			complete_time_csv = Time.strptime("#{complete_time["date"]}:#{complete_time["time"]}", '%d/%m/%y:%H:%M') # 2016-04-23 17:06:00 +0000
			complete_time_csv = Time.use_zone(tz) { complete_time_csv.to_datetime.change(offset: Time.zone.now.strftime("%z")) }
			complete_timestamp = complete_time_csv.utc
			vehicle_id  = shipment.shipment_fleets.last.company.vehicles.first.try(:id) if shipment.shipment_fleets.present? && shipment.shipment_vehicles.blank?
			vehicle_id = shipment.shipment_vehicles.first.try(:vehicle_id)  if shipment.shipment_vehicles.present?
			
			params = {
  
		table_name: table_name,
		key_condition_expression: "#vehicle_id = :vehicle_id and #created between  :start_time and :end_time",
		expression_attribute_names: {
			"#created" => "created",
			"#vehicle_id" => "vehicle_id"
		},
		expression_attribute_values: {
		  ":vehicle_id" => vehicle_id,
			":start_time" =>   start_timestamp.to_s,
			":end_time" => complete_timestamp.to_s
		} }
	@resp = dynamodb.query(params)  



	@distance = 0 
	@resp.items.each do |user| 
   
  
		 @distance = @distance + Geocoder::Calculations.distance_between([@temp_lat , @temp_lng],[user["lat"],user["lng"]],:units =>:km)  if   @temp_lat.present?
		 @temp_lat = user["lat"]
		 @temp_lng= user["lng"]
  
	   end
		 
	   @distance

		end




end


	def local_datetime_format datetime
		local_time(datetime, "%d-%m-%Y %l:%M%P") rescue ""
	end
	def bid_eta pickup_lat,pickup_lng,shipment
       distance = 0
		# url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{pickup_lat.to_f},#{pickup_lng.to_f}&destinations=#{shipment.pickup_lat.to_f},#{shipment.pickup_lng.to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
		# response = HTTParty.get(url)
		# distance = response["rows"].first["elements"].first["duration"].blank? ? 0 : (response["rows"].first["elements"].first["duration"]["text"] )  rescue ""
		return distance

	  end
	
	  def shipment_eta pickup_lat,pickup_lng,drop_lat,drop_lng,
		distance = 0
		# url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=#{pickup_lat.to_f},#{pickup_lng.to_f}&destinations=#{drop_lat.to_f},#{drop_lat.to_f}&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"
		# response = HTTParty.get(url)
		# distance = response["rows"].first["elements"].first["duration"].blank? ? 0 : (response["rows"].first["elements"].first["duration"]["text"] )  rescue ""
		return distance

	  end

	def local_datetime_excel_format datetime
		local_time(datetime, excel_datetime_format) rescue ""
	end

	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end
	

	def action_date(activity)
		# "#{activity.created_at.strftime("%d/%m/%Y %I:%M%p")}" rescue ''
		local_time(activity.created_at, "%d-%m-%Y %l:%M%P") rescue ''
	end

	def rebid s
		s.bids.pluck(:company_id).include? current_user.company.id
	end

	def entity_name(activity)

		# if activity.shipment_vehicle?
		# 	@link_id = activity.auditable.shipment.id rescue ''
		# elsif activity.bid?
		# 	@link_id = activity.auditable.shipment_id
		# else
		# 	@link_id = activity.auditable.id
		# end
		if activity.shipment?
			"#{activity.auditable_type} ##{link_to "#{activity.auditable.id}", admin_shipment_path(activity.auditable.id) }".html_safe rescue ''	
		elsif activity.user?
			"#{activity.auditable_type} ##{link_to "#{activity.auditable.id}", admin_user_path(activity.auditable.id) }".html_safe rescue ''	
		else
			"#{activity.auditable_type}".html_safe rescue ''	
		end
		
	end

	def display_bids_on_cargo_shipment_status
		["ongoing", "cancel" , "completed" ,"upcoming" , "accepted"]
	end

	def cargo_pay_to_lorryz shipments
		(shipments.where(state: "completed").sum(:amount).to_f rescue 0.0) + (shipments.where(state: "cancel").sum(:cancel_penalty).to_f rescue 0.0)
	end



	def fleet_current_receivable_payments shipments
		payable = shipments.where(state: "completed", lorryz_comission_received: false, payment_option: ["before_pick_up","after_delivery"]).sum(:lorryz_share_amount).to_f #rescue 0.0
		cancel_payable = (shipments.where(state: "cancel", cancel_by: "fleet_owner").sum(:cancel_penalty).to_f) #	rescue 0.0
		receivable  = (shipments.where(state: "completed", paid_to_fleet: false, payment_option: ["credit_card","bank_remittance","credit_period"]).sum(:fleet_income).to_f ) #rescue 0.0
		return (  receivable - (payable + cancel_payable) )  #rescue 0.0
	end

	def fleet_current_payable_payments shipments
		payable = shipments.where(state: "completed", lorryz_comission_received: false, payment_option: ["before_pick_up","after_delivery"]).sum(:lorryz_share_amount).to_f  #rescue 0.0
		cancel_payable = (shipments.where(state: "cancel", cancel_by: "fleet_owner").sum(:cancel_penalty).to_f) #	rescue 0.0
		receivable  = (shipments.where(state: "completed", paid_to_fleet: false, payment_option: ["credit_card","bank_remittance","credit_period"]).sum(:fleet_income).to_f ) #rescue 0.0
		return (  (payable + cancel_payable) - receivable )  #rescue 0.0
	end

	def rated_by company
		rating_count  = company.received_ratings.count
		if rating_count > 0
			return "(" + rating_count.to_s + ")"
		else
			"(0)"
		end
	end

end
